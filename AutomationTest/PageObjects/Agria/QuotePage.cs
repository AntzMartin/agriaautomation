﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;
using CsvHelper;
using System.IO;
using System.Linq;
using System.Reflection;
using AutomationTest.Core.Models;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = OpenQA.Selenium.Support.UI.ExpectedConditions;
using System;
using AutomationTest.Helpers;

namespace AutomationExample.PageObjects
{
    public class QuotePage : PageWithMenu
    {
        public QuotePage(SeleniumTest test, QuoteAndBuyUserModel user) : base(test)
        {
            if (user.Journey != "Free")
            {
                if (user.Brand.Contains("Agria"))
                {
                    Url = Configuration.RunConfiguration.BaseUrl + "quote";
                }

                else if (user.Brand.Contains("KC"))
                {
                    Url = Configuration.RunConfiguration.BaseUrl.Replace("agriapet", "kcinsurance") + "quote";

                }

                else if (user.Brand.Contains("GCCF"))
                {
                    Url = Configuration.RunConfiguration.BaseUrl + "quote/?affid=20275&cat";
                }
            }
            else
            {
                if (user.Brand.Contains("Agria") && user.Type.Contains("Breeder"))
                {
                    Url = Configuration.RunConfiguration.BaseUrl + "breeders/free-cover";
                }
            }
            //  Url = Configuration.RunConfiguration.BaseUrl + "breeders/free-cover/";
            test.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }



        #region Fields
        [FindsBy(How = How.ClassName, Using = "generic-spinner__wheel")]
        private IWebElement APILoading;

        [FindsBy(How = How.Id, Using = "SelectDog")]
        private IWebElement Dog;

        [FindsBy(How = How.Id, Using = "SelectCat")]
        private IWebElement Cat;

        [FindsBy(How = How.Id, Using = "SelectRabbit")]
        private IWebElement Rabbit;

        //Your Details
        [FindsBy(How = How.XPath, Using = "//select[@name='title']")]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@id='stepForm']/div/div/div/div[2]/div[1]/div[1]/div/div[1]/select/option[2]")]
        public IWebElement Mr;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='First name']")]
        private IWebElement FirstName;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Last name']")]
        private IWebElement LastName;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Enter your postcode']")]
        private IWebElement PostCodeField;

        [FindsBy(How = How.XPath, Using = "//a[@class='tab-sections__inline-action address-input__find-button']")]
        private IWebElement PostCodeButton;

        [FindsBy(How = How.XPath, Using = "//li[contains(text(),'58, EQUATOR, ELLIOT STREET, GLASGOW, LANARKSHIRE, ')]")]
        private IWebElement PostCodeSelection;

        [FindsBy(How = How.XPath, Using = "//input[@name='mobileNumber']")]
        private IWebElement MobileNumber;

        [FindsBy(How = How.XPath, Using = "//input[@name='homePhoneNumber']")]
        private IWebElement HomeNumber;

        [FindsBy(How = How.XPath, Using = "//input[@name='emailAddress']")]
        private IWebElement EmailAddress;

        [FindsBy(How = How.XPath, Using = "//label[@id='confirmAgeLbl']")]
        private IWebElement Over18;

        [FindsBy(How = How.Id, Using = "continueBtn")]
        private IWebElement ContinueButton;

        [FindsBy(How = How.Id, Using = "ContinueBtn")]
        private IWebElement ContinueButton2;

        [FindsBy(How = How.Name, Using = "pet_name")]
        private IWebElement PetNameXpath;

        [FindsBy(How = How.XPath, Using = "//a[@id='DontKnowDobBtn']")]
        private IWebElement NotExactDOB;

        [FindsBy(How = How.Name, Using = "ageYears")]
        private IWebElement YearsDOB;

        [FindsBy(How = How.Id, Using = "petBreed")]
        private IWebElement PetBreed;

        [FindsBy(How = How.Name, Using = "petSpecies")]
        private IWebElement PetSpecies;

        [FindsBy(How = How.Id, Using = "pet-species-Puppy-label")]
        private IWebElement Puppy;

        [FindsBy(How = How.Id, Using = "pet-species-Kitten-label")]
        private IWebElement Kitten;

        [FindsBy(How = How.Id, Using = "9aa8b61c-145b-4260-bad0-cbe4f464087d")]
        private IWebElement Male;

        [FindsBy(How = How.Id, Using = "206716fc-8baa-4eab-8c72-ffcb42e39d52")]
        private IWebElement Female;

        [FindsBy(How = How.XPath, Using = "//input[@id='petCostField']")]
        private IWebElement Cost;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input']//label[@class='btn'][contains(text(),'Yes')]")]
        private IWebElement IllnessYes;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input']//label[@class='btn'][contains(text(),'No')]")]
        private IWebElement IllnessNo;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'Yes')]")]
        private IWebElement LiableYes;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'No')]")]
        private IWebElement LiableNo;

        [FindsBy(How = How.XPath, Using = "//div[@class='cover-options grid grid--half']//div[1]//div[1]//div[2]//div[1]//label[1]")]
        private IWebElement LowCover;

        [FindsBy(How = How.XPath, Using = "//div[@class='cover-options grid grid--half']//div[2]//div[1]//div[2]//div[1]//label[1]")]
        private IWebElement HighCover;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/label[1]")]
        private IWebElement HighExcess;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/label[1]")]
        private IWebElement LowExcess;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/a[2]")]
        private IWebElement OverseasTravel;

        [FindsBy(How = How.XPath, Using = "//a[@class='btn cover-extra__button--remove']")]
        private IWebElement OverseasTravelRemove;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/a[2]")]
        private IWebElement DeathAndLoss;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/a[2]")]
        private IWebElement BreadingRisks;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[6]/div[2]/a[2]")]
        private IWebElement BoardingFees;

        [FindsBy(How = How.Id, Using = "startDatePicker")]
        private IWebElement PolicyStart;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='‹'])[1]/following::th[2]")]
        private IWebElement PolicyStartYear;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[1]/following::span[9]")]
        private IWebElement Date;

        [FindsBy(How = How.Id, Using = "vaccinationDatePicker")]
        private IWebElement VaccinationDate;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Name']")]
        private IWebElement VetName;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Location']")]
        private IWebElement VetLocation;

        [FindsBy(How = How.XPath, Using = "//a[@id='vetSearchBtn']")]
        private IWebElement VetSearchButton;

        [FindsBy(How = How.XPath, Using = "//li[@class='multiselect__item ng-binding ng-scope']")]
        private IWebElement VetLocationSelect;


        [FindsBy(How = How.XPath, Using = "//select[@name='campaignSelector']")]
        private IWebElement HowDidYouHear;

        [FindsBy(How = How.XPath, Using = "//label[@for='c1']")]
        private IWebElement NoThanks;

        [FindsBy(How = How.XPath, Using = "//label[@for='c2']")]
        private IWebElement YesPlease;

        [FindsBy(How = How.XPath, Using = "//label[@class='checkbox-before ng-binding']")]
        private IWebElement IConfirm;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='›'])[1]/following::span[5]")]
        private IWebElement VaccinationYear;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='Jun'])[1]/following::span[1]")]
        private IWebElement VaccinationMonth;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='So we know when the next one is due'])[1]/following::div[2]")]
        private IWebElement VaccinationDay;//button[@id='continueBtn']

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Annually')]")]
        private IWebElement Yearly;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Monthly')]")]
        private IWebElement Monthly;

        [FindsBy(How = How.XPath, Using = "//select[@name='preferredDayOfPayment-']")]
        private IWebElement MonthlyDate;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Direct debit')]")]
        private IWebElement DirectDebit;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Credit/Debit card')]")]
        private IWebElement CreditCard;

        [FindsBy(How = How.Id, Using = "card.cardNumber")]
        private IWebElement CreditCardNumberField;

        [FindsBy(How = How.Id, Using = "card.cardHolderName")]
        private IWebElement CreditCardNameField;

        [FindsBy(How = How.Id, Using = "card.expiryMonth")]
        private IWebElement CreditCardMonthField;

        [FindsBy(How = How.Id, Using = "card.expiryYear")]
        private IWebElement CreditCardYearField;

        [FindsBy(How = How.Name, Using = "pay")]
        private IWebElement CreditCardSubmit;

        [FindsBy(How = How.Id, Using = "CreditCardIFrame")]
        private IWebElement iFrame;

        [FindsBy(How = How.XPath, Using = "//input[@name='accountHolderName']")]
        private IWebElement DirectDebitNameField;

        [FindsBy(How = How.XPath, Using = "//input[@name='sortCode1']")]
        private IWebElement DirectDebitSortCodeField;

        [FindsBy(How = How.XPath, Using = "//input[@name='accountNumber']")]
        private IWebElement DirectDebitAccountField;

        [FindsBy(How = How.Id, Using = "CompleteDirectDebitBtn")]
        private IWebElement DirectDebitCompleteButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'No')]")]
        private IWebElement HomeCatNo;

        [FindsBy(How = How.Id, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'Yes')]")]
        private IWebElement HomeCatYes;

        [FindsBy(How = How.Id, Using = "//input[@id='promoCode']")]
        private IWebElement InputPromoCode;

        [FindsBy(How = How.Id, Using = "//a[@id='applyPromocodeBtn']")]
        private IWebElement ApplyPromoCode;

        [FindsBy(How = How.XPath, Using = "//*[@id='yie - close - button - ba29a23a - d8d1 - 5ad1 - 8e9b - 279e32e048c8']")]
        private IWebElement CloseButton;

        [FindsBy(How = How.XPath, Using = "//a[@id='addAnotherPetBtn']")]
        private IWebElement MultiPet;

        ///############## KC Elements ###################
        [FindsBy(How = How.XPath, Using = "//a[@class='top-bar__nav-link'][contains(text(),'Breeders')]")]
        private IWebElement KCBreeders;

        [FindsBy(How = How.XPath, Using = "//a[@class='logo-bar__button logo-bar__button--get-quote']")]
        private IWebElement KCActiveFreeCover;

        [FindsBy(How = How.XPath, Using = "//input[@name='email']")]
        private IWebElement Agria_KC_EnterEmail;
        /// <summary>
        /// 
        /// </summary>
        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement ContinueAsGuest;

        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement title;

        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement firstName;

        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement Surname;

        [FindsBy(How = How.XPath, Using = "//input[@id='addressSearchPostcode']")]
        private IWebElement EnterPostcode;

        [FindsBy(How = How.XPath, Using = "//a[@class='tab-sections__inline-action address-input__find-button']")]
        private IWebElement FindPostcode;

        [FindsBy(How = How.XPath, Using = "//li[contains(text(),'58, EQUATOR, ELLIOT STREET, GLASGOW, LANARKSHIRE, ')]")]
        private IWebElement SelectPostcode;

        [FindsBy(How = How.Id, Using = "//input[@id='homePhoneNumber']")]
        private IWebElement PhoneNumber;

        [FindsBy(How = How.Id, Using = "//input[@id='dobPicker']")]
        private IWebElement BreederDOB;

        [FindsBy(How = How.Id, Using = "//button[@id='continueBtn']")]
        private IWebElement KCSubmitEmail;

        [FindsBy(How = How.XPath, Using = "//input[@name='agriaId']")]
        private IWebElement Agria_KC_EnterPostcode;

        [FindsBy(How = How.Id, Using = "//button[@id='loginBtn']")]
        private IWebElement KCSubmitPostcode;

        [FindsBy(How = How.XPath, Using = "//label[@id='noRegNumberLbl']")]
        private IWebElement KCCheckDontKnow;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='e.g. 'Snowball'']")]
        private IWebElement KCPetName;

        [FindsBy(How = How.XPath, Using = "//input[@id='petBreed']")]
        private IWebElement KCPetBreed;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Join the Club')]")]
        private IWebElement JoinClub;

        [FindsBy(How = How.Id, Using = "dobPicker")]
        private IWebElement DOBpicker;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='›'])[1]/following::span[5]")]
        private IWebElement DOByear;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='Nov'])[1]/following::span[1]")]
        private IWebElement DOBmonth;

        [FindsBy(How = How.XPath, Using = "//body[@id='ng-app']/div/div/div/section/div/div/form/div/div/div/div[2]/div[2]/div/div/div/div/div/div/table/tbody/tr[4]/td[6]/span")]
        private IWebElement DOBday;


        #endregion

        public void APIWait()
        {
            if (APILoading.Displayed)
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("generic-spinner__wheel")));
            }
        }

        public void HighlightElement(IWebElement element)
        {
            var jsDriver = (IJavaScriptExecutor)test.Driver;
            string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"", ""background"" : ""yellow"" });";
            jsDriver.ExecuteScript(highlightJavascript, new object[] { element });
            Thread.Sleep(200);
            var clear = @"$(arguments[0]).css({ ""border-width"" : ""0px"", ""border-style"" : ""solid"", ""border-color"" : """", ""background"" : """" });"; ;
            jsDriver.ExecuteScript(clear, new object[] { element });
        }




        public void SelectSpecies(string species)
        {
            APIWait();
            var driver = test.Driver;

            if (string.Equals(species, "Dog", StringComparison.InvariantCultureIgnoreCase))
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(Dog)).Click();
            }
            else if (string.Equals(species, "Cat", StringComparison.InvariantCultureIgnoreCase))
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(Cat)).Click();
            }
            else if (string.Equals(species, "Rabbit", StringComparison.InvariantCultureIgnoreCase))
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(Rabbit)).Click();
            }
        }

        public void ClickContinueButton()
        {
            HighlightElement(ContinueButton);
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueButton)).Click();
            //Thread.Sleep(3000);
            APIWait();
        }

        public void ClickContinueButton2()
        {
            HighlightElement(ContinueButton2);
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueButton2)).Click();
            //Thread.Sleep(3000);
            APIWait();
        }

        public void CoverOption(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;

            if (user.IsLifeTimePlus)
            {
                driver.ClickButton(HighCover);

            }
            else
            {
                driver.ClickButton(LowCover);
            }

            driver.ClickButton(ContinueButton);
        }

        public void Excess(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;

            if (user.HighExcess)
            {
                driver.ClickButton(HighExcess);

            }
            else
            {
                driver.ClickButton(LowExcess);
            }

            driver.ClickButton(ContinueButton);
        }

        public void ExtrasButtons(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;

            if (!user.HomeCat && user.OverseasTravel)
            {
                driver.ClickButton(OverseasTravel);
            }

            if (user.DeathAndLoss)
            {
                driver.ClickButton(DeathAndLoss);
            }

            if (user.BreedingRisks)
            {
                driver.ClickButton(BreadingRisks);
            }

            if (user.BoardingFees)
            {
                driver.ClickButton(BoardingFees);
            }
        }

        public void addAnotherPet()
        {
            HighlightElement(MultiPet);
            wait.Until(ExpectedConditions.ElementToBeClickable(MultiPet)).Click();
        }

        public void YourDetails(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;
            driver.SelectInput(Title, user.Title);

            driver.TextFieldInput(FirstName, user.Firstname);

            driver.TextFieldInput(LastName, user.Lastname);

            driver.TextFieldInput(PostCodeField, user.Postcode);

            driver.ClickButton(PostCodeButton);

            SelectAddress(user.HouseNumberANDName);

            if (user.MobileNumber[0] != 0)
            {
                user.MobileNumber = $"0{user.MobileNumber}";
            }

            driver.TextFieldInput(MobileNumber, user.MobileNumber);

            driver.TextFieldInput(EmailAddress, user.Email);

            if (driver.Url.Contains("free"))
            {
                driver.ClickButton(IConfirm);
            }
            else
            {
                driver.ClickButton(Over18);
            }

            driver.ClickButton(ContinueButton);

        }

        public void YourPetDetails(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;

            driver.TextFieldInput(PetNameXpath, user.PetName);

            driver.ClickButton(NotExactDOB);

            driver.TextFieldInput(YearsDOB, user.PetAge);

            if (string.Equals(user.PetSpecies, "Agria", StringComparison.InvariantCultureIgnoreCase))
            {
                driver.SelectInput(PetSpecies, user.PetSpecies);
            }

            driver.TextFieldInput(PetBreed, user.PetBreed);

            driver.DateInput(PolicyStart, user.PolicyStartDate);

            if (user.HomeCat)
            {
                driver.ClickButton(HomeCatYes);
            }

            if (string.Equals(user.PetGender, "Female", StringComparison.InvariantCultureIgnoreCase))
            {
                driver.ClickButton(Female);
            }
            else
            {
                driver.ClickButton(Male);
            }

            driver.TextFieldInput(Cost, user.PetCost);

            if (string.Equals(user.PetSpecies, "Cat", StringComparison.InvariantCultureIgnoreCase))
            {
                if (user.HomeCat)
                {
                    driver.ClickButton(HomeCatYes);
                }
                else
                {
                    driver.ClickButton(HomeCatNo);
                }
            }

            if (string.Equals(user.PetSpecies, "Dog", StringComparison.InvariantCultureIgnoreCase))
            {
                if (user.PetLiable)
                {
                    driver.ClickButton(LiableYes);
                }
                else
                {
                    driver.ClickButton(LiableNo);
                }
            }

            if (user.PetIllness)
            {
                driver.ClickButton(IllnessYes);
            }
            else
            {
                driver.ClickButton(IllnessNo);
            }

            driver.ClickButton(ContinueButton);

        }

        public void SummaryPage(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;

            if (user.KeepingYouInformed)
            {
                driver.ClickButton(YesPlease);
            }
            else
            {
                driver.ClickButton(NoThanks);
            }

            if (!string.Equals(user.PromoCode, "None", StringComparison.InvariantCultureIgnoreCase))
            {
                driver.TextFieldInput(InputPromoCode, user.PromoCode);
                driver.ClickButton(ApplyPromoCode);
            }

            driver.ClickButton(IConfirm);
            driver.ClickButton(ContinueButton);
        }

        public void PaymentType(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;

            if (user.PayMonthly)
            {
                driver.ClickButton(Monthly);
                driver.SelectInput(MonthlyDate, user.DayOfPayment);

                if (user.Multipet)
                {

                }
            }
            else
            {
                driver.ClickButton(Yearly);
            }

            if (user.DriectDebit)
            {
                driver.ClickButton(DirectDebit);
            }
            else
            {
                driver.ClickButton(CreditCard);
            }

            if (user.DriectDebit)
            {
                driver.ClickButton(DirectDebit);
            }
            else
            {
                driver.ClickButton(CreditCard);
            }

            driver.ClickButton(ContinueButton);
        }

        public void PaymentInput(QuoteAndBuyUserModel user)
        {
            APIWait();
            var driver = test.Driver;
            if (Url.Contains("eqtr-test") || Url.Contains("eqtr-staging"))
            {

                if (user.DriectDebit)
                {
                    driver.TextFieldInput(DirectDebitNameField, user.Lastname);

                    wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitSortCodeField)).SendKeys("560036");

                    wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitAccountField)).SendKeys("40308669");

                    driver.ClickButton(DirectDebitCompleteButton);
                }
                else
                {
                    wait.Until(ExpectedConditions.ElementExists((By.Id("CreditCardIFrame"))));

                    test.Driver.SwitchTo().Frame(test.Driver.FindElement(By.Id("CreditCardIFrame")));

                    wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardNumberField)).SendKeys("5555444433331111");

                    wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardNameField)).SendKeys("Test");

                    wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardMonthField)).SendKeys("05");

                    wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardYearField)).SendKeys("2025");

                    wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardSubmit)).Click();
                }
            }
            APIWait();
        }



        //public void PaymentYear()
        //{
        //    APIWait();
        //    HighlightElement(Yearly);
        //    wait.Until(ExpectedConditions.ElementToBeClickable(Yearly)).Click();
        //}

        //public void PaymentMonthly()
        //{
        //    APIWait();
        //    HighlightElement(Monthly);
        //    wait.Until(ExpectedConditions.ElementToBeClickable(Monthly)).Click();
        //    var dropdown = new SelectElement(MonthlyDate);
        //    HighlightElement(MonthlyDate);
        //    dropdown.SelectByIndex(1);
        //}

        //public void CCPaymentClick()
        //{
        //    HighlightElement(CreditCard);
        //    wait.Until(ExpectedConditions.ElementToBeClickable(CreditCard)).Click();

        //}

        //public void DDPayment()
        //{
        //    HighlightElement(DirectDebit);
        //    wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebit)).Click();
        //}
        //public void DDPayment()
        //{
        //    HighlightElement(DirectDebit);
        //    wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebit)).Click();
        //}

        public void DDPaymentEnter()
        {
            HighlightElement(DirectDebitNameField);
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitNameField)).SendKeys("Anthony Test");
            HighlightElement(DirectDebitSortCodeField);
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitSortCodeField)).SendKeys("560036");
            HighlightElement(DirectDebitAccountField);
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitAccountField)).SendKeys("40308669");
            HighlightElement(DirectDebitCompleteButton);
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitCompleteButton)).Click();

        }

        public void AgriaAndKcFreeCover(QuoteAndBuyUserModel user)
        {
            APIWait();

            var driver = test.Driver;

            driver.TextFieldInput(Agria_KC_EnterEmail, user.Email);
            driver.ClickButton(ContinueButton);
            driver.TextFieldInput(Agria_KC_EnterPostcode, user.Postcode);
            driver.TextFieldInput(PetNameXpath, user.PetName);

            if (string.Equals(user.PetSpecies, "Puppy", StringComparison.InvariantCultureIgnoreCase))
            {
                driver.ClickButton(Puppy);
            }
            else
            {
                driver.ClickButton(Kitten);
            }

            driver.TextFieldInput(PetBreed, user.PetBreed);

            //Needs rewritten to be dynamic input from CSV
            driver.ClickButton(DOBpicker);
            driver.ClickButton(DOByear);
            driver.ClickButton(DOBmonth);
            driver.ClickButton(DOBday);

            if (string.Equals(user.PetGender, "Female", StringComparison.InvariantCultureIgnoreCase))
            {
                driver.ClickButton(Female);
            }
            else
            {
                driver.ClickButton(Male);
            }

            driver.TextFieldInput(Cost, user.PetCost);

            if (user.PetIllness)
            {
                driver.ClickButton(IllnessYes);
            }
            else
            {
                driver.ClickButton(IllnessNo);
            }

            driver.DateInput(PolicyStart, user.PolicyStartDate);

            driver.ClickButton(ContinueButton);

        }


        public void KCFreeCover()
        {
            int count = 59;

            wait.Until(ExpectedConditions.ElementToBeClickable(KCBreeders)).Click();
            APIWait();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCActiveFreeCover)).Click();
            APIWait();
            Thread.Sleep(2000);
            count++;
            wait.Until(ExpectedConditions.ElementToBeClickable(Agria_KC_EnterEmail)).SendKeys("test" + count + "@eqtr.com");
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCSubmitEmail)).Click();
            APIWait();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueAsGuest)).Click();


            wait.Until(ExpectedConditions.ElementToBeClickable(Agria_KC_EnterPostcode)).SendKeys("G3 8DZ");
            wait.Until(ExpectedConditions.ElementToBeClickable(KCSubmitPostcode)).Click();
            APIWait();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCCheckDontKnow)).Click();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCPetName)).SendKeys("Fluffy");
            wait.Until(ExpectedConditions.ElementToBeClickable(KCPetBreed)).SendKeys("American Lamalese");
        }

        static IEnumerable<ContactFormData> GetContactFormCases(string fileName)
        {
            //Check and see if the file exists first
            var file = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            if (!File.Exists(file))
            {
                throw new FileNotFoundException($"Unable to find the file {fileName}");
            }

            //Now read it in
            using (TextReader fileReader = File.OpenText(file))
            {
                var csv = new CsvReader(fileReader);
                csv.Configuration.HasHeaderRecord = true;
                return csv.GetRecords<ContactFormData>().ToList();
            }
        }

        public void ChromeFullScreenshot()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            //sim.Keyboard.ModifiedKeyStroke(
            //new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            //new[] { VirtualKeyCode.VK_2 });

            //Take screenshot
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);

            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            new[] { VirtualKeyCode.VK_P }
            );
            Thread.Sleep(1000);


            sim.Keyboard.TextEntry(">screenshot");
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);
        }

        public void PageDown()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            sim.Keyboard.KeyPress(VirtualKeyCode.DOWN);

        }
        //public void CompleteYourDetailsPet()
        //{
        //    PetsNameDetails();
        //}



        //public void InputContactData(ContactFormData data)
        //{
        //    FirstName.SendKeys(data.FName);
        //    LastName.SendKeys(data.LName);
        //    MobileNumber.SendKeys(data.MobileNum);
        //    HomeNumber.SendKeys(data.HomeNum);
        //    EmailAddress.SendKeys(data.Email);
        //}


        private void SelectAddress(string Address)
        {
            var driver = test.Driver;

            var addressElement = driver.FindElements(By.ClassName("address-input__address-select")).FirstOrDefault();

            var availaibleAddresses = addressElement.FindElements(By.ClassName("multiselect__item")).FirstOrDefault(x => x.Text.ToLower().Contains(Address.ToLower()));

            wait.Until(ExpectedConditions.ElementToBeClickable(availaibleAddresses)).Click();

        }


    }
}

