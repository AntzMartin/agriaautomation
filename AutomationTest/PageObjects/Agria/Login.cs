﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;

namespace AutomationTest.PageObjects.Agria
{
    namespace AutomationExample.PageObjects
    {
        public class Login : PageWithMenu
        {
            public Login(SeleniumTest test) : base(test)
            {
                Url = Configuration.RunConfiguration.BaseUrl;
            }


            public void ValidateTitle()
            {
                Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
            }

            [FindsBy(How = How.ClassName, Using = "generic-spinner__wheel")]
            private IWebElement APILoading;

            [FindsBy(How = How.XPath, Using = "//a[@class='top-bar__button top-bar__button--log-in']")]
            private IWebElement MyAccountButton;

            [FindsBy(How = How.XPath, Using = "//a[@class='btn btn--bar']")]
            private IWebElement LogOutButton;

            [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Login with your AgriaId')]")]
            private IWebElement AgriaIdButton;

            [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Login with your Email')]")]
            private IWebElement EmailButton;

            [FindsBy(How = How.Id, Using = "AgriaId")]
            private IWebElement AgriaId;

            [FindsBy(How = How.Id, Using = "Password")]
            private IWebElement AgriaPassword;

            [FindsBy(How = How.Id, Using = "loginBtn")]
            private IWebElement LoginButton;

            [FindsBy(How = How.XPath, Using = "//input[@id='Email']")]
            private IWebElement EmailAddress;

            [FindsBy(How = How.XPath, Using = "//input[@id='Password']")]
            private IWebElement EmailPassword;



            public void LoginAgriaId()
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(MyAccountButton)).Click();
                wait.Until(ExpectedConditions.ElementToBeClickable(AgriaIdButton)).Click();
                wait.Until(ExpectedConditions.ElementToBeClickable(AgriaId)).SendKeys("14787");
                wait.Until(ExpectedConditions.ElementToBeClickable(AgriaPassword)).SendKeys("Passw0rd$");
                wait.Until(ExpectedConditions.ElementToBeClickable(LoginButton)).Click();
            }

            public void LogOutAgriaId()
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(LogOutButton)).Click();
            }

            public void ShareXRecordGIF()
            {
                InputSimulator sim = new InputSimulator();
                //Record video
                sim.Keyboard.ModifiedKeyStroke(
                new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
                new[] { VirtualKeyCode.RETURN });
            }
        }


    }
}
