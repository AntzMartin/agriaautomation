﻿namespace AutomationExample.PageObjects
{
    public interface IHasMenu
    {
        void OpenMenu();
        void CloseMenu();
    }
}
