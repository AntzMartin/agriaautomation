﻿using Test.Automation.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace AutomationExample.PageObjects
{
    public abstract class PageWithMenu: BasePage
    {

        public PageWithMenu(SeleniumTest test) : base(test) { }

        [FindsBy(How = How.ClassName, Using = "menu-trigger")]
        protected IWebElement menu;

        [FindsBy(How = How.ClassName, Using = "main-nav__close")]
        protected IWebElement menuClose;


        

        public void OpenMenu()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(menu)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(menuClose));
        }

        public void CloseMenu()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(menuClose)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(menu));
        }
    }
}
