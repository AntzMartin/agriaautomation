﻿using AventStack.ExtentReports;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using OpenQA.Selenium.Chrome;

namespace AutomationTest.Reports
{
    [TestFixture]
    class ReportBase
    {
        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\\AgriaReport.html");
            htmlReporter.Configuration().Theme = Theme.Standard;
            htmlReporter.Configuration().DocumentTitle = "Anthony Doc";
            htmlReporter.Configuration().ReportName = "Anthony Report";

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }

        [SetUp]
        public void InitBrowser()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }

        /*[Test]
        public void PassingTest()
        {
            test = extent.CreateTest("Passing test");
            
            
           // driver.Navigate().GoToUrl("http://www.google.com");

            try
            {
                Assert.IsTrue(true);
                test.Pass("Assertion passed");
            }
            catch (AssertionException)
            {
                test.Fail("Assertion failed");
                throw;
            }
        }

        [Test]
        public void FailingTest()
        {
            test = extent.CreateTest("Failing test");

            //driver.Navigate().GoToUrl("http://www.yahoo.com");

            try
            {
                Assert.IsTrue(false);
                test.Pass("Assertion passed");
            }
            catch (AssertionException)
            {
                test.Fail("Assertion failed");
                throw;
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            driver.Quit();
        }
        */
        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush();
        }
    }
}

