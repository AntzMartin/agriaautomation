﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Diagnostics;
using AutomationTest.PageObjects.Agria.AutomationExample.PageObjects;
using System.Windows.Forms;

namespace Agria
{
    public class MyAccount : SeleniumTest
    {

        public MyAccount(string description, Target browser) : base(description, browser) { }

        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocation = ("C:\\temp\\tests\\screenshots\\Agria\\test\\1.2\\");

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AgriaMyAccount.html");
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Anthony Doc";
            htmlReporter.Configuration().ReportName = "Anthony Report";

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }

        //Members login
        [Test]
        public void EmailLogin()
        {
            string testName = "Email Login";

            try
            {


            }
            catch (Exception e)
            {
                TakeFullPageSnapShots(testName);
                string fullScreenshot1 = (screenshotLocation + "\\David laptop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "\\David laptop chrome - " + testName + " 2.png");
                test.Fail(e.StackTrace + "\nScreenshots below: ");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                throw;
            }
        }

        [Test]
        public void AgriaIDLogin()
        {
            string testName = "Agria ID";

            var login = new Login(this);
            var home = new Home(this);
            home.GoToPage();
            Thread.Sleep(2000);

            try
            {
                login.LoginAgriaId();
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                TakeFullPageSnapShots(testName);
                string fullScreenshot1 = (screenshotLocation + "\\David laptop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "\\David laptop chrome - " + testName + " 2.png");
                test.Fail(e.StackTrace + "\nScreenshots below: ");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                throw;
            }
        }

        [Test]
        public void AgriaIDLoginAndOut()
        {
            string testName = "Agria ID Login and Out";
            test = extent.CreateTest(testName);

            var login = new Login(this);
            var home = new Home(this);
            home.GoToPage();
            Thread.Sleep(2000);

            try
            {
                //login.ShareXRecordGIF();
                login.LoginAgriaId();
                Thread.Sleep(5000);
                login.LogOutAgriaId();
                //TakeFullPageSnapShots(testName + " Journey Completed");
                login.ShareXRecordGIF();
                string clipboardText = Clipboard.GetText(TextDataFormat.Text);
                Thread.Sleep(10000);
                test.AddScreenCaptureFromPath("C:/Users/anthony.martin/Desktop/Captures/Agria ID Login and Out.gif");
                test.AssignCategory("Passed");
                Thread.Sleep(10000);
            }
            catch (Exception e)
            {
                TakeFullPageSnapShots(testName);
                string fullScreenshot1 = (screenshotLocation + "\\David laptop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "\\David laptop chrome - " + testName + " 2.png");
                test.Fail(e.StackTrace + "\nScreenshots below: ");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                test.AssignCategory("Failed");
                throw;
            }
        }

        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush();
        }
    }
}
