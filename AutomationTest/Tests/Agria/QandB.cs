﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using System;


using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter.Configuration;
using AutomationTest.Services;
using System.Linq;
using AutomationTest.Core.Models;
using System.IO;
using OpenQA.Selenium;

namespace Agria
{

    public class Journeys : SeleniumTest
    {


        public Journeys(string description, Target browser) : base(description, browser) { }

        public static IWebDriver _driver;
        private static ExtentHtmlReporter _htmlReporter;
        public static ExtentReports _extent;
        public static ExtentTest test;
        public static string _screenshotLocationTest = (@"C:\temp\tests\screenshots\Agria\test\1.2\");
        public static string _screenshotLocationStaging = (@"C:\temp\tests\screenshots\Agria\staging\1.2\");
        public static string _screenshotLocationLive = ("");
        private readonly QuoteAndBuyPetModel _pet;
      

        [OneTimeSetUp]
        public void SetupReporting()
        {
            _htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\OneDrive - Equator (Scotland) Limited\Agria\Automation Reports\Q&B.html");

            _htmlReporter.Configuration().Theme = Theme.Dark;

            _htmlReporter.Configuration().DocumentTitle = "Anthony Q&B Doc";

            _htmlReporter.Configuration().ReportName = "Anthony Q&B Report";

            _extent = new ExtentReports();

            _extent.AttachReporter(_htmlReporter);
        }

        public void PassedTest(string testName, string extentTestName, string currentUser, string testStatus)
        {

            ExtentScreenShot(testName, currentUser, testStatus);

            string[] files = Directory.GetFiles(_screenshotLocationTest + testName);

            foreach (string file in files)
            {
                if (file.Contains(currentUser))
                {
                    test.AddScreenCaptureFromPath(file);
                }
            }

            test.AssignCategory("Passed");
        }

        public void FailedTest(string testName, string extentTestName, string currentUser, string testStatus, Exception e)
        {
            ExtentScreenShot(testName, currentUser, testStatus);

            if (!Directory.Exists("_screenshotLocationTest + testName"))
            {
                Directory.CreateDirectory(_screenshotLocationTest + testName);
            }
            string[] files = Directory.GetFiles(_screenshotLocationTest + testName);

            foreach (string file in files)
            {
                var sourceFile = new System.IO.FileInfo(file);

                if (file.Contains(currentUser))
                {
                    test.AddScreenCaptureFromPath(file);
                }
            }

            if (e.Message.Contains("not set"))
            {
                test.Fail("Error: CSV data incorrect - Check and run test again");
            }
            else
            {
                test.Fail(e.Message);
            }

            test.Fail("Screenshots below of where the test failed: ");
            test.AssignCategory("Failed");
        }

        public void ExtentScreenShot(string testName, string currentUser, string testStatus)
        {

            var file = "";

            var image = ((ITakesScreenshot)Driver).GetScreenshot();

            if (Driver.Url.Contains("test"))
            {
                file = $@"C:\temp\tests\screenshots\Agria\test\1.2\QuoteAndBuy";
            }
            else if (Driver.Url.Contains("staging"))
            {
                file = $@"C:\temp\tests\screenshots\Agria\staging\1.0\QuoteAndBuy";
            }
            else
            {
                file = $@"C:\temp\tests\screenshots\Agria\live\1.0\QuoteAndBuy";
            }
            if (!Directory.Exists(file))
            {
                Directory.CreateDirectory(file);
            }

            image.SaveAsFile($@"{file}\{testName} - {currentUser} - {testStatus}.png", ScreenshotImageFormat.Png);
        }


        //Quote and Buy for all pets,
        //both monthly and yearly, 
        //both credit card and debit card.

        
        [Test]
        public void QuoteAndBuy()
        {
            string testName = "QuoteAndBuy";
            string extentTestName = "Quote & Buy";


            var userServices = new ParseCSV();

            var allusers = userServices.Users();

            foreach (var user in allusers)
            {
                string currentSite = user.Brand;
                string currentJourney = user.Journey;
                string currentUser = user.Firstname;
                string testStatus;

                test = _extent.CreateTest(currentSite + " - " + currentJourney + " - " + currentUser);

                if (user.Journey == "QB")
                {
                    try
                    {
                        var quote = new QuotePage(this, user);

                        quote.GoToPage();

                        if (string.Equals(user.Brand, "Agria", StringComparison.InvariantCultureIgnoreCase)
                        && !string.Equals(user.Journey, "Free", StringComparison.InvariantCultureIgnoreCase))
                        {
                            quote.SelectSpecies(user.PetSpecies);
                        }

                        var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                        if (signupOverlay != null)
                        {
                            Driver.SwitchTo().Frame(signupOverlay);
                        }

                        quote.YourDetails(user);

                        quote.YourPetDetails(user);

                        quote.CoverOption(user);

                        quote.Excess(user);

                        if (!string.Equals(user.PetSpecies, "Rabbit", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (user.Multipet)
                            {

                                Driver.FindElement(By.XPath("//a[@id='addAnotherPetBtn']")).Click();

                                quote.YourPetDetails(user);

                                quote.CoverOption(user);

                                quote.Excess(user);

                                quote.ExtrasButtons(user);
                            }
                            else
                            {

                                quote.ExtrasButtons(user);
                            }

                            Driver.FindElement(By.XPath("//a[@id='continueBtn']")).Click();
                        }

                        quote.SummaryPage(user);

                        quote.PaymentType(user);

                        quote.PaymentInput(user);

                        if (Driver.Url.Contains("confirmation"))
                        {
                            testStatus = "Pass";
                            ExtentScreenShot(testName, currentUser, testStatus);
                            //ExtentScreenShot(testName, currentUser);
                            PassedTest(testName, extentTestName, currentUser, testStatus);
                            

                        }
                    }
                    catch (Exception e)
                    {
                        testStatus = "Fail";
                        ExtentScreenShot(testName, currentUser, testStatus);
                        FailedTest(testName, extentTestName, currentUser, testStatus, e);

                        //System.Diagnostics.Process proc = new System.Diagnostics.Process();
                        //proc.StartInfo.FileName = @"C:\Users\anthony.martin\Desktop\Cache_Clear.bat";
                        //proc.StartInfo.WorkingDirectory = @"C: \Users\anthony.martin\Desktop\";
                        //proc.Start();
                        //throw new Exception("Error: ", e);
                    }
                }
                else if (user.Journey == "Free")
                {
                    var quote = new QuotePage(this, user);

                    quote.GoToPage();

                    if (string.Equals(user.Brand, "Agria", StringComparison.InvariantCultureIgnoreCase)
                        && !string.Equals(user.Journey, "Free", StringComparison.InvariantCultureIgnoreCase))
                    {
                        quote.SelectSpecies(user.PetSpecies);
                    }

                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    try
                    {
                        quote.AgriaAndKcFreeCover(user);
                        quote.YourDetails(user);
                    }
                    catch (Exception e)
                    {
                        testStatus = "Fail";
                        ExtentScreenShot(testName, currentUser, testStatus);
                        FailedTest(testName, extentTestName, currentUser, testStatus, e);
                        //throw new Exception("Error: ", e);
                    }
                }
                else if (user.Journey == "Continue Cover")
                {
                    var quote = new QuotePage(this, user);

                    quote.GoToPage();

                    if (string.Equals(user.Brand, "Agria", StringComparison.InvariantCultureIgnoreCase)
                        && !string.Equals(user.Journey, "Continue Cover", StringComparison.InvariantCultureIgnoreCase))
                    {
                        quote.SelectSpecies(user.PetSpecies);
                    }

                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    try
                    {
                        quote.AgriaAndKcFreeCover(user);
                        quote.YourDetails(user);
                    }
                    catch (Exception e)
                    {
                        testStatus = "Fail";
                        ExtentScreenShot(testName, currentUser, testStatus);
                        FailedTest(testName, extentTestName, currentUser, testStatus, e);
                        //throw new Exception("Error: ", e);
                    }
                }
            }
        }
        
        [OneTimeTearDown]
        public void GenerateReport()
        {
            _extent.Flush();
        }
    }
}
