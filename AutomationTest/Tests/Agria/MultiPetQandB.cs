﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Diagnostics;
using AutomationTest.Services;
using System.Linq;
using AutomationTest.Core.Models;
using System.IO;

namespace Agria
{

    public class MultiPetQandB : SeleniumTest
    {
        //region Constructors - Do not modify

        public MultiPetQandB(string description, Target browser) : base(description, browser) { }


        //endregion


        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocationTest = (@"C:\temp\tests\screenshots\Agria\test\1.2\");
        public static string screenshotLocationStaging = ("C:\\temp\\tests\\screenshots\\Agria\\staging\\1.0\\");
        public static string screenshotLocationLive = ("C:\\temp\\tests\\screenshots\\Agria\\live\\1.0\\");
        private QuoteAndBuyPetModel pet;

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\Desktop\Agria Automation\AutomationTest\AutomationTest\Reports\MultiPet Q&B.html");

            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Anthony Q&B Doc";
            htmlReporter.Configuration().ReportName = "Anthony Q&B Report";

            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);

            //extentxReporter.setAppendExisting(true);
        }

        public void passedTest(string testName, string extentTestName)//, string currentUser)
        {
           // TakeFullPageSnapShots(testName + " " + currentUser + " - Passed");

            string[] files = Directory.GetFiles(screenshotLocationTest + testName);

            foreach (string file in files)
            {
                if (file.Contains(testName + " - Passed"))
                {
                    test.AddScreenCaptureFromPath(file);
                }
            }

            test.AssignCategory("Passed");
        }

        public void failedTest(string testName, string extentTestName, Exception e)//, string currentUser, Exception e)
        {
            //TakeFullPageSnapShots(testName + " " + currentHotel + " - Failed");
            TakeFullPageSnapShots(testName + " - Failed");
            //var today = DateTime.Today;

            string[] files = Directory.GetFiles(screenshotLocationTest + testName); //+ Date);

            foreach (string file in files)
            {
                var sourceFile = new System.IO.FileInfo(file);

                if (file.Contains(testName + " - Failed"))
                {
                    test.AddScreenCaptureFromPath(file);
                }
            }

            if (e.Message.Contains("not set"))
            {
                test.Fail("Error: CSV data incorrect - Check and run test again");
            }
            else
            {
                test.Fail(e.Message);
            }

            test.Fail("Screenshots below of where the test failed: ");
            test.AssignCategory("Failed");

        }


        public void extentScreenshot(IWebDriver driver, String screenshotName)
        {
            TakeSnapShot("Extent Report");
        }

        //public bool PopUpCheck()
        //{
        //    var iFrameActive = Driver.SwitchTo().Frame(Driver.FindElement(By.Id("yie-overlay-ba29a23a-d8d1-5ad1-8e9b-279e32e048c8")));
        //    bool popUpExists;

        //    try
        //    {
        //        if (iFrameActive.Displayed)
        //        {
        //            popUpExists = true;
        //        }
        //        else
        //        {
        //            popUpExists = false;
        //        }
        //    }
        //    catch
        //    {
        //        popUpExists = false;

        //    }

        //    return popUpExists;
        //}


        //Quote and Buy for all pets,
        //both monthly and yearly, 
        //both credit card and debit card.

    //[Test]
    //public void MultiPet_DogQuote_CreditCardYearly()
    //{
    //    string testName = "MultiPet_DogQuote_CreditCardYearly";
    //    string extentTestName = "MultiPet - Dog Credit Card Yearly";
    //    try
    //    {
    //        var userServices = new ParseCSV();
    //        var petServices = new ParseCSV();

    //        var allusers = userServices.Users();
    //        var allPets = petServices.Pets();


    //        foreach (var user in allusers)
    //        {
    //            test = extent.CreateTest(extentTestName);
    //            var quote = new QuotePage(this);
    //            quote.GoToPage();
    //            Thread.Sleep(1000);

    //           /* quote.SelectDogQuote()*/;

    //            Thread.Sleep(3000);
    //            var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

    //            if (signupOverlay != null)
    //            {
    //                Driver.SwitchTo().Frame(signupOverlay);
    //            }

    //            quote.YourDetails(user);
    //            //quote.PopUpClose();
    //            quote.ClickContinueButton();
    //            quote.YourPetDetails(user);
    //            quote.ClickContinueButton();
    //            //quote.CoverOption();
    //            //quote.ClickContinueButton();
    //            //quote.Excess();
    //            quote.ClickContinueButton();
    //            //quote.ExtrasButtons();
    //            quote.ClickContinueButton();
    //            //quote.Vacctination();
    //            quote.addAnotherPet();
    //            foreach (var pet in allPets)
    //            {
    //                //quote.DogYourDetailsPet(pet);
    //            }
    //            quote.ClickContinueButton();
    //            //quote.CoverOption();
    //            //quote.ClickContinueButton();
    //            //quote.Excess();
    //            quote.ClickContinueButton();
    //            //quote.//ExtrasButtons2();
    //            //quote.ClickContinueButton();
    //            ////quote.Vacctination();
    //            //quote.ClickContinueButton2();
    //            //Thread.Sleep(2000);
    //            //quote.KeepingYouInformed();
    //            //quote.Terms();
    //            //quote.ClickContinueButton();
    //            //Thread.Sleep(2000);
    //            //quote.PaymentYearly();
    //            //quote.ClickContinueButton();
    //            //quote.CCPaymentClick();
    //            //quote.ClickContinueButton();
    //            //Thread.Sleep(2000);
    //            //Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
    //            //quote.CCPaymentEnter();
    //            //passedTest(testName, extentTestName);
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        failedTest(testName, extentTestName, e);
    //        throw new Exception("Error: " + e);

    //    }
    //}

    //[Test]
    //public void MultiPet_DogAndCat_QuoteCreditCardYearly()
    //{
    //    string testName = "MultiPet_DogAndCat_QuoteCreditCardYearly";
    //    string extentTestName = "MultiPet - Dog and Cat Credit Card Yearly";
    //    try
    //    {
    //        var userServices = new ParseCSV();
    //        var petServices = new ParseCSV();

    //        var allusers = userServices.Users();
    //        var allPets = petServices.Pets();


    //        foreach (var user in allusers)
    //        {
    //            test = extent.CreateTest(extentTestName);
    //            var quote = new QuotePage(this);
    //            quote.GoToPage();
    //            Thread.Sleep(1000);

    //            //quote.SelectDogQuote();

    //            Thread.Sleep(3000);
    //            var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

    //            if (signupOverlay != null)
    //            {
    //                Driver.SwitchTo().Frame(signupOverlay);
    //            }

    //            quote.YourDetails(user);
    //            //quote.PopUpClose();
    //            quote.ClickContinueButton();
    //            quote.YourPetDetails(user);
    //            quote.ClickContinueButton();
    //            //quote.CoverOption();
    //            //quote.ClickContinueButton();
    //            //quote.Excess();
    //            quote.ClickContinueButton();
    //            //quote.ExtrasButtons();
    //            quote.ClickContinueButton();
    //            //quote.Vacctination();
    //            quote.addAnotherPet();
    //            foreach (var pet in allPets)
    //            {
    //                //quote.DogYourDetailsPet(pet);
    //            }
    //            quote.ClickContinueButton();
    //            //quote.CoverOption();
    //            //quote.ClickContinueButton();
    //            //quote.Excess();
    //            quote.ClickContinueButton();
    //            //quote.ExtrasButtons2();
    //            quote.ClickContinueButton();
    //            //quote.Vacctination();
    //            quote.ClickContinueButton2();
    //            Thread.Sleep(2000);
    //            //quote.KeepingYouInformed();
    //            //quote.Terms();
    //            quote.ClickContinueButton();
    //            Thread.Sleep(2000);
    //            //quote.PaymentYearly();
    //            //quote.ClickContinueButton();
    //            //quote.CCPaymentClick();
    //            //quote.ClickContinueButton();
    //            //Thread.Sleep(2000);
    //            //Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
    //            //quote.CCPaymentEnter();
    //            passedTest(testName, extentTestName);
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        failedTest(testName, extentTestName, e);
    //        throw new Exception("Error: " + e);

    //    }
    //}

    //[Test]
    //public void KCFreeCoverGuest()
    //{
    //    string testName = "KCFreeCoverGuest";
    //    string extentTestName = "KC - Free Cover - Guest";

    //    try
    //    {
    //        test = extent.CreateTest(extentTestName);
    //        var quote = new QuotePage(this);
    //        Driver.Navigate().GoToUrl("https://eqtr-test.kcinsurance.co.uk");
    //        quote.KCFreeCover();
    //        passedTest(testName, extentTestName);
    //    }
    //    catch (Exception e)
    //    {
    //        failedTest(testName, extentTestName, e);
    //        throw new Exception("Error: " + e);
    //    }
    //}


    ////[Test]
    ////public void CheckAllLinks()
    ////{

    ////    var home = new HomePage(this);
    ////    home.GoToPage();
    ////    Thread.Sleep(1000);
    ////    //home.CookieAccept();

    ////List<IWebElement> 
    ////links = Driver.FindElements(By.TagName("a"));

    ////foreach (var item in Driver.FindElements(By.TagName("a")))
    ////{
    ////    Console.WriteLine(item.GetAttribute("herf"));
    ////}
    ////}

    [OneTimeTearDown]
    public void GenerateReport()
    {
        extent.Flush();
    }
}
}
