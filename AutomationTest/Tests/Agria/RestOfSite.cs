﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Diagnostics;
using System.Net;
using System.Collections.Generic;
using System.IO;

namespace Agria
{
    public class RestOfSite : SeleniumTest
    {

        public RestOfSite(string description, Target browser) : base(description, browser) { }

        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocation = ("C:\\temp\\tests\\screenshots\\Agria\\test\\1.2\\");

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\LinkChecker.html");
            htmlReporter.Configuration().Theme = Theme.Standard;
            htmlReporter.Configuration().DocumentTitle = "Anthony Link Checker";
            htmlReporter.Configuration().ReportName = "Anthony Link Checker Report";

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }


        [Test]
        public void CheckAllLinks()
        {


            string testName = "Link Check (Entire Site)";
            test = extent.CreateTest(testName);
            var home = new Home(this);
            home.GoToPage();
            Thread.Sleep(2000);
            // home.GoToSiteMap();
            Thread.Sleep(2000);
            //home.CookieAccept();

            //if (File.Exists(@"C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\AgriaLinkCheckReport.txt"))
            //{
            //    File.Delete(@"C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\AgriaLinkCheckReport.txt");
            //}

            //home.SiteMapLinkCheck();
            var allLinks = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\AgriaLinkCheckReport.txt";
            var wrapLinks = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\wrap.txt";
            var wrapAssests = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\wrap.txt";

            string[] urls = System.IO.File.ReadAllLines(wrapLinks);
            string[] assets = System.IO.File.ReadAllLines(wrapAssests);

            //foreach (string lines in urls)
            //{
            //    Driver.Navigate().GoToUrl(lines);
            //    home.SiteMapLinkCheck();

            //}

            //foreach (string line in urls)
            //{
            //    if (line.Contains("INVALID"))
            //    {
            //        test.Fail("\n" + line);
            //    }
            //    else
            //    {
            //        test.Pass("\n" + line);
            //    }
            //}
            ////test.Pass(url);

            //test.Log(Status.Info, "All the links: " + @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AgriaLinkCheckReport.txt");


            // try
            //{
            //foreach(var url in urls)
            //{
            //    request = (HttpWebRequest)WebRequest.Create(url.GetAttribute("href"));
            //    try
            //    {
            //        var response = (HttpWebResponse)request.GetResponse();
            //        System.Console.WriteLine($"URL: {url.GetAttribute("href")} status is :{response.StatusCode}");
            //    }
            //    catch (WebException f)
            //    {
            //        var errorResponse = (HttpWebResponse)f.Response;
            //        System.Console.WriteLine($"URL: {url.GetAttribute("href")} status is :{errorResponse.StatusCode}");
            //    }
            //}
            //}

            //catch (Exception e)
            //{
            //    TakeFullPageSnapShots(testName);
            //    string fullScreenshot1 = (screenshotLocation + "\\David laptop chrome - " + testName + " 1.png");
            //    string fullScreenshot2 = (screenshotLocation + "\\David laptop chrome - " + testName + " 2.png");
            //    test.Fail(e.StackTrace + "\nScreenshots below: ");
            //    test.AddScreenCaptureFromPath(fullScreenshot1);
            //    test.AddScreenCaptureFromPath(fullScreenshot2);
            //    throw;
            //}

            //var urls = Driver.FindElements(By.TagName("a"));
            //HttpWebRequest request = null;

            //Driver.Navigate().GoToUrl("https://eqtr-test.agriapet.co.uk");

            //try
            //{
            //    foreach (var url in urls)
            //    {
            //        request = (HttpWebRequest)WebRequest.Create(url.GetAttribute("href"));
            //    }

            //    test.Pass("All links: " + request);
            //}

            //catch (Exception e)
            //{
            //    TakeFullPageSnapShots(testName);
            //    string fullScreenshot1 = (screenshotLocation + "\\David laptop chrome - " + testName + " 1.png");
            //    string fullScreenshot2 = (screenshotLocation + "\\David laptop chrome - " + testName + " 2.png");
            //    test.Fail(e.StackTrace + "\nScreenshots below: ");
            //    test.AddScreenCaptureFromPath(fullScreenshot1);
            //    test.AddScreenCaptureFromPath(fullScreenshot2);
            //    throw;
            //}
        }

        //[Test]
        //public void Migration()
        //{
        //    test = extent.CreateTest(testName);
        //    var home = new Home(this);
        //    home.GoToPage();
        //    Thread.Sleep(2000);
        //    // home.GoToSiteMap();
        //    Thread.Sleep(2000);
        //    home.WrapLogin();
        //    home.SearchResources();




            //var migration = "C:\\Users\\anthony.martin\\Desktop\\Captures\\Wrap\\CorporateCollect & Reprocess Page.txt";
            //string[] Resources = System.IO.File.ReadAllLines(migration);

            //foreach (string lines in Resources)
            //{
                
            //}
        }

        //[OneTimeTearDown]
        //public void GenerateReport()
        //{
        //    extent.Flush();
        //}
    }

