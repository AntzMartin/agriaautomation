﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using Test.Automation.Framework;

namespace AutomationTest.Helpers
{
    

    public static class PopulateFields
    {
        public static void TextFieldInput(this IWebDriver driver, IWebElement element, string input)
        {
           
                var name = string.IsNullOrWhiteSpace(input) ? $"input-{new Guid().ToString()}" : input;

                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

                HighlightElement(driver, element);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).Clear();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).SendKeys(input);
            
        }

        public static void TextFieldInput(this IWebDriver driver, string xPath, string input)           
        {
            var element = driver.FindElement(By.XPath(xPath));

            var name = string.IsNullOrWhiteSpace(input) ? $"input-{new Guid().ToString()}" : input;

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

            HighlightElement(driver, element);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).Clear();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).SendKeys(input);
        }



        public static void ClickButton(this IWebDriver driver, IWebElement element)
        {
            HighlightElement(driver, element);

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).Click();
            //if (element.)
            //{
            //    wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("generic-spinner__wheel")));
            //}
        }

        public static void SelectInput(this IWebDriver driver, IWebElement element, string input)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

            HighlightElement(driver, element);
            
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).Click();

            var dropdown = new SelectElement(element);

            dropdown.SelectByText(input);
        }     

        public static void SelectInput(this IWebDriver driver, IWebElement element, int index)
        {
            HighlightElement(driver, element);

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element)).Click();

            var dropdown = new SelectElement(element);

            dropdown.SelectByIndex(index);
        }


        public static void DateInput(this IWebDriver driver, IWebElement element, DateTime input)
        {
            var jsDriver = (IJavaScriptExecutor)driver;

            var highlightJavascript = $"$(arguments[0]).val(\"{input.ToString("dd/MM/yyyy")}\")";

            jsDriver.ExecuteScript(highlightJavascript, new object[] { element });
        }



        private static void HighlightElement(IWebDriver driver, IWebElement element)
        {
            var jsDriver = (IJavaScriptExecutor)driver;

            var highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"", ""background"" : ""yellow"" });";

            jsDriver.ExecuteScript(highlightJavascript, new object[] { element });

            Thread.Sleep(200);

            var clear = @"$(arguments[0]).css({ ""border-width"" : ""0px"", ""border-style"" : ""solid"", ""border-color"" : """", ""background"" : """" });"; ;

            jsDriver.ExecuteScript(clear, new object[] { element });
        }
    }
}
