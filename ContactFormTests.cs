﻿using AutomationExample.PageObjects;
using CsvHelper;
using FrameworkExample.TestDataModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework;
using Test.Automation.Framework.ConfigurationHelpers;

namespace FrameworkExample
{
    public class ContactFormTests : SeleniumTest
    {
        #region Constructors - Do not modify
        public ContactFormTests(string description, Target browser) : base(description, browser) { }
        #endregion

        [TestCaseSource(nameof(GetContactFormCases), new object[] { "TestData\\contactFromNegativeTests.csv" })]
        public void FillInContactFormNegativeTests(ContactFormData data)
        {
            var page = new ContactPage(this);
            page.GoToPage();
            page.InputContactData(data);
            TakeFullPageSnapShots($"Contact form negative test {data.Id}");
        }



        static IEnumerable<ContactFormData> GetContactFormCases(string fileName)
        {
            //Check and see if the file exists first
            var file = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            if (!File.Exists(file))
            {
                throw new FileNotFoundException($"Unable to find the file {fileName}");
            }

            //Now read it in
            using (TextReader fileReader = File.OpenText(file))
            {
                var csv = new CsvReader(fileReader);
                csv.Configuration.HasHeaderRecord = true;
                return csv.GetRecords<ContactFormData>().ToList();
            }
        }
    }


}
