﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;

namespace AutomationExample.PageObjects
{
    public class HomePage : PageWithMenu
    {
        public HomePage(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='about']")]
        private IWebElement about;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='work']")]
        private IWebElement work;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='services']")]
        private IWebElement services;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='clients']")]
        private IWebElement clients;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='ideas']")]
        private IWebElement ideas;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='thoughts']")]
        private IWebElement thoughts;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='careers']")]
        private IWebElement careers;

        [FindsBy(How = How.CssSelector, Using = ".menu-list-item a[href*='contact']")]
        private IWebElement contact;

        public override void GoToPage()
        {
            base.GoToPage();
            wait.Until(ExpectedConditions.ElementToBeClickable(menu));
            Thread.Sleep(2000);
        }

        public AboutPage GoToAboutPage()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(about)).Click();
            return new AboutPage(test);
        }

        public void ValidateMainMav()
        {
            List<KeyValuePair<string, IWebElement>> menuList = new List<KeyValuePair<string, IWebElement>>()
            {
                new KeyValuePair<string, IWebElement>("about", about),
                new KeyValuePair<string, IWebElement>("work", work),
                new KeyValuePair<string, IWebElement>("services", services),
                new KeyValuePair<string, IWebElement>("clients", clients),
                new KeyValuePair<string, IWebElement>("ideas", ideas),
                new KeyValuePair<string, IWebElement>("thoughts", thoughts),
                new KeyValuePair<string, IWebElement>("careers", careers),
                new KeyValuePair<string, IWebElement>("contact", contact)
            };

            menuList.ForEach(x =>
            {
                if (!(x.Value != null && x.Value.Enabled && x.Value.Displayed && x.Value.Text.ToLower() == x.Key && x.Value.GetAttribute("href").ToLower() == Configuration.RunConfiguration.BaseUrl+x.Key+"/")) Assert.Fail($"Error found with menu link '{x.Key}'");
            });            
        }
        
        //public void ValidateMainNavFromGeneralSettings()
        //{
        //    var menuItems = Configuration.GeneralSettings.mainMenuItems as JArray;
        //    if (menuItems!= null)
        //    {
        //        foreach (var item in menuItems)
        //        {
        //            var value = item.ToString().ToLower();
        //            var element = test.Driver.FindElement(By.CssSelector(".menu-list-item a[href*='"+value+"']"));
        //            if (!(element != null && element.Enabled && element.Displayed && element.Text.ToLower() == value && element.GetAttribute("href").ToLower() == Configuration.RunConfiguration.BaseUrl + value + "/")) Assert.Fail($"Error found with menu link \"{value}\"");
        //        }
        //    }
        //}

        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.homePageTitle.ToString(), test.Driver.Title);
        }
    }
}
