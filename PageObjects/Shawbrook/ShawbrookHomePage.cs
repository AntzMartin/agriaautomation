﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;
using AutomationTest.Core.Models;
using AutomationTest.Core.Constants;


namespace AutomationExample.PageObjects
{
    public class ShawbrookHomePage : PageWithMenu
    {
        public ShawbrookHomePage(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;// + "contact-us";
        }


        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }

        [FindsBy(How = How.ClassName, Using = "cookie-popup__button")]
        private IWebElement Cookie;

        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/nav[1]/a[2]")]
        private IWebElement BussinessPage;

        [FindsBy(How = How.XPath, Using = "//a[@class='utility-nav__link']")]
        private IWebElement ContactUs;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Commercial Mortgages')]")]
        private IWebElement AF;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Technology Finance')]")]
        private IWebElement ContactTechnologyFinance;

        [FindsBy(How = How.XPath, Using = "//section[@id='component-2125']//a[@class='btn'][contains(text(),'Get in touch')]")]
        private IWebElement ContactFM;

        [FindsBy(How = How.XPath, Using = "//div[@id=':nl']")]
        private IWebElement EmailBody;

        [FindsBy(How = How.XPath, Using = "//*[@id='identifierId']")]
        private IWebElement EmailAddress;

        [FindsBy(How = How.XPath, Using = "//input[@name='password']")]
        private IWebElement EmailPassword;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Next')]")]
        private IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//a[@class='top-bar__link top-bar__link--active']")]
        private IWebElement Intermediaries;
                


        public void closeCookiePolicy()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Cookie)).Click();
        }

        public void goToBusinessPage()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(BussinessPage)).Click();
        }

        public void SelectContactUs()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(ContactUs)).Click();
        }

        public void SelectIntermediaries()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Intermediaries)).Click();
        }

        public void SelectAF()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(AF)).Click();
        }

        public void SelectContactTechnology()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(ContactTechnologyFinance)).Click();
        }

        public void SelectFM()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(ContactFM)).Click();
        }

        public void EnterEmail()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(EmailAddress)).SendKeys("equatortest58@gmail.com");

        }

        public void Password()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(EmailPassword)).SendKeys("Test1234?");

        }

        public void ClickButton()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(NextButton)).Click();

        }

        public void WriteEmail()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(EmailBody)).SendKeys("Test email body Shawbrook");

        }

        public void NewTab()
        {
            InputSimulator sim = new InputSimulator();
            
            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL},
            new[] { VirtualKeyCode.VK_T }
            );
        }




    }
}