﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;


namespace AutomationExample.PageObjects
{
    public class Bookings : PageWithMenu
    {
        public Bookings(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }

        
        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }

        [FindsBy(How = How.XPath, Using = "//div[@class='quick-book__triggers-container']//button[1]//span[1]")]
        private IWebElement BookARoom;
        
        [FindsBy(How = How.XPath, Using = "//*[@id='QuickBookRoom']/div/div[2]/form/div/div/select/option[9]")]
        private IWebElement Glasgow;

        


        public void BookARoomButton()
        {
                wait.Until(ExpectedConditions.ElementToBeClickable(BookARoom)).Click();
        }

        public void HotelSelect()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Glasgow)).Click();
        }



        static IEnumerable<ContactFormData> GetContactFormCases(string fileName)
        {
            //Check and see if the file exists first
            var file = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            if (!File.Exists(file))
            {
                throw new FileNotFoundException($"Unable to find the file {fileName}");
            }

            //Now read it in
            using (TextReader fileReader = File.OpenText(file))
            {
                var csv = new CsvReader(fileReader);
                csv.Configuration.HasHeaderRecord = true;
                return csv.GetRecords<ContactFormData>().ToList();
            }
        }

        public void ChromeFullScreenshot()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            //sim.Keyboard.ModifiedKeyStroke(
            //new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            //new[] { VirtualKeyCode.VK_2 });

            //Take screenshot
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);

            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            new[] { VirtualKeyCode.VK_P }
            );
            Thread.Sleep(1000);


            sim.Keyboard.TextEntry("screenshot");
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);
        }

        public void PageDown()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            sim.Keyboard.KeyPress(VirtualKeyCode.DOWN);

        }
            //public void CompleteYourDetailsPet()
            //{
            //    PetsNameDetails();
            //}



            //public void InputContactData(ContactFormData data)
            //{
            //    FirstName.SendKeys(data.FName);
            //    LastName.SendKeys(data.LName);
            //    MobileNumber.SendKeys(data.MobileNum);
            //    HomeNumber.SendKeys(data.HomeNum);
            //    EmailAddress.SendKeys(data.Email);
            //}



        }
    }

