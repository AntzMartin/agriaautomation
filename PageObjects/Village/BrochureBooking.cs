﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;
using OpenQA.Selenium.Chrome;

namespace AutomationTest.PageObjects.Village
{
    public class BrochureBooking : PageWithMenu
    {
        public BrochureBooking(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }


        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Book a Room')]")]
        private IWebElement BookRoomButton;

        public void QuickBook()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(BookRoomButton)).Click();
        }
    }
}