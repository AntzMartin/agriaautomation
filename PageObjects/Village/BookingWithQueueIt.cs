﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;
using OpenQA.Selenium.Chrome;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter.Configuration;

namespace AutomationExample.PageObjects
{
    public class BookingWithQueueIt : PageWithMenu
    {
        public BookingWithQueueIt(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }

        
        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }

        [FindsBy(How = How.XPath, Using = "//div[@class='btn btn--full signup-overlay-full-price-button js-signup-trigger']")]
        private IWebElement LikePayingFullPrice;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Book a Room')]")]
        private IWebElement QuickBookButton;
        
        [FindsBy(How = How.Id, Using = "Location")]
        private IWebElement Location;

        [FindsBy(How = How.XPath, Using = "//ul//li[2]//div[1]//div[3]//ul[1]//li[2]//div[1]//div[3]//div[4]//span[1]")]
        private IWebElement Select;

        //This will need updated from time to time as rates often change
        [FindsBy(How = How.XPath, Using = "//span[@id='rate-1262-btn']")]
        private IWebElement Extras;

        [FindsBy(How = How.Id, Using = "footerBarBookBtn")]
        private IWebElement ContinueToPayment;
        
        [FindsBy(How = How.XPath, Using = "//input[@value='Check availability']")]
        private IWebElement CheckAvailability;
        
        [FindsBy(How = How.Id, Using = "PromoCode")]
        private IWebElement PromoCode;

        [FindsBy(How = How.ClassName, Using = "checkout-layout__inner1")]
        private IWebElement AvailabilityCalendarBody;

        [FindsBy(How = How.ClassName, Using = "price-container ng-scope")]
        private IWebElement PriceOnCalendar;

        [FindsBy(How = How.ClassName, Using = "menu-btn-title")]
        private IWebElement HomePageMenuButton;

        [FindsBy(How = How.ClassName, Using = "loader-gif")]
        private IWebElement APILoading;


        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest report;
        public static string screenshotLocation = (@"C:\temp\tests\screenshots\Village\VillageLive\1.0\QuickBookWithoutPromo\");


        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\Village Regression.html");

            //Use if you want to append reports rather than overwrite
            //ExtentReports extent = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\Agria - Q&B.html", false);
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Village";
            htmlReporter.Configuration().ReportName = "Regression";

            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }

        public void passedTest(string testName)
        {
            report = extent.CreateTest(testName);
            test.TakeFullPageSnapShots(testName);
            string passedScreenshot1 = (screenshotLocation + testName);
            string passedScreenshot2 = (screenshotLocation + testName);
            report.AssignCategory("Passed");
            report.AddScreenCaptureFromPath(passedScreenshot1);
            report.AddScreenCaptureFromPath(passedScreenshot2);
        }

        public void failedTest(string testName)
        {
            report = extent.CreateTest(testName);
            test.TakeFullPageSnapShots(testName);
            string failedScreenshot1 = (screenshotLocation + testName);
            string failedScreenshot2 = (screenshotLocation + testName);

            // test.Fail(e.Message);
            report.Fail("Screenshots below of where the test failed: ");
            report.AssignCategory("Failed");
            report.AddScreenCaptureFromPath(failedScreenshot1);
            report.AddScreenCaptureFromPath(failedScreenshot2);
        }

       
        public void APIWait()
        {
            if (APILoading.Displayed)
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("loader-gif")));
                //Thread.Sleep(5000);
            }
        }
        
        public void FullPriceNotification()
        {
            if (LikePayingFullPrice.Displayed)
            {
                LikePayingFullPrice.Click();
            }
        }

        public void QuickBookWithoutPromo()
        {
            string testName = "BookingPage";
            
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(QuickBookButton)).Click();
                wait.Until(ExpectedConditions.ElementToBeClickable(Location)).SendKeys("Glasgow");
                wait.Until(ExpectedConditions.ElementToBeClickable(CheckAvailability)).Click();
                Thread.Sleep(3000);
                passedTest(testName);
            }
            catch (Exception e)
            {
                failedTest(testName);
                throw new Exception("Error: ", e);
            }
        }

        public void RoomRates()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Select)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(Extras)).Click();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueToPayment)).Click();
            Thread.Sleep(5000);
        }

        public void availability()
        {
            if (PriceOnCalendar.Displayed)
            {

            }
        }

        public void QuickBookWithPromo()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(QuickBookButton)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(Location)).SendKeys("Glasgow");
            wait.Until(ExpectedConditions.ElementToBeClickable(PromoCode)).SendKeys("PRA"); //Should be PROA
            wait.Until(ExpectedConditions.ElementToBeClickable(CheckAvailability)).Click();
            Thread.Sleep(10000);
        }

        public void QueueItBookMulti()
        {
            QuickBookButton.Click();
        }

        public void BookOffer()
        {

        }
        
        public void CloseMultipleBrowsers()
        {
            
        }

        public void ChromeFullScreenshot()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            //sim.Keyboard.ModifiedKeyStroke(
            //new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            //new[] { VirtualKeyCode.VK_2 });

            //Take screenshot
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);

            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            new[] { VirtualKeyCode.VK_P }
            );
            Thread.Sleep(1000);


            sim.Keyboard.TextEntry("screenshot");
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);
        }

        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush();
        }
    }
}
