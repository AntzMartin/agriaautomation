﻿using Test.Automation.Framework;
using NUnit.Framework;

namespace AutomationExample.PageObjects
{
    public class AboutPage: PageWithMenu
    {
        public AboutPage(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl +"about/";
        }

        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }
    }
}
