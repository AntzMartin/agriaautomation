﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using System;
using System.Net;
using System.IO;
using System.Linq;
using WindowsInput;
using WindowsInput.Native;

namespace AutomationExample.PageObjects
{
    public class Home : PageWithMenu
    {
        public Home(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }

        [FindsBy(How = How.TagName, Using = "loc")]
        protected IList<IWebElement> allElements;

        [FindsBy(How = How.TagName, Using = "a")]
        protected IList<IWebElement> allElements2;

        [FindsBy(How = How.CssSelector, Using = "href")]
        protected IList<IWebElement> allElements3;

        
        //WRAP*******************************************************************************
        //[FindsBy(How = How.XPath, Using = "//input[@id='edit-submit']")]
        //protected IWebElement login;

        //public bool noResults()
        //{
        //    bool noResultsExist = false;

        //    try
        //    {
        //        if (noResultsDisplayed.Displayed)
        //        {
        //            noResultsExist = true;
        //            return noResultsExist;
        //        }
        //    }
        //    catch
        //    {
        //        noResultsExist = false;
        //    }
        //    return noResultsExist;
        //}


        //public void WrapLogin()
        //{
        //    Thread.Sleep(2000);
        //    if (Url == "https://wrapcorporate.equator-staging.com/user/login")
        //        Console.WriteLine(Url);
        //    {
        //        wait.Until(ExpectedConditions.ElementToBeClickable(username)).SendKeys("admin");
                
        //        wait.Until(ExpectedConditions.ElementToBeClickable(password)).SendKeys("random-secrets-rarely-shared");
        //        Thread.Sleep(2000);
        //        EnterKey();
        //        Thread.Sleep(10000);
        //        //wait.Until(ExpectedConditions.ElementToBeClickable(login)).Click();
                
        //    }
        //}

        //public void SearchResources()
        //{
        //    Thread.Sleep(2000);
        //    string resourcePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Wrap\\Sustainable Textile Docs.txt";
        //    string resultsResourcePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Wrap\\Wrap - Individual Results Resources - Sustainable Textile Docs.txt";
        //    wait.Until(ExpectedConditions.ElementToBeClickable(manage)).Click();
        //    wait.Until(ExpectedConditions.ElementToBeClickable(content)).Click();

        //    if (File.Exists(resultsResourcePath))
        //    {
        //        File.Delete(resultsResourcePath);
        //    }

        //    string[] resources = System.IO.File.ReadAllLines(resourcePath);
        //    foreach (string pages in resources)
        //    {
        //        title.Clear();
        //        wait.Until(ExpectedConditions.ElementToBeClickable(title)).SendKeys(pages);
        //        wait.Until(ExpectedConditions.ElementToBeClickable(filter)).Click();

        //        if (noResults() == true)
        //        {
                    
        //            Console.WriteLine(pages + " **** Not Found - Check Manually ****");
        //            var resourceNotFound = pages.Replace(pages, pages + " **** Not Found - Check Manually ****");
        //            using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsResourcePath, true)) file.WriteLine(resourceNotFound);
        //        }
        //        else
        //        {
        //            Console.WriteLine(pages + " - Found");
        //            //Thread.Sleep(2000);
        //            //wait.Until(ExpectedConditions.ElementToBeClickable(oneResult)).Click();
        //            var foundResource = pages.Replace(pages, pages + " - Found");
        //            using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsResourcePath, true)) file.WriteLine(foundResource);
        //           // wait.Until(ExpectedConditions.ElementToBeClickable(oneResult)).Click();



        //        }
        //        //var migration = "C:\\Users\\anthony.martin\\Desktop\\Captures\\Wrap\\CorporateCollect & Reprocess Page.txt";
        //        //string[] Resources = System.IO.File.ReadAllLines(migration);

        //        //foreach (string pages in Resources)
        //        //{

        //    }

        //    wait.Until(ExpectedConditions.ElementToBeClickable(filter)).Click();

        //}

        //public void EnterKey()
        //{
        //    WindowsInput.InputSimulator sim = new InputSimulator();
        //    sim.Keyboard.KeyDown(VirtualKeyCode.RETURN);
            
        //}




        //WRAP *************************************************************************************************************************


        public void SiteMapLinkCheck()
        {
            //String[] allText = new String[allElements.Count];
            //int i = 0;

            foreach (IWebElement element in allElements2)
            {
                //allText[i++] = element.Text;
                //Console.WriteLine(element.Text);

              //  var elemInstance = (element.Text).Replace("<loc>", "");
               // var elemInstance1 = (elemInstance).Replace("</loc>", " ");

                var tags = element.GetAttribute("href");
                if (tags.Contains("file-type"))
                {
                    Console.WriteLine(tags);
                    //using (System.IO.StreamWriter file = new System.IO.StreamWriter
                    //    (@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\wrapAssests.txt", true))
                    //    file.WriteLine(tags);   
                }





                //allElements3.GetAttribute("href");
                //var elemInstance = (element.Text).Replace("<loc>", "");
                //var elemInstance1 = (elemInstance).Replace("</loc>", " ");

                //elemInstance2.Add(elemInstance1);
                //Console.WriteLine(elemInstance1);
                //}
                //    try
                //    {
                //        // Creates an HttpWebRequest for the specified URL. 
                //        HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(elemInstance1);
                //        // Sends the HttpWebRequest and waits for a response.
                //        HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                //        int statusCode = (int)myHttpWebResponse.StatusCode;
                //        if (statusCode == 200)
                //        {
                //            Console.WriteLine(" - VALID - " + statusCode + " - RESPONSE");
                //            using (System.IO.StreamWriter file = new System.IO.StreamWriter (@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AgriaLinkCheckReport.txt", true))
                //            file.WriteLine(elemInstance1 + " - VALID - " + statusCode + " RESPONSE");

                //            //Releases the resources of the response.
                //            myHttpWebResponse.Close();
                //        }

                //    }
                //    catch (WebException e)
                //    {
                //        Console.WriteLine(e.Message);
                //        Console.WriteLine(" - INVALID - " + e.Message + " - RESPONSE");

                //        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AgriaLinkCheckReport.txt", true))
                //            file.WriteLine(elemInstance1 + "- INVALID - " + e.Message + " RESPONSE");

                //        //Releases the resources of the response.

                //        // file.WriteLine(elemInstance1 + "- " + e.Message.ToUpper());
                //    }
            }

        }
    }
}