﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;
using AutomationTest.Core.Models;
using AutomationTest.Core.Constants;

namespace AutomationExample.PageObjects
{
    public class QuotePage : PageWithMenu
    {
        public QuotePage(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl + "quote";
        }


        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }

        [FindsBy(How = How.ClassName, Using = "generic-spinner__wheel")]
        private IWebElement APILoading;

        [FindsBy(How = How.Id, Using = "SelectDog")]
        private IWebElement Dog;

        [FindsBy(How = How.Id, Using = "SelectCat")]
        private IWebElement Cat;

        [FindsBy(How = How.Id, Using = "SelectRabbit")]
        private IWebElement Rabbit;

        //Your Details
        [FindsBy(How = How.XPath, Using = "//select[@name='title']")]
        private IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//*[@id='stepForm']/div/div/div/div[2]/div[1]/div[1]/div/div[1]/select/option[2]")]
        public IWebElement Mr;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='First name']")]
        private IWebElement FirstName;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Last name']")]
        private IWebElement LastName;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Enter your postcode']")]
        private IWebElement PostCodeField;

        [FindsBy(How = How.XPath, Using = "//a[@class='tab-sections__inline-action address-input__find-button']")]
        private IWebElement PostCodeButton;

        [FindsBy(How = How.XPath, Using = "//li[contains(text(),'58, EQUATOR, ELLIOT STREET, GLASGOW, LANARKSHIRE, ')]")]
        private IWebElement PostCodeSelection;

        [FindsBy(How = How.XPath, Using = "//input[@name='mobileNumber']")]
        private IWebElement MobileNumber;

        [FindsBy(How = How.XPath, Using = "//input[@name='homePhoneNumber']")]
        private IWebElement HomeNumber;

        [FindsBy(How = How.XPath, Using = "//input[@name='emailAddress']")]
        private IWebElement EmailAddress;

        [FindsBy(How = How.XPath, Using = "//label[@id='confirmAgeLbl']")]
        private IWebElement Over18;

        [FindsBy(How = How.Id, Using = "continueBtn")]
        private IWebElement ContinueButton;

        [FindsBy(How = How.Id, Using = "ContinueBtn")]
        private IWebElement ContinueButton2;

        [FindsBy(How = How.Name, Using = "pet_name")]
        private IWebElement PetNameXpath;

        [FindsBy(How = How.Id, Using = "dobPicker")]
        private IWebElement DOBpicker;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='›'])[1]/following::span[5]")]
        private IWebElement DOByear;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='Nov'])[1]/following::span[1]")]
        private IWebElement DOBmonth;

        [FindsBy(How = How.XPath, Using = "//body[@id='ng-app']/div/div/div/section/div/div/form/div/div/div/div[2]/div[2]/div/div/div/div/div/div/table/tbody/tr[4]/td[6]/span")]
        private IWebElement DOBday;

        [FindsBy(How = How.Id, Using = "petBreed")]
        private IWebElement PetBreed;

        [FindsBy(How = How.Id, Using = "9aa8b61c-145b-4260-bad0-cbe4f464087d")]
        private IWebElement Male;

        [FindsBy(How = How.Id, Using = "206716fc-8baa-4eab-8c72-ffcb42e39d52")]
        private IWebElement Female;

        [FindsBy(How = How.XPath, Using = "//input[@id='petCostField']")]
        private IWebElement cost;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input']//label[@class='btn'][contains(text(),'Yes')]")]
        private IWebElement IllnessYes;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input']//label[@class='btn'][contains(text(),'No')]")]
        private IWebElement IllnessNo;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'Yes')]")]
        private IWebElement LiableYes;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'No')]")]
        private IWebElement LiableNo;

        [FindsBy(How = How.XPath, Using = "//div[@class='cover-options grid grid--half']//div[1]//div[1]//div[2]//div[1]//label[1]")]
        private IWebElement LowCover;

        [FindsBy(How = How.XPath, Using = "//div[@class='cover-options grid grid--half']//div[2]//div[1]//div[2]//div[1]//label[1]")]
        private IWebElement HighCover;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/label[1]")]
        private IWebElement HighExcess;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/label[1]")]
        private IWebElement LowExcess;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/a[2]")]
        private IWebElement OverseasTravel;

        [FindsBy(How = How.XPath, Using = "//a[@class='btn cover-extra__button--remove']")]
        private IWebElement OverseasTravelRemove;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/a[2]")]
        private IWebElement DeathAndLoss;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/a[2]")]
        private IWebElement BreadingRisks;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[6]/div[2]/a[2]")]
        private IWebElement BoardingFees;

        [FindsBy(How = How.Id, Using = "startDatePicker")]
        private IWebElement PolicyStart;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='‹'])[1]/following::th[2]")]
        private IWebElement PolicyStartYear;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[1]/following::span[9]")]
        private IWebElement Date;

        [FindsBy(How = How.Id, Using = "vaccinationDatePicker")]
        private IWebElement VaccinationDate;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Name']")]
        private IWebElement VetName;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Location']")]
        private IWebElement VetLocation;

        [FindsBy(How = How.XPath, Using = "//a[@id='vetSearchBtn']")]
        private IWebElement VetSearchButton;

        [FindsBy(How = How.XPath, Using = "//li[@class='multiselect__item ng-binding ng-scope']")]
        private IWebElement VetLocationSelect;


        [FindsBy(How = How.XPath, Using = "//select[@name='campaignSelector']")]
        private IWebElement HowDidYouHear;

        [FindsBy(How = How.XPath, Using = "//label[@for='c1']")]
        private IWebElement NoThanks;

        [FindsBy(How = How.XPath, Using = "//label[@for='c2']")]
        private IWebElement YesPlease;

        [FindsBy(How = How.XPath, Using = "//label[@class='checkbox-before ng-binding']")]
        private IWebElement IConfirm;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='›'])[1]/following::span[5]")]
        private IWebElement VaccinationYear;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='Jun'])[1]/following::span[1]")]
        private IWebElement VaccinationMonth;

        [FindsBy(How = How.XPath, Using = "(.//*[normalize-space(text()) and normalize-space(.)='So we know when the next one is due'])[1]/following::div[2]")]
        private IWebElement VaccinationDay;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Annually')]")]
        private IWebElement Yearly;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Monthly')]")]
        private IWebElement Monthly;

        [FindsBy(How = How.XPath, Using = "//select[@name='preferredDayOfPayment-']")]
        private IWebElement MonthlyDate;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Direct debit')]")]
        private IWebElement DirectDebit;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'Credit/Debit card')]")]
        private IWebElement CreditCard;

        [FindsBy(How = How.Id, Using = "card.cardNumber")]
        private IWebElement CreditCardNumberField;

        [FindsBy(How = How.Id, Using = "card.cardHolderName")]
        private IWebElement CreditCardNameField;

        [FindsBy(How = How.Id, Using = "card.expiryMonth")]
        private IWebElement CreditCardMonthField;

        [FindsBy(How = How.Id, Using = "card.expiryYear")]
        private IWebElement CreditCardYearField;

        [FindsBy(How = How.Name, Using = "pay")]
        private IWebElement CreditCardSubmit;

        [FindsBy(How = How.XPath, Using = "//input[@name='accountHolderName']")]
        private IWebElement DirectDebitNameField;

        [FindsBy(How = How.XPath, Using = "//input[@name='sortCode1']")]
        private IWebElement DirectDebitSortCodeField;

        [FindsBy(How = How.XPath, Using = "//input[@name='accountNumber']")]
        private IWebElement DirectDebitAccountField;

        [FindsBy(How = How.Id, Using = "CompleteDirectDebitBtn")]
        private IWebElement DirectDebitCompleteButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'No')]")]
        private IWebElement HomeCatNo;

        [FindsBy(How = How.Id, Using = "//div[@class='tab-sections__input-row radio-input ng-scope']//label[@class='btn'][contains(text(),'Yes')]")]
        private IWebElement HomeCatYes;

        [FindsBy(How = How.Id, Using = "//input[@id='promoCode']")]
        private IWebElement InputPromoCode;

        [FindsBy(How = How.Id, Using = "//a[@id='applyPromocodeBtn']")]
        private IWebElement ApplyPromoCode;

        [FindsBy(How = How.XPath, Using = "//*[@id='yie - close - button - ba29a23a - d8d1 - 5ad1 - 8e9b - 279e32e048c8']")]
        private IWebElement CloseButton;

        [FindsBy(How = How.XPath, Using = "//a[@id='addAnotherPetBtn']")]
        private IWebElement MultiPet;
        
        ///############## KC Elements ###################
        [FindsBy(How = How.XPath, Using = "//a[@class='top-bar__nav-link'][contains(text(),'Breeders')]")]
        private IWebElement KCBreeders;

        [FindsBy(How = How.XPath, Using = "//a[@class='logo-bar__button logo-bar__button--get-quote']")]
        private IWebElement KCActiveFreeCover;

        [FindsBy(How = How.XPath, Using = "//input[@name='email']")]
        private IWebElement KCEnterEmail;
    /// <summary>
    /// 
    /// </summary>
        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement ContinueAsGuest;
       
        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement title;

        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement firstName;

        [FindsBy(How = How.Id, Using = "enrolBtn")]
        private IWebElement Surname;

        [FindsBy(How = How.XPath, Using = "//input[@id='addressSearchPostcode']")]
        private IWebElement EnterPostcode;

        [FindsBy(How = How.XPath, Using = "//a[@class='tab-sections__inline-action address-input__find-button']")]
        private IWebElement FindPostcode;

        [FindsBy(How = How.XPath, Using = "//li[contains(text(),'58, EQUATOR, ELLIOT STREET, GLASGOW, LANARKSHIRE, ')]")]
        private IWebElement SelectPostcode;

        [FindsBy(How = How.Id, Using = "//input[@id='homePhoneNumber']")]
        private IWebElement PhoneNumber;

        [FindsBy(How = How.Id, Using = "//input[@id='dobPicker']")]
        private IWebElement BreederDOB;



        
        /// <summary>
        /// 
        /// </summary>

        [FindsBy(How = How.Id, Using = "//button[@id='continueBtn']")]
        private IWebElement KCSubmitEmail;

        [FindsBy(How = How.XPath, Using = "//input[@name='agriaId']")]
        private IWebElement KCEnterPostcode;

        [FindsBy(How = How.Id, Using = "//button[@id='loginBtn']")]
        private IWebElement KCSubmitPostcode;

        [FindsBy(How = How.XPath, Using = "//label[@id='noRegNumberLbl']")]
        private IWebElement KCCheckDontKnow;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='e.g. 'Snowball'']")]
        private IWebElement KCPetName;

        [FindsBy(How = How.XPath, Using = "//input[@id='petBreed']")]
        private IWebElement KCPetBreed;

        
        public void APIWait()
        {
            if (APILoading.Displayed)
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("generic-spinner__wheel")));
                //Thread.Sleep(1000);
            }
        }

        public void PopUpCheck()
        {
            var iFrameElement = test.Driver.FindElements(By.Id("yie-*")).FirstOrDefault();
            
            if (iFrameElement == null)
            {
                return;
            }

            var iFrameActive = test.Driver.SwitchTo().Frame(iFrameElement);

            var overLayOpen = iFrameActive.FindElements(By.Id("yie-overlay-*")).FirstOrDefault();

            if (overLayOpen.Displayed)
            {
                Thread.Sleep(3000);
                wait.Until(ExpectedConditions.ElementToBeClickable(CloseButton)).Click();
            }
        }     

        public void SelectDogQuote()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Dog)).Click();
        }

        public void SelectCatQuote()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Cat)).Click();
        }

        public void SelectRabbitQuote()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Rabbit)).Click();
        }

        //Your details
        public void SelectTitle(string userTitle)
        {
            switch (userTitle)
            {
                case "Mr":
                    wait.Until(ExpectedConditions.ElementToBeClickable(Mr)).Click();
                    break;
                case "Mrs":
                    wait.Until(ExpectedConditions.ElementToBeClickable(Mr)).Click();
                    break;
                default:
                    wait.Until(ExpectedConditions.ElementToBeClickable(Mr)).Click();
                    break;
            }

        }

        public void FirstNameDetails(string firstName)
        {
            var name = string.IsNullOrWhiteSpace(firstName) ? "Anthony" : firstName;
            wait.Until(ExpectedConditions.ElementToBeClickable(FirstName)).SendKeys(name);
        }

        public void LastNameDetails(string lastName)
        {
            var userLastName = string.IsNullOrWhiteSpace(lastName) ? "Test" : lastName;

            wait.Until(ExpectedConditions.ElementToBeClickable(LastName)).SendKeys(userLastName);
        }

        public void PostCodeEntry(string postCode = "G3 8DZ")
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(PostCodeField)).SendKeys(postCode);
            wait.Until(ExpectedConditions.ElementToBeClickable(PostCodeButton)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(PostCodeSelection)).Click();
        }

        public void MobileNumberDetails(string mobileNumber = "07777777777")
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(MobileNumber)).SendKeys(mobileNumber);
        }

        public void HomeNumberDetails()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(HomeNumber)).SendKeys("01411111111");
        }

        public void EmailDetails(string personalEmail = "anthony.martin@eqtr.com")
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(EmailAddress)).SendKeys(personalEmail);
        }
        public void EmailDetailsNothing()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(EmailAddress)).SendKeys("nothing@eqtr.com");
        }


        public void ClickOver18()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Over18)).Click();
        }

        public void ClickContinueButton()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueButton)).Click();
            //Thread.Sleep(3000);
            APIWait();
        }

        public void ClickContinueButton2()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueButton2)).Click();
            //Thread.Sleep(3000);
            APIWait();
        }

        //Pets details
        public void PetNameField(string petName = "Drax")
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(PetNameXpath)).SendKeys(petName);
        }

        public void DOBSelection()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(DOBpicker)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(DOByear)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(DOBmonth)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(DOBday)).Click();

        }


        public void PetBreedSelection(string breed = "Crossbreed")
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(PetBreed)).SendKeys(breed);
        }

        public void Sex()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(Female)).Click();
            //thread.sleep(500);
            wait.Until(ExpectedConditions.ElementToBeClickable(Male)).Click();
            //thread.sleep(500);
        }

        public void CostOfPet(string petCost = "1")
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(cost)).SendKeys(petCost);
        }

        public void HomeCat()
        {
            //wait.Until(ExpectedConditions.ElementToBeClickable(HomeCatYes)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(HomeCatNo)).Click();

        }

        public void SufferedIllness()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(IllnessYes)).Click();
            //thread.sleep(500);
            wait.Until(ExpectedConditions.ElementToBeClickable(IllnessNo)).Click();
        }

        public void Liable()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(LiableYes)).Click();
            //thread.sleep(500);
            wait.Until(ExpectedConditions.ElementToBeClickable(LiableNo)).Click();
        }

        public void CoverOption()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(LowCover)).Click();
            //thread.sleep(500);
            wait.Until(ExpectedConditions.ElementToBeClickable(HighCover)).Click();

        }

        public void Excess()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(LowExcess)).Click();
            //thread.sleep(500);
            wait.Until(ExpectedConditions.ElementToBeClickable(HighExcess)).Click();


        }

        public void ExtrasButtons()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(OverseasTravel)).Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(DeathAndLoss)).Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(BreadingRisks)).Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(BoardingFees)).Click();

        }

        public void ExtrasButtons2()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(OverseasTravelRemove)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(DeathAndLoss)).Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(BreadingRisks)).Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(BoardingFees)).Click();

        }



        public void Policy()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(PolicyStart)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(PolicyStartYear)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(Date)).Click();
        }

        public void Vacctination()
        {
            Thread.Sleep(1000);
            wait.Until(ExpectedConditions.ElementToBeClickable(VaccinationDate)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(VaccinationYear)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(VaccinationMonth)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(VaccinationDay)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(VetName)).SendKeys("Ark");
            wait.Until(ExpectedConditions.ElementToBeClickable(VetLocation)).SendKeys("ML1 4TA");
            wait.Until(ExpectedConditions.ElementToBeClickable(VetSearchButton)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(HowDidYouHear)).SendKeys("Event");
            //Thread.Sleep(2000);   
        }

        public void addAnotherPet()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(MultiPet)).Click();
        }

        public void KeepingYouInformed()
        {
            Thread.Sleep(1000);
            wait.Until(ExpectedConditions.ElementToBeClickable(YesPlease)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(NoThanks)).Click();

        }

        public void PromoCode()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(InputPromoCode)).SendKeys("crufts");
            wait.Until(ExpectedConditions.ElementToBeClickable(ApplyPromoCode)).Click();
        }

        public void Terms()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(IConfirm)).Click();
        }

        public void DogYourDetails(QuoteAndBuyUserModel user)
        {
            SelectTitle(user.Title);
            FirstNameDetails(user.Firstname);
            LastNameDetails(user.Lastname);
            PostCodeEntry(user.Postcode);
            MobileNumberDetails(user.MobileNumber);
            HomeNumberDetails();
            EmailDetails(user.Email);
            //EmailDetailsNothing();
            ClickOver18();

        }

        public void DogYourDetailsPet(QuoteAndBuyUserModel user)
        {
            PetNameField(user.PetName);
            DOBSelection();
            PetBreedSelection(user.DogBreed);
            Sex();
            CostOfPet(user.CostOfDog);
            SufferedIllness();
            Liable();

        }

        public void DogYourDetailsPet(QuoteAndBuyPetModel pet)
        {
            PetNameField(pet.PetName);
            DOBSelection();
            wait.Until(ExpectedConditions.ElementToBeClickable(PetBreed)).Clear();
            PetBreedSelection(pet.DogBreed);
            Sex();
            CostOfPet(pet.CostOfDog);
            SufferedIllness();
            Liable();

        }

        public void CatYourDetails()
        {
            //SelectTitle();
            //FirstNameDetails();
            //LastNameDetails();
            //PostCodeEntry();
            //MobileNumberDetails();
            //HomeNumberDetails();
            //EmailDetails();
            ////EmailDetailsNothing();
            //ClickOver18();

        }

        public void CatYourDetailsPet()
        {
            PetNameField();
            DOBSelection();
            PetBreedSelection();
            Sex();
            CostOfPet();
            HomeCat();
            SufferedIllness();

        }

        public void RabbitYourDetails()
        {
            //SelectTitle();
            //FirstNameDetails();
            //LastNameDetails();
            PostCodeEntry();
            MobileNumberDetails();
            HomeNumberDetails();
            EmailDetails();
            //EmailDetailsNothing();
            ClickOver18();

        }

        public void RabbitYourDetailsPet()
        {
            PetNameField();
            DOBSelection();
            PetBreedSelection();
            Sex();
            CostOfPet();
            SufferedIllness();
        }

        public void PaymentYearly()
        {
            APIWait();
            wait.Until(ExpectedConditions.ElementToBeClickable(Yearly)).Click();
        }

        public void PaymentMonthly()
        {
            APIWait();
            wait.Until(ExpectedConditions.ElementToBeClickable(Monthly)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(MonthlyDate)).Click();
            InputSimulator sim = new InputSimulator();
            sim.Keyboard.KeyPress(VirtualKeyCode.VK_1);
        }

        public void CCPaymentClick()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(CreditCard)).Click();

        }

        public void CCPaymentEnter()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardNumberField)).SendKeys("5555444433331111");
            wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardNameField)).SendKeys("Anthony Test");
            wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardMonthField)).SendKeys("05");
            wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardYearField)).SendKeys("2025");
            wait.Until(ExpectedConditions.ElementToBeClickable(CreditCardSubmit)).Click();
        }

        public void DDPayment()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebit)).Click();
        }

        public void DDPaymentEnter()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitNameField)).SendKeys("Anthony Test");
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitSortCodeField)).SendKeys("560036");
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitAccountField)).SendKeys("40308669");
            wait.Until(ExpectedConditions.ElementToBeClickable(DirectDebitCompleteButton)).Click();

        }

        public void KCFreeCover()
        {
            int count = 59;

            wait.Until(ExpectedConditions.ElementToBeClickable(KCBreeders)).Click();
            APIWait();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCActiveFreeCover)).Click();
            APIWait();
            Thread.Sleep(2000);
            count++;
            wait.Until(ExpectedConditions.ElementToBeClickable(KCEnterEmail)).SendKeys("test"+count+"@eqtr.com");
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCSubmitEmail)).Click();
            APIWait();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(ContinueAsGuest)).Click();
            

            wait.Until(ExpectedConditions.ElementToBeClickable(KCEnterPostcode)).SendKeys("G3 8DZ");
            wait.Until(ExpectedConditions.ElementToBeClickable(KCSubmitPostcode)).Click();
            APIWait();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCCheckDontKnow)).Click();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(KCPetName)).SendKeys("Fluffy");
            wait.Until(ExpectedConditions.ElementToBeClickable(KCPetBreed)).SendKeys("American Lamalese");
        }
        
        static IEnumerable<ContactFormData> GetContactFormCases(string fileName)
        {
            //Check and see if the file exists first
            var file = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            if (!File.Exists(file))
            {
                throw new FileNotFoundException($"Unable to find the file {fileName}");
            }

            //Now read it in
            using (TextReader fileReader = File.OpenText(file))
            {
                var csv = new CsvReader(fileReader);
                csv.Configuration.HasHeaderRecord = true;
                return csv.GetRecords<ContactFormData>().ToList();
            }
        }

        public void ChromeFullScreenshot()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            //sim.Keyboard.ModifiedKeyStroke(
            //new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            //new[] { VirtualKeyCode.VK_2 });

            //Take screenshot
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);

            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            new[] { VirtualKeyCode.VK_P }
            );
            Thread.Sleep(1000);


            sim.Keyboard.TextEntry(">screenshot");
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);
        }

        public void PageDown()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            sim.Keyboard.KeyPress(VirtualKeyCode.DOWN);

        }
        //public void CompleteYourDetailsPet()
        //{
        //    PetsNameDetails();
        //}



        //public void InputContactData(ContactFormData data)
        //{
        //    FirstName.SendKeys(data.FName);
        //    LastName.SendKeys(data.LName);
        //    MobileNumber.SendKeys(data.MobileNum);
        //    HomeNumber.SendKeys(data.HomeNum);
        //    EmailAddress.SendKeys(data.Email);
        //}



    }
}

