﻿using Test.Automation.Framework;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;

namespace AutomationExample.PageObjects
{
    public abstract class BasePage
    {
        protected WebDriverWait wait;
        protected SeleniumTest test;
        public string Url { get; set; }

        public BasePage(SeleniumTest test)
        {
            this.test = test;
            wait = new WebDriverWait(test.Driver, TimeSpan.FromSeconds(15));
            PageFactory.InitElements(test.Driver, this);
        }

        public virtual void GoToPage()
        {
            test.Driver.Navigate().GoToUrl(Url);
        }

        public virtual void GoToSiteMap()
        {             
            test.Driver.Navigate().GoToUrl(Url + "sitemap.xml");
        }
    }
}
