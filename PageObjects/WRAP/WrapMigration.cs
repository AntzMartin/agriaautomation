﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;
using System.Net;
using System.Collections;

namespace AutomationExample.PageObjects
{
    public class WrapMigration : PageWithMenu
    {
        public WrapMigration(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }

        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);
        }



        //this section should be in a seperate file for Wrap****************************************************

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-name']")]
        protected IWebElement username;

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-pass']")]
        protected IWebElement password;

        [FindsBy(How = How.XPath, Using = "//input[@value='Log in']")]
        protected IWebElement login;

        [FindsBy(How = How.XPath, Using = "//a[@class='toolbar-icon toolbar-icon-system-admin-content']")]
        protected IWebElement content;

        [FindsBy(How = How.XPath, Using = "//select[@id='edit-type']")]
        protected IWebElement filter;

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-submit-content']")]
        protected IWebElement filterSubmit;


        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Next ›')]")]
        protected IWebElement next;


        [FindsBy(How = How.XPath, Using = "//input[@id='edit-title']")]
        protected IWebElement title;

        [FindsBy(How = How.XPath, Using = "//td[@class='views-empty']")]
        protected IWebElement noResultsDisplayed;

        [FindsBy(How = How.XPath, Using = "//a[@id='toolbar-item-administration']")]
        protected IWebElement manage;

        [FindsBy(How = How.XPath, Using = "//td[@class='views-field views-field-title']")]
        protected IWebElement oneResult;

        [FindsBy(How = How.ClassName, Using = "views-field views-field-node-bulk-form")]
        protected IWebElement multipleResults;

        [FindsBy(How = How.TagName, Using = "td")]
        protected IList<IWebElement> allElements;

        [FindsBy(How = How.TagName, Using = "tr")]
        protected IList<IWebElement> allElements2;

        [FindsBy(How = How.CssSelector, Using = "href")]
        protected IList<IWebElement> allElements3;

        [FindsBy(How = How.XPath, Using = ".//div[@class='views-row']")]
        protected IList<IWebElement> allElements4;

        [FindsBy(How = How.XPath, Using = "//ul[@class='tabs primary']//a[contains(text(),'Edit')]")]
        protected IWebElement editPage;

        [FindsBy(How = How.XPath, Using = "//summary[contains(text(),'Hero Subpage')]")]
        protected IWebElement clickHero;

        [FindsBy(How = How.XPath, Using = "//a[@class='entity-browser-handle entity-browser-iframe']")]
        protected IWebElement clickMedia;

        [FindsBy(How = How.XPath, Using = "//*[@id='entity - browser - media - entity - browser - form']/nav/ul/li[5]/a")]
        protected IWebElement clickExistingFiles;

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-name']")]
        protected IWebElement MediaNameInput;

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-submit-media-entity-browser']")]
        protected IWebElement apply;
        //input[@id='edit-submit-media-entity-browser']

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-submit']")]
        protected IWebElement submitMedia;

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-submit']")]
        protected IWebElement savePage;

        [FindsBy(How = How.XPath, Using = "//a[@class='button-alt button-alt--no-background agree-button eu-cookie-compliance-agree-button']")]
        protected IWebElement cookie;

        

        //WRAP*******************************************************************************
        //[FindsBy(How = How.XPath, Using = "//input[@id='edit-submit']")]
        //protected IWebElement login;

        //public bool noResults()
        //{
        //    bool noResultsExist = false;

        //    try
        //    {
        //        if (noResultsDisplayed.Displayed)
        //        {
        //            noResultsExist = true;
        //            return noResultsExist;
        //        }
        //    }
        //    catch
        //    {
        //        noResultsExist = false;
        //    }
        //    return noResultsExist;
        //}


        //public void WrapLogin()
        //{
        //    Thread.Sleep(2000);
        //    if (Url == "https://wrapcorporate.equator-staging.com/user/login")
        //        Console.WriteLine(Url);
        //    {
        //        wait.Until(ExpectedConditions.ElementToBeClickable(username)).SendKeys("admin");

        //        wait.Until(ExpectedConditions.ElementToBeClickable(password)).SendKeys("random-secrets-rarely-shared");
        //        Thread.Sleep(2000);
        //        EnterKey();
        //        Thread.Sleep(10000);
        //        //wait.Until(ExpectedConditions.ElementToBeClickable(login)).Click();

        //    }
        //}

        //public void SearchResources()
        //{
        //    Thread.Sleep(2000);
        //    string resourcePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Wrap\\Sustainable Textile Docs.txt";
        //    string resultsResourcePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Wrap\\Wrap - Individual Results Resources - Sustainable Textile Docs.txt";
        //    wait.Until(ExpectedConditions.ElementToBeClickable(manage)).Click();
        //    wait.Until(ExpectedConditions.ElementToBeClickable(content)).Click();

        //    if (File.Exists(resultsResourcePath))
        //    {
        //        File.Delete(resultsResourcePath);
        //    }

        //    string[] resources = System.IO.File.ReadAllLines(resourcePath);
        //    foreach (string pages in resources)
        //    {
        //        title.Clear();
        //        wait.Until(ExpectedConditions.ElementToBeClickable(title)).SendKeys(pages);
        //        wait.Until(ExpectedConditions.ElementToBeClickable(filter)).Click();

        //        if (noResults() == true)
        //        {

        //            Console.WriteLine(pages + " **** Not Found - Check Manually ****");
        //            var resourceNotFound = pages.Replace(pages, pages + " **** Not Found - Check Manually ****");
        //            using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsResourcePath, true)) file.WriteLine(resourceNotFound);
        //        }
        //        else
        //        {
        //            Console.WriteLine(pages + " - Found");
        //            //Thread.Sleep(2000);
        //            //wait.Until(ExpectedConditions.ElementToBeClickable(oneResult)).Click();
        //            var foundResource = pages.Replace(pages, pages + " - Found");
        //            using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsResourcePath, true)) file.WriteLine(foundResource);
        //           // wait.Until(ExpectedConditions.ElementToBeClickable(oneResult)).Click();



        //        }
        //        //var migration = "C:\\Users\\anthony.martin\\Desktop\\Captures\\Wrap\\CorporateCollect & Reprocess Page.txt";
        //        //string[] Resources = System.IO.File.ReadAllLines(migration);

        //        //foreach (string pages in Resources)
        //        //{

        //    }

        //    wait.Until(ExpectedConditions.ElementToBeClickable(filter)).Click();

        //}

        //public void EnterKey()
        //{
        //    WindowsInput.InputSimulator sim = new InputSimulator();
        //    sim.Keyboard.KeyDown(VirtualKeyCode.RETURN);

        //}


        string listOfPages = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\missingWRAPpages.txt";
        string resultsOfPages = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\missingWRAPpages - results.txt";

        string AgreementPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AgreementPages.txt";
        string ArticlePages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\ArticlePages.txt";
        string AssetPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AssetPages.csv";
        string BasicPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\BasicPagesAfter.csv";
        string CampaignPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\CampaignPages.csv";
        string EventPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\EventPages.txt";
        string FAQPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\FAQPages.txt";
        string FAQGroupPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\FAQGroupPages.txt";
        string NewsPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\NewsPages.csv";
        string PartnerMessagePages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\PartnerMessagePages.txt";
        string ResourcePages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\ResourcePages.csv";
        string VacancyPages = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\VacancyPages.txt";
        //string hotelsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GKMissingHotelsOnly.txt";
        //string resultsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingResults-AfterFix.txt";


        public void AcceptCookies()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(cookie)).Click();
        }


        public void AdminLogin()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(username)).SendKeys("admin");
            wait.Until(ExpectedConditions.ElementToBeClickable(password)).SendKeys("random-secrets-rarely-shared");
            wait.Until(ExpectedConditions.ElementToBeClickable(login)).Click();
        }

        public void EditBasic()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(editPage)).Click();
        }

        public void selectMedia()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(clickHero)).Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(clickMedia)).Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(clickExistingFiles)).Click();
        }


        public void applyClick()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(apply)).Click();
            Thread.Sleep(2000);
           // wait.Until(ExpectedConditions.ElementToBeClickable(savePage)).Click();
        }

        public void Submit()
        {
           
            var imageSelected = allElements4;
            imageSelected.FirstOrDefault().Click();
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(submitMedia)).Click();
            Thread.Sleep(2000);
        }
        


        public void Agreement()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Agreement");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void Article()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Article");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void Asset()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Asset");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void BasicPage()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Basic");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void Campaign()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Campaign");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void Event()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Event");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void FAQ()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("FAQ");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void FAQGroup()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("FAQ Group");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void News()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("News");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void PartnerMessage()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Partner");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void Resources()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Resources");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void Vacancy()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(filter)).SendKeys("Vacancy");
            wait.Until(ExpectedConditions.ElementToBeClickable(filterSubmit)).Click();
        }

        public void GetLinksAgreementPages()
        {

            using (System.IO.StreamWriter fileAgreementPages = new System.IO.StreamWriter
                            (AgreementPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();
                   // var tableDate = element.FindElements(By.XPath(".//td[@class='priority-low views-field views-field-changed is-active']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();
                       

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href"); 
                            Console.WriteLine(tags);

                            fileAgreementPages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksArticlePages()
        {

            using (System.IO.StreamWriter fileArticlePages = new System.IO.StreamWriter
                            (ArticlePages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href");

                            Console.WriteLine(tags);

                            fileArticlePages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksAssetPages()
        {

            using (System.IO.StreamWriter fileAssetPages = new System.IO.StreamWriter
                            (AssetPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    var date = "Cannot Retrive Date";
                    var name = "Cannot Retrive Name";
                    var status = "Unknown";


                    var elementTdList = element.FindElements(By.XPath(".//td")).ToList();

                    if (elementTdList.Any() && elementTdList.Count > 4)
                    {

                        status = elementTdList.ElementAt(4).Text;

                    }


                    if (elementTdList.Any() && elementTdList.Count > 5)
                    {

                        date = elementTdList.ElementAt(5).Text;

                    }


                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            name = tableLink.GetAttribute("href");

                            Console.WriteLine(name);


                        }

                        fileAssetPages.WriteLine($"{name},{status},{date}");

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksBasicPages()
        {
            
            using (System.IO.StreamWriter fileBasicPages = new System.IO.StreamWriter
                            (BasicPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    var date = "Cannot Retrive Date";
                    var name = "Cannot Retrive Name";
                    var status = "Unknown";
                   

                    var elementTdList = element.FindElements(By.XPath(".//td")).ToList();

                    if (elementTdList.Any() && elementTdList.Count > 4)
                    {

                        status = elementTdList.ElementAt(4).Text;

                    }


                    if (elementTdList.Any() && elementTdList.Count > 5)
                    {

                        date = elementTdList.ElementAt(5).Text;
                      
                    }


                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            name = tableLink.GetAttribute("href");

                            Console.WriteLine(name);

                           
                        }

                        fileBasicPages.WriteLine($"{name},{status},{date}");

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksCampaignPages()
        {

            using (System.IO.StreamWriter fileCampaignPages = new System.IO.StreamWriter
                            (CampaignPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    var date = "Cannot Retrive Date";
                    var name = "Cannot Retrive Name";
                    var status = "Unknown";


                    var elementTdList = element.FindElements(By.XPath(".//td")).ToList();

                    if (elementTdList.Any() && elementTdList.Count > 4)
                    {

                        status = elementTdList.ElementAt(4).Text;

                    }


                    if (elementTdList.Any() && elementTdList.Count > 5)
                    {

                        date = elementTdList.ElementAt(5).Text;

                    }


                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            name = tableLink.GetAttribute("href");

                            Console.WriteLine(name);


                        }

                        fileCampaignPages.WriteLine($"{name},{status},{date}");

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksEventPages()
        {

            using (System.IO.StreamWriter fileEventPages = new System.IO.StreamWriter
                            (EventPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href");

                            Console.WriteLine(tags);

                            fileEventPages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksFAQPages()
        {

            using (System.IO.StreamWriter fileFAQPages = new System.IO.StreamWriter
                            (FAQPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href");

                            Console.WriteLine(tags);

                            fileFAQPages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksFAQGroupPages()
        {

            using (System.IO.StreamWriter fileFAQGroupPages = new System.IO.StreamWriter
                            (FAQGroupPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href");

                            Console.WriteLine(tags);

                            fileFAQGroupPages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksNewsPages()
        {

            using (System.IO.StreamWriter fileBasicPages = new System.IO.StreamWriter
                            (NewsPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    var date = "Cannot Retrive Date";
                    var name = "Cannot Retrive Name";
                    var status = "Unknown";


                    var elementTdList = element.FindElements(By.XPath(".//td")).ToList();

                    if (elementTdList.Any() && elementTdList.Count > 4)
                    {

                        status = elementTdList.ElementAt(4).Text;

                    }


                    if (elementTdList.Any() && elementTdList.Count > 5)
                    {

                        date = elementTdList.ElementAt(5).Text;

                    }


                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            name = tableLink.GetAttribute("href");

                            Console.WriteLine(name);


                        }

                        fileBasicPages.WriteLine($"{name},{status},{date}");

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksPartnerMessagePages()
        {

            using (System.IO.StreamWriter filePartnerMessagePages = new System.IO.StreamWriter
                            (PartnerMessagePages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href");

                            Console.WriteLine(tags);

                            filePartnerMessagePages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksResourcePages()
        {

            using (System.IO.StreamWriter fileResourcePages = new System.IO.StreamWriter
                             (ResourcePages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    var date = "Cannot Retrive Date";
                    var name = "Cannot Retrive Name";
                    var status = "Unknown";


                    var elementTdList = element.FindElements(By.XPath(".//td")).ToList();

                    if (elementTdList.Any() && elementTdList.Count > 4)
                    {

                        status = elementTdList.ElementAt(4).Text;

                    }


                    if (elementTdList.Any() && elementTdList.Count > 5)
                    {

                        date = elementTdList.ElementAt(5).Text;

                    }


                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            name = tableLink.GetAttribute("href");

                            Console.WriteLine(name);


                        }

                        fileResourcePages.WriteLine($"{name},{status},{date}");

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        public void GetLinksVacancyPages()
        {

            using (System.IO.StreamWriter fileVacancyPages = new System.IO.StreamWriter
                            (VacancyPages, true))
            {

                foreach (IWebElement element in allElements2)
                {

                    var tableData = element.FindElements(By.XPath(".//td[@class='views-field views-field-title']")).FirstOrDefault();

                    if (tableData != null)
                    {

                        var tableLink = tableData.FindElements(By.TagName("a")).FirstOrDefault();

                        if (tableLink != null)
                        {
                            var tags = tableLink.GetAttribute("href");

                            Console.WriteLine(tags);

                            fileVacancyPages.WriteLine(tags);
                        }

                    }

                }

                //file.WriteLine("<---- Next Page ---->");
            }
        }

        
        public IWebElement NextPage()
        {
            if (next != null)
            {
                return wait.Until(ExpectedConditions.ElementToBeClickable(next));
            }
            else
            {
                return null;
            }
        }


        public void ChromeFullScreenshot()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            //sim.Keyboard.ModifiedKeyStroke(
            //new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            //new[] { VirtualKeyCode.VK_2 });
            sim.Keyboard.KeyPress(VirtualKeyCode.NEXT);
            sim.Keyboard.KeyPress(VirtualKeyCode.NEXT);
            sim.Keyboard.KeyPress(VirtualKeyCode.NEXT);
            //Take screenshot
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);

            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            new[] { VirtualKeyCode.VK_P }
            );
            Thread.Sleep(1000);
            sim.Keyboard.TextEntry("screenshot");
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);
        }
    }
}




