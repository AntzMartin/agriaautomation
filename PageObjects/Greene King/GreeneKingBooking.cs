﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;
using WindowsInput;
using WindowsInput.Native;

using AutomationExample.PageObjects;
using CsvHelper;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Test.Automation.Framework.ConfigurationHelpers;
using System.Net;
using System.Collections;

namespace AutomationExample.PageObjects
{
    public class GreeneKingBooking : PageWithMenu
    {
        public GreeneKingBooking(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl;
        }

        public void ValidateTitle()
        {
            Assert.AreEqual(Configuration.GeneralSettings.aboutPageTitle.ToString(), test.Driver.Title);

        }

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[6]/nav[1]/div[2]/div[1]/button[1]/span[1]")]
        private IWebElement BookNow;

        [FindsBy(How = How.XPath, Using = "//input[@id='KeywordSearch']")]
        private IWebElement SearchHotel;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'booking-component-form-locations__list')]")]
        private IWebElement HotelList;

        [FindsBy(How = How.XPath, Using = "//div[@class='booking-component__form-container']//span[@class='cta__label'][contains(text(),'Check Availability')]")]
        private IWebElement CheckAvailabilityButton;

        [FindsBy(How = How.XPath, Using = "//img[@class='progress-bar__link']")]
        private IWebElement Home;

        [FindsBy(How = How.XPath, Using = "//input[@id='CheckInDate']")]
        private IWebElement DatePicker;

        [FindsBy(How = How.XPath, Using = "//table[@id='CheckInDate_table']//div[contains(@class,'picker__day picker__day--infocus')][contains(text(),'26')]")]
        private IWebElement dateSelection;

        [FindsBy(How = How.XPath, Using = "The Bay Horse Hotel, Wigan')]")]
        private IWebElement BayWigan;

        [FindsBy(How = How.XPath, Using = "Berkshire Arms Hotel, Newbury')]")]
        private IWebElement NewBury;

        [FindsBy(How = How.XPath, Using = "The Bridge Inn, Bristol')]")]
        private IWebElement Bristol;

        [FindsBy(How = How.XPath, Using = "The Dog & Partridge Hotel, Burton-On-Trent')]")]
        private IWebElement Brent;

        [FindsBy(How = How.XPath, Using = "//a[@class='cta button-new--filled-green']")]
        private IWebElement HotelPageAvailability;

        [FindsBy(How = How.ClassName, Using = "loader__image")]
        private IWebElement APILoadingImage;

        [FindsBy(How = How.ClassName, Using = "booking-coponent-form-locations__result-button")]
        protected IList<IWebElement> HotelLocation;

        [FindsBy(How = How.Id, Using = "popup-room-availability-error")]
        private IWebElement NoAvailability;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Close')]")]
        private IWebElement closeAvailability;

        [FindsBy(How = How.ClassName, Using = "page page--search section")]
        private IWebElement stall;

        [FindsBy(How = How.XPath, Using = "//div[@id='CheckInDate_root']//div[contains(@title,'Next month')]")]
        private IWebElement NextMonth;

        [FindsBy(How = How.XPath, Using = "//h2[@class='h1']")]
        private IWebElement RoomInfo;
        
        [FindsBy(How = How.XPath, Using = "//a[@class='cta button-new--filled-white js-cookie-policy-bar-close']//span[@class='cta__label'][contains(text(),'Close')]")]
        private IWebElement CookieAccept;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'overlay overlay--booking-component')]")]
        private IWebElement firstChoice;

        [FindsBy(How = How.XPath, Using = "//a[@class='button-new button-new--filled-green']")]
        private IWebElement seeAllRates;

        [FindsBy(How = How.XPath, Using = "//div[@class='room-info__prices']//div[2]//div[1]//div[1]//a[1]")]
        private IWebElement clickRate;

        [FindsBy(How = How.XPath, Using = "//div[@class='room-info__prices']//div[2]//div[1]//div[1]//a[1]")]
        private IWebElement clickRateSingle;

        [FindsBy(How = How.XPath, Using = "//a[@class='basket-heading__button button-new button-new--filled-white button-new--large']")]
        private IWebElement basketContinue;

        
        public void Cookie()
        {
            Thread.Sleep(2000);
            wait.Until(ExpectedConditions.ElementToBeClickable(CookieAccept)).Click();
            //Thread.Sleep(1000);
        }


        public bool apiLoading()
        {

            bool loadingIconExists = false;

            try
            {
                if (APILoadingImage.Displayed)
                {
                    loadingIconExists = true;
                }
                else
                {
                    loadingIconExists = false;
                }
            }
            catch
            {
                loadingIconExists = false;

            }

            return loadingIconExists;
        }

        public void APIWait()
        {
            if (apiLoading() == true)
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("loader__image")));
                Thread.Sleep(7000);
            }
        }


        string hotelsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\List of Hotels on Live.txt";
        string resultsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Results on Live.txt";
        //string hotelsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GKMissingHotelsOnly.txt";
        //string resultsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingResults-AfterFix.txt";

        public void CompleteHotelSearch()
        {
            if (File.Exists(hotelsFilePath) || File.Exists(resultsFilePath))
            {
                File.Delete(hotelsFilePath);
                File.Delete(resultsFilePath);
            }

            int numberOfHotels = HotelLocation.Count;

            foreach (IWebElement hotels in HotelLocation)
            {


                //var indexOfHotel = hotelName.Last();
                //var indexOfHotel = HotelLocation.Last();
                // string currentHotel = hotelName;
                //String[] allText = new String[allElements.Count];
                var hotelName = hotels.GetAttribute("innerText");

               

                //Console.WriteLine("Hotel Name Again: " + hotelName);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(hotelsFilePath, true))
                    file.WriteLine(hotelName);
            }
        }

        public bool NoAvailabilityCheck()
        {
           
            bool modalExists = false;

            try
            {
                if (closeAvailability.Displayed)
                {
                    modalExists = true;
                }
                else
                {
                    modalExists = false;
                }
            }
            catch
            {
                modalExists = false;
                
            }

            return modalExists;
        }

        public bool imageLoaded()
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://booking.greenekinginns.co.uk/book/room/configure/1");
            request.Method = "HEAD";

            bool exists;

            try
            {
                request.GetResponse();
                exists = true;
            }
            catch
            {
                exists = false;
            }
            return exists;
        }

        public void CheckAvailability()
        {
            //Console.WriteLine(imageLoaded());
            int countMonths = 0;
            int countHotels = 24;
            bool availibilityRestart = false;
            //bool imageLoaded;

            //test.TakeFullPageSnapShots("Test - Test Screen - Test");


            List<string> hotelListFile = File.ReadAllLines(hotelsFilePath).ToList();
            foreach (string hotel in hotelListFile)
            {

                //var hotelTdList = hotel.FindElements(By.XPath("//div[contains(@class,'booking-component__inner')]//ul[contains(@class,'booking-component-form-locations__list')]")).ToList();

                //if (hotelTdList.Any())
                //{
                //}
                
                var filename = hotel.Replace("https://www.greenekinginns.co.uk", "").Replace("/", "-");
                countMonths++;
                countHotels++;
                Thread.Sleep(5000);
                if (availibilityRestart == false)
                {
                    wait.Until(ExpectedConditions.ElementToBeClickable(BookNow)).Click();
                    availibilityRestart = false;

                }
                wait.Until(ExpectedConditions.ElementToBeClickable(DatePicker)).Click();
                if (countMonths <= 2)
                {
                    wait.Until(ExpectedConditions.ElementToBeClickable(NextMonth)).Click();
                    wait.Until(ExpectedConditions.ElementToBeClickable(NextMonth)).Click();
                }
                
                
                wait.Until(ExpectedConditions.ElementToBeClickable(dateSelection)).Click();
                wait.Until(ExpectedConditions.ElementToBeClickable(SearchHotel)).Clear();
                wait.Until(ExpectedConditions.ElementToBeClickable(SearchHotel)).SendKeys(hotel);

                string liCount = "//li["+countHotels+"]";
                string hotelDiv = "//div[contains(@class,'overlay overlay--booking-component')]";

                
                test.Driver.FindElement(By.XPath(hotelDiv+liCount)).Click();
                //wait.Until(ExpectedConditions.ElementToBeClickable(firstChoice)).Click();
                
                wait.Until(ExpectedConditions.ElementToBeClickable(CheckAvailabilityButton)).Click();
                Thread.Sleep(5000);

                try
                {
                    if (NoAvailabilityCheck() == true) //|| stall.Enabled)
                    {
                        Console.WriteLine("No availability: " + hotel);
                        //Console.WriteLine(hotel + " - No availability - Check Manually");
                        var hotelNoAvailability = hotel.Replace(hotel, hotel + " - No availability - Check Manually");

                        test.TakeFullPageSnapShots(filename);
                        //ChromeFullScreenshot();
                        wait.Until(ExpectedConditions.ElementToBeClickable(closeAvailability)).Click();
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(hotelNoAvailability);
                        Thread.Sleep(1000);
                        availibilityRestart = true;
                        restart();
                    }

                    else if (test.Driver.Url == "https://www.greenekinginns.co.uk/error.htm?aspxerrorpath=/hotels/")
                    {
                        Console.WriteLine("Error: " + hotel);
                        //Console.WriteLine(hotel + " - No availability - Check Manually");
                        var hotelNoAvailability = hotel.Replace(hotel, hotel + " **** Error **** Check Manually");
                        test.TakeFullPageSnapShots(filename);
                        restartAfterCrash();
                    }

                    else if (hotel.Contains("&"))
                    {
                        var containsAmpersand = hotel.Replace(hotel, hotel + " $$$$ Contained & $$$$ Check Manually");
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(containsAmpersand);
                        test.TakeFullPageSnapShots(filename);
                        restartAfterCrash();
                    }

                    //else if (imageLoaded() == false && RoomInfo.Displayed)
                    //{
                    //    Console.WriteLine(hotel + " ##### Images weren't loaded ##### Check Manually");
                    //    var imageLoadFailed = hotel.Replace(hotel, hotel + "- Images weren't loaded - Check Manually");
                    //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(imageLoadFailed);

                    //    APIWait();
                    //    Thread.Sleep(1000);
                    //    test.TakeFullPageSnapShots(filename);
                    //    //ChromeFullScreenshot();

                    //    restart();
                    //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(imageLoadFailed);
                    //}

                    else
                    {
                        Thread.Sleep(2000);
                        try
                        {
                            wait.Until(ExpectedConditions.ElementToBeClickable(seeAllRates)).Click();
                            Thread.Sleep(2000);
                            wait.Until(ExpectedConditions.ElementToBeClickable(clickRate)).Click();
                        }
                        catch
                        {
                            wait.Until(ExpectedConditions.ElementToBeClickable(clickRateSingle)).Click();
                        }

                        test.TakeFullPageSnapShots(filename);
                        wait.Until(ExpectedConditions.ElementToBeClickable(basketContinue)).Click();

                        Thread.Sleep(1000);
                        var screenshotTaken = hotel.Replace(hotel, hotel + " - Screenshot Taken");

                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(screenshotTaken);
                        Thread.Sleep(2000);
                        restart();
                    }
                }
            catch (IOException)
            {
                Console.WriteLine(hotel + " - Catch Error - Check Manually");
                var hotelError = hotel.Replace(hotel, hotel + " ******** Error - Check Manually ********");
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(hotelError);
                    wait.Until(ExpectedConditions.ElementToBeClickable(closeAvailability)).Click();
                    restart();

            }

        }
    }

        public void restartAfterCrash()
        {
            Thread.Sleep(1000);
            test.Driver.Navigate().GoToUrl("https://www.greenekinginns.co.uk");
        }

        public void restart()
        {
            Thread.Sleep(1000);
            wait.Until(ExpectedConditions.ElementToBeClickable(Home)).Click();
        }

        public void ChromeFullScreenshot()
        {
            InputSimulator sim = new InputSimulator();
            //Record video
            //sim.Keyboard.ModifiedKeyStroke(
            //new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            //new[] { VirtualKeyCode.VK_2 });
            sim.Keyboard.KeyPress(VirtualKeyCode.NEXT);
            sim.Keyboard.KeyPress(VirtualKeyCode.NEXT);
            sim.Keyboard.KeyPress(VirtualKeyCode.NEXT);
            //Take screenshot
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);

            Thread.Sleep(1000);
            sim.Keyboard.ModifiedKeyStroke(
            new[] { VirtualKeyCode.CONTROL, VirtualKeyCode.SHIFT },
            new[] { VirtualKeyCode.VK_P }
            );
            Thread.Sleep(1000);
            sim.Keyboard.TextEntry("screenshot");
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(1000);
            sim.Keyboard.KeyPress(VirtualKeyCode.F12);
        }     
    }
}




