﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using Test.Automation.Framework;
using SeleniumExtras.WaitHelpers;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using FrameworkExample.TestDataModels;

namespace AutomationExample.PageObjects
{
    public class ContactPage: BasePage
    {
        public ContactPage(SeleniumTest test) : base(test)
        {
            Url = Configuration.RunConfiguration.BaseUrl + "contact/";
        }

        [FindsBy(How = How.Id, Using = "Name")]
        private IWebElement Name;

        [FindsBy(How = How.Id, Using = "Email")]
        private IWebElement Email;

        [FindsBy(How = How.Id, Using = "PhoneNumber")]
        private IWebElement PhoneNumber;

        [FindsBy(How = How.Id, Using = "Company")]
        private IWebElement Company;

        [FindsBy(How = How.Id, Using = "Message")]
        private IWebElement Message;

        public override void GoToPage()
        {
            base.GoToPage();
        }

        public void InputContactData(ContactFormData data)
        {
            Name.SendKeys(data.Name);
            Email.SendKeys(data.Email);
            PhoneNumber.SendKeys(data.Phone);
            Company.SendKeys(data.Company);
            Message.SendKeys(data.Message);
        }
    }
}
