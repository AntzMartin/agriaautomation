﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomationTest.Core.Models
{
    public class QuoteAndBuyPetModel
    {
        public string PetName { get; set; }
        public string DogBreed { get; set; }
        public string CostOfDog { get; set; }
    }
}
