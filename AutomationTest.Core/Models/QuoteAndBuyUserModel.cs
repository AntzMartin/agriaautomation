﻿using System;

namespace AutomationTest.Core.Models
{
    public class QuoteAndBuyUserModel
    {
        public string Brand { get; set; }
        public string Type { get; set; }
        public string Journey { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string HouseNumberANDName { get; set; }
        public string Postcode { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string PetName { get; set; }
        public string PetAge { get; set; }
        public string PetSpecies { get; set; }
        public string PetBreed { get; set; }
        public bool HomeCat { get; set; }
        public bool PetLiable { get; set; }
        public bool PetIllness { get; set; }
        public string PetGender { get; set; }
        public string PetCost { get; set; }
        public int DaysTillStart { get; set; }
        public bool IsLifeTimePlus { get; set; }
        public bool HighExcess { get; set; }
        public bool Multipet { get; set; }
        public bool OverseasTravel { get; set; }
        public bool DeathAndLoss { get; set; }
        public bool BreedingRisks { get; set; }
        public bool BoardingFees { get; set; }
        public string PromoCode { get; set; }
        public bool KeepingYouInformed { get; set; }
        public bool PayMonthly { get; set; }
        public bool DriectDebit { get; set; }
        public int DayOfPayment { get; set; }
        
        public DateTime PolicyStartDate
        {
            get
            {
                return DateTime.Today.AddDays(this.DaysTillStart);
            }

        }

    }
}
