﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using global::NUnit.Framework;

namespace Automation.Core.Models
{
    class CSVHelper
    {
        public static IEnumerable<TestCaseData> ReadDataDriveFileCsv(string file, string[] diffParam, string testName)
            {
                using (var fs = File.OpenRead(file))
                using (var sr = new StreamReader(fs))
                {
                    string line = string.Empty;
                    line = sr.ReadLine();
                    string[] columns = line.Split(
                                    new char[] { ';' },
                                    StringSplitOptions.None);

                    var columnsNumber = columns.Length;
                    var row = 1;

                    while (line != null)
                    {
                        string testCaseName;
                        line = sr.ReadLine();
                        if (line != null)
                        {
                            var testParams = new Dictionary<string, string>();

                            string[] split = line.Split(
                                new char[] { ';' },
                                StringSplitOptions.None);

                            for (int i = 0; i < columnsNumber; i++)
                            {
                                testParams.Add(columns[i], split[i]);
                            }

                            try
                            {
                                testCaseName = TestCaseName(diffParam, testParams, testName);
                            }
                            catch (DataDrivenReadException e)
                            {
                                throw new DataDrivenReadException(
                                string.Format(
                                           " Exception while reading Csv Data Driven file\n searched key '{0}' not found \n for test {1} in file '{2}' at row {3}",
                                            e.Message,
                                            testName,
                                            file,
                                            row));
                            }

                            row = row + 1;

                            var data = new TestCaseData(testParams);
                            data.SetName(testCaseName);
                            yield return data;
                        }
                    }
                }
            }
    