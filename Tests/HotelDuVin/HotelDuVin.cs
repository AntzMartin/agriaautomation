﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;

namespace Automation
{

    public class HotelDuVinBooking : SeleniumTest
    {
        //region Constructors - Do not modify
        public HotelDuVinBooking(string description, Target browser) : base(description, browser) { }

        //endregion

        [Test]
        public void BookingWithoutPromo()
        {

            var booking = new Bookings(this);
            booking.GoToPage();
            Thread.Sleep(1000);

            booking.BookARoomButton();
            Thread.Sleep(2000);
            booking.HotelSelect();
            Thread.Sleep(5000);


            //var quote = new QuotePage(this);
            //quote.GoToPage();
            ////Driver.Navigate().GoToUrl("https://eqtr-test.agriapet.co.uk/quote/");
            //IWebDriver driverFirefox = new FirefoxDriver();
            //IWebDriver driverChrome = new ChromeDriver();

        }
    }
}
