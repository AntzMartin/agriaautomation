﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;
using WindowsInput;
using WindowsInput.Native;

namespace Village
{

    public class VillageQueueIt : SeleniumTest
    {
        //region Constructors - Do not modify

        public VillageQueueIt(string description, Target browser) : base(description, browser) { }

        //endregion
        
        [Test]
        public void QuickBookWithoutPromo()
        {
            var queueIt = new BookingWithQueueIt(this);
            queueIt.GoToPage();
            while (Driver.Url.Contains("queue-it"))
            {
                Thread.Sleep(5000);
            }

            //queueIt.FullPriceNotification();
            queueIt.QuickBookWithoutPromo();
            queueIt.RoomRates();

        }

        [Test]
        public void QuickBookWithPromo()
        {
            
            var queueIt = new BookingWithQueueIt(this);

            while (Driver.Url.Contains("queue-it"))
            {
                Thread.Sleep(5000);
            }

            queueIt.FullPriceNotification();
            queueIt.QuickBookWithPromo();

        }

        [Test]
        public void BookViaOffersPage()
        {
            var queueIt = new BookingWithQueueIt(this);

            while (Driver.Url.Contains("queue-it"))
            {
                Thread.Sleep(5000);
            }

            queueIt.FullPriceNotification();
            queueIt.BookOffer();
        }

        [Test]
        public void HotelPages()
        {
            var queueIt = new BookingWithQueueIt(this);

            while (Driver.Url.Contains("queue-it"))
            {
                Thread.Sleep(5000);
            }

            queueIt.FullPriceNotification();
            queueIt.BookOffer();
        }





    }
}

    