﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;
using WindowsInput;
using WindowsInput.Native;

namespace Village
{
        public class VillageMultipleUsers : SeleniumTest
        {
            //region Constructors - Do not modify

            public VillageMultipleUsers(string description, Target browser) : base(description, browser) { }


            //endregion
            [Test]
            public void ChromeMultipleTabs()
            {
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                //Thread.Sleep(1000);

                var queueIt = new BookingWithQueueIt(this);

                //while (Driver.Url.Contains("queue-it"))
                //{
                //    Thread.Sleep(5000);
                //}

                //            queueIt.FullPrice();
                //          queueIt.QuickBook();

                int number = 0;

                IWebDriver driver2 = new ChromeDriver();
                driver2.Manage().Window.Maximize();
                // driver2.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                // queueIt.QuickBook();
                // driver2.Close();

                //while (driver2.Url.Contains("queue-it"))
                //{
                //    Thread.Sleep(5000);
                //}

                IWebDriver driver3 = new ChromeDriver();
                driver3.Manage().Window.Maximize();
                //    driver3.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver3.Close();

                IWebDriver driver4 = new ChromeDriver();
                driver4.Manage().Window.Maximize();
                //   driver4.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver4.Close();

                IWebDriver driver5 = new ChromeDriver();
                driver5.Manage().Window.Maximize();
                //   driver5.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver5.Close();

                IWebDriver driver6 = new ChromeDriver();
                driver6.Manage().Window.Maximize();
                //    driver6.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver6.Close();

                IWebDriver driver7 = new ChromeDriver();
                driver7.Manage().Window.Maximize();
                //    driver7.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver2.Close();

                IWebDriver driver8 = new ChromeDriver();
                driver8.Manage().Window.Maximize();
                //    driver8.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver3.Close();

                IWebDriver driver9 = new ChromeDriver();
                driver9.Manage().Window.Maximize();
                //   driver9.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver4.Close();

                IWebDriver driver10 = new ChromeDriver();
                driver10.Manage().Window.Maximize();
            //   driver10.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver11 = new ChromeDriver();
            //driver11.Manage().Window.Maximize();
            //driver11.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver12 = new ChromeDriver();
            //driver12.Manage().Window.Maximize();
            //driver12.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver2.Close();

            //IWebDriver driver13 = new ChromeDriver();
            //driver13.Manage().Window.Maximize();
            //driver13.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver3.Close();

            //IWebDriver driver14 = new ChromeDriver();
            //driver14.Manage().Window.Maximize();
            //driver14.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver4.Close();

            //IWebDriver driver15 = new ChromeDriver();
            //driver15.Manage().Window.Maximize();
            //driver15.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver5.Close();

            //IWebDriver driver16 = new ChromeDriver();
            //driver16.Manage().Window.Maximize();
            //driver16.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver6.Close();
            //IWebDriver driver17 = new ChromeDriver();
            //driver17.Manage().Window.Maximize();
            //driver17.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver2.Close();

            //IWebDriver driver18 = new ChromeDriver();
            //driver18.Manage().Window.Maximize();
            //driver18.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver3.Close();

            //IWebDriver driver19 = new ChromeDriver();
            //driver19.Manage().Window.Maximize();
            //driver19.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver4.Close();

            //IWebDriver driver20 = new ChromeDriver();
            //driver20.Manage().Window.Maximize();
            //driver20.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver5.Close();

            //IWebDriver driver21 = new ChromeDriver();
            //driver21.Manage().Window.Maximize();
            //driver21.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver5.Close();

            //IWebDriver driver22 = new ChromeDriver();
            //driver22.Manage().Window.Maximize();
            //driver22.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver6.Close();

            //IWebDriver driver23 = new ChromeDriver();
            //driver23.Manage().Window.Maximize();
            //driver23.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver2.Close();

            //IWebDriver driver24 = new ChromeDriver();
            //driver24.Manage().Window.Maximize();
            //driver24.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver3.Close();

            //IWebDriver driver25 = new ChromeDriver();
            //driver25.Manage().Window.Maximize();
            //driver25.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver4.Close();

            //IWebDriver driver26 = new ChromeDriver();
            //driver26.Manage().Window.Maximize();
            //driver26.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver5.Close();

            //IWebDriver driver27 = new ChromeDriver();
            //driver27.Manage().Window.Maximize();
            //driver27.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver6.Close();
            //IWebDriver driver28 = new ChromeDriver();
            //driver28.Manage().Window.Maximize();
            //driver28.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver2.Close();

            //IWebDriver driver29 = new ChromeDriver();
            //driver29.Manage().Window.Maximize();
            //driver29.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver3.Close();

            //IWebDriver driver30 = new ChromeDriver();
            //driver30.Manage().Window.Maximize();
            //driver30.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver4.Close();

            //IWebDriver driver31 = new ChromeDriver();
            //driver31.Manage().Window.Maximize();
            //driver31.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver5.Close();

            //IWebDriver driver32 = new ChromeDriver();
            //driver32.Manage().Window.Maximize();
            //driver32.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver6.Close();
            //IWebDriver driver33 = new ChromeDriver();
            //driver33.Manage().Window.Maximize();
            //driver33.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver2.Close();

            //IWebDriver driver34 = new ChromeDriver();
            //driver34.Manage().Window.Maximize();
            //driver34.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver3.Close();

            //IWebDriver driver35 = new ChromeDriver();
            //driver35.Manage().Window.Maximize();
            //driver35.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver4.Close();

            //IWebDriver driver36 = new ChromeDriver();
            //driver36.Manage().Window.Maximize();
            //driver36.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver5.Close();

            //IWebDriver driver37 = new ChromeDriver();
            //driver37.Manage().Window.Maximize();
            //driver37.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver6.Close();

            //IWebDriver driver38 = new ChromeDriver();
            //driver38.Manage().Window.Maximize();
            //driver38.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver2.Close();

            //IWebDriver driver39 = new ChromeDriver();
            //driver39.Manage().Window.Maximize();
            //driver39.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver3.Close();

            //IWebDriver driver40 = new ChromeDriver();
            //driver40.Manage().Window.Maximize();
            //driver40.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //driver40.Close();

            //    IWebDriver driver41 = new ChromeDriver();
            //    driver41.Manage().Window.Maximize();
            //  //  driver41.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver5.Close();

            //    IWebDriver driver42 = new ChromeDriver();
            //    driver42.Manage().Window.Maximize();
            //   // driver42.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver43 = new ChromeDriver();
            //    driver43.Manage().Window.Maximize();
            //  //  driver43.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver2.Close();

            //    IWebDriver driver44 = new ChromeDriver();
            //    driver44.Manage().Window.Maximize();
            // //   driver44.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver3.Close();

            //    IWebDriver driver45 = new ChromeDriver();
            //    driver45.Manage().Window.Maximize();
            ////    driver45.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver4.Close();

            //    IWebDriver driver46 = new ChromeDriver();
            //    driver46.Manage().Window.Maximize();
            // //   driver46.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver5.Close();

            //    IWebDriver driver47 = new ChromeDriver();
            //    driver47.Manage().Window.Maximize();
            // //   driver47.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver6.Close();

            //    IWebDriver driver48 = new ChromeDriver();
            //    driver48.Manage().Window.Maximize();
            // //   driver48.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver6.Close();

            //    IWebDriver driver49 = new ChromeDriver();
            //    driver49.Manage().Window.Maximize();
            // //   driver49.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
            //    //driver6.Close();

            //    IWebDriver driver50 = new ChromeDriver();
            //    driver50.Manage().Window.Maximize();
            // //   driver50.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver51 = new ChromeDriver();
            //    driver51.Manage().Window.Maximize();
            // //   driver51.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver52 = new ChromeDriver();
            //    driver52.Manage().Window.Maximize();
            // //   driver52.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver53 = new ChromeDriver();
            //    driver53.Manage().Window.Maximize();
            //  //  driver53.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver54 = new ChromeDriver();
            //    driver54.Manage().Window.Maximize();
            //  //  driver54.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver55 = new ChromeDriver();
            //    driver55.Manage().Window.Maximize();
            //  //  driver55.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver56 = new ChromeDriver();
            //    driver56.Manage().Window.Maximize();
            // //   driver56.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver57 = new ChromeDriver();
            //    driver57.Manage().Window.Maximize();
            // //   driver57.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver58 = new ChromeDriver();
            //    driver58.Manage().Window.Maximize();
            //  //  driver58.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver59 = new ChromeDriver();
            //    driver59.Manage().Window.Maximize();
            //   // driver59.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //    IWebDriver driver60 = new ChromeDriver();
            //    driver60.Manage().Window.Maximize();
            //   // driver60.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver61 = new ChromeDriver();
            //driver61.Manage().Window.Maximize();
            //driver61.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver62 = new ChromeDriver();
            //driver61.Manage().Window.Maximize();
            //driver61.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver63 = new ChromeDriver();
            //driver62.Manage().Window.Maximize();
            //driver63.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver64 = new ChromeDriver();
            //driver64.Manage().Window.Maximize();
            //driver64.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver65 = new ChromeDriver();
            //driver65.Manage().Window.Maximize();
            //driver65.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver66 = new ChromeDriver();
            //driver66.Manage().Window.Maximize();
            //driver66.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver67 = new ChromeDriver();
            //driver67.Manage().Window.Maximize();
            //driver67.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver68 = new ChromeDriver();
            //driver68.Manage().Window.Maximize();
            //driver68.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver69 = new ChromeDriver();
            //driver69.Manage().Window.Maximize();
            //driver69.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            //IWebDriver driver70 = new ChromeDriver();
            //driver70.Manage().Window.Maximize();
            //driver70.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");

            driver3.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver4.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver5.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver6.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver7.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver8.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver9.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                driver10.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver11.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver12.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver13.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver14.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver15.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver16.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver17.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver18.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver19.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver20.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver21.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver22.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver23.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver24.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver25.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver26.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver27.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver28.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver29.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver30.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver31.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver32.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver33.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver34.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver35.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver36.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver37.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver38.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver39.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                //driver40.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");


                //Thread.Sleep(5000);

                ////driver41.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver42.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver43.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver44.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver45.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver46.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver47.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver48.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver49.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver50.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver51.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver52.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver53.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver54.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver55.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver56.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver57.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver58.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver59.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");
                ////driver60.Navigate().GoToUrl("https://village-mainsite.equator-staging.com/");


                //Thread.Sleep(50000);


                //Driver.Close();
                //driver2.Close();
                //driver3.Close();
                //driver4.Close();
                //driver5.Close();
                //driver6.Close();
                //driver7.Close();
                //driver8.Close();
                //driver9.Close();
                //driver10.Close();
                //driver11.Close();
                //driver12.Close();
                //driver13.Close();
                //driver14.Close();
                //driver15.Close();
                //driver16.Close();
                //driver17.Close();
                //driver18.Close();
                //driver19.Close();
                //driver20.Close();
                //driver21.Close();
                //driver22.Close();
                //driver23.Close();
                //driver24.Close();
                //driver25.Close();
                //driver26.Close();
                //driver27.Close();
                //driver28.Close();
                //driver29.Close();
                //driver30.Close();
                //driver31.Close();
                //driver32.Close();
                //driver33.Close();
                //driver34.Close();
                //driver35.Close();
                //driver36.Close();
                //driver37.Close();
                //driver38.Close();
                //driver39.Close();
                //driver40.Close();
            //driver41.Close();
            //driver42.Close();
            //driver43.Close();
            //driver44.Close();
            //driver45.Close();
            //driver47.Close();
            //driver48.Close();
            //driver49.Close();
            //driver50.Close();
            //driver51.Close();
            //driver52.Close();
            //driver53.Close();
            //driver54.Close();
            //driver55.Close();
            //driver56.Close();
            //driver57.Close();
            //driver58.Close();
            //driver59.Close();
            //driver60.Close();
            //driver61.Close();
            //driver62.Close();
            //driver63.Close();
            //driver64.Close();
            //driver65.Close();
            //driver66.Close();
            //driver67.Close();
            //driver68.Close();
            //driver69.Close();
            //driver70.Close();
        }

        
    }
}



