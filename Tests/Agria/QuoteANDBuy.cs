﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Diagnostics;
using AutomationTest.Services;
using System.Linq;
using AutomationTest.Core.Models;
using System.IO;

namespace Agria
{

    public class QuoteANDBuy : SeleniumTest
    {
        //region Constructors - Do not modify

        public QuoteANDBuy(string description, Target browser) : base(description, browser) { }


        //endregion


        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocationTest = (@"C:\temp\tests\screenshots\Agria\test\1.2\");
        public static string screenshotLocationStaging = ("C:\\temp\\tests\\screenshots\\Agria\\staging\\1.0\\");
        private QuoteAndBuyPetModel pet;

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\Agria - Q&B.html");
            
            //Use if you want to append reports rather than overwrite
            //ExtentReports extent = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\Agria - Q&B.html", false);
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Anthony Q&B Doc";
            htmlReporter.Configuration().ReportName = "Anthony Q&B Report";

            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);

            //extentxReporter.setAppendExisting(true);
        }

        public void passedTest(string testName, string extentTestName)
        {
            TakeFullPageSnapShots(extentTestName + " - Passed");

            string[] fileListTest = Directory.GetFiles($"{screenshotLocationTest}{testName}");
            //string[] fileListStaging = Directory.GetFiles($"{screenshotLocationStaging}{testName}");

            if (!fileListTest.Any())
          //if (!fileListStaging.Any())
            {
                return;
            }

            foreach (string file in fileListTest)
            //foreach (string file in fileListStaging)
            {
                test.AddScreenCaptureFromPath(file);
            }

            test.AssignCategory("Passed");
        }

        public void failedTest(string testName, string extentTestName, Exception e)
        {           
            TakeFullPageSnapShots(extentTestName + " - Failed");
            
            string[] fileListTest = Directory.GetFiles($"{screenshotLocationTest}{testName}");
            //string[] fileListStaging = Directory.GetFiles($"{screenshotLocationStaging}{testName}");

            if (!fileListTest.Any())
            //if (!fileListStaging.Any())
            {
                return;
            }

            foreach (string file in fileListTest)
            //foreach (string file in fileListStaging)
            {
                test.AddScreenCaptureFromPath(file);
            }

            test.Fail(e.InnerException.Message);

            test.Fail("Screenshots below of where the test failed: ");

            test.AssignCategory("Failed");            
        }


        public void extentScreenshot(IWebDriver driver, String screenshotName)
        {
            TakeSnapShot("Extent Report");
        }

        //public bool PopUpCheck()
        //{
        //    var iFrameActive = Driver.SwitchTo().Frame(Driver.FindElement(By.Id("yie-overlay-ba29a23a-d8d1-5ad1-8e9b-279e32e048c8")));
        //    bool popUpExists;

        //    try
        //    {
        //        if (iFrameActive.Displayed)
        //        {
        //            popUpExists = true;
        //        }
        //        else
        //        {
        //            popUpExists = false;
        //        }
        //    }
        //    catch
        //    {
        //        popUpExists = false;

        //    }

        //    return popUpExists;
        //}


        //Quote and Buy for all pets,
        //both monthly and yearly, 
        //both credit card and debit card.
        [Test]
        public void DogQuoteCreditCardMonthly()
        {
            string testName = "DogQuoteCreditCardMonthly";
            string extentTestName = "Dog Credit Card Monthly";
            
            try
            {
                var userServices = new ParseCSV();

                var allusers = userServices.Users();

                foreach (var user in allusers)
                {

                    test = extent.CreateTest(extentTestName);
                    var quote = new QuotePage(this);
                    quote.GoToPage();
                    Thread.Sleep(1000);
                    quote.SelectDogQuote();
                    Thread.Sleep(3000);

                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null) {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    //Driver.FindElement(By.)//*[@id="element-10004"]

                    quote.DogYourDetails(user);
                    //quote.PopUpClose();
                    quote.ClickContinueButton();
                    quote.DogYourDetailsPet(user);
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.ClickContinueButton2();
                    Thread.Sleep(1000);
                    quote.KeepingYouInformed();
                    quote.Terms();
                    quote.ClickContinueButton();
                    Thread.Sleep(1000);
                    quote.PaymentMonthly();
                    Thread.Sleep(1000);
                    quote.ClickContinueButton();
                    quote.CCPaymentClick();
                    quote.ClickContinueButton();
                    Thread.Sleep(1000);
                    Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                    quote.CCPaymentEnter();
                    passedTest(testName, extentTestName);
                    //Thread.Sleep(10000);
                }
                

            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: ", e);
            }
            
        }

        [Test]
        public void DogQuoteCreditCardYearly()
        {
            string testName = "DogQuoteCreditCardYearly";
            string extentTestName = "Dog Credit Card Yearly";
            try
            {
                var userServices = new ParseCSV();

                var allusers = userServices.Users();


                foreach (var user in allusers)
                {
                    test = extent.CreateTest(extentTestName);
                    var quote = new QuotePage(this);
                    quote.GoToPage();
                    Thread.Sleep(1000);
                    
                    quote.SelectDogQuote();

                    Thread.Sleep(3000);
                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    quote.DogYourDetails(user);
                    //quote.PopUpClose();
                    quote.ClickContinueButton();
                    quote.DogYourDetailsPet(user);
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.ClickContinueButton2();
                    Thread.Sleep(2000);
                    quote.KeepingYouInformed();
                    quote.Terms();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    quote.PaymentYearly();
                    quote.ClickContinueButton();
                    quote.CCPaymentClick();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                    quote.CCPaymentEnter();
                    passedTest(testName, extentTestName);
                }
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void DogQuoteDirectDebitMonthly()
        {
            string testName = "DogQuoteDirectDebitMonthly";
            string extentTestName = "Dog Direct Debit Monthly";

            try
            {
                var userServices = new ParseCSV();

                var allusers = userServices.Users();
                var allPets = userServices.Pets();

                var count = 0;
                foreach (var user in allusers)
                {
                    var userPet = allPets.ElementAt(count);

                    test = extent.CreateTest(extentTestName);
                    var quote = new QuotePage(this);
                    quote.GoToPage();

                    Thread.Sleep(1000);
                    quote.SelectDogQuote();

                    Thread.Sleep(3000);
                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    quote.DogYourDetails(user);
                    quote.ClickContinueButton();
                    quote.DogYourDetailsPet(user);
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.ClickContinueButton2();
                    Thread.Sleep(2000);
                    quote.KeepingYouInformed();
                    quote.Terms();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    quote.PaymentMonthly();
                    quote.ClickContinueButton();
                    quote.DDPayment();
                    quote.ClickContinueButton();
                    quote.DDPaymentEnter();
                    passedTest(testName, extentTestName);
                }
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void DogQuoteDirectDebitYearly()
        {
            string testName = "DogQuoteDirectDebitYearly";
            string extentTestName = "Dog Direct Debit Yearly";
            try
            {

                var userServices = new ParseCSV();

                var allusers = userServices.Users();


                foreach (var user in allusers)
                {
                    
                    test = extent.CreateTest(extentTestName);
                    var home = new AutomationExample.PageObjects.Shawbrook(this);
                    home.GoToPage();
                    // Thread.Sleep(1000);
                    home.CookieAccept();

                    var quote = new QuotePage(this);
                    quote.GoToPage();

                    //Thread.Sleep(1000);
                    quote.SelectDogQuote();

                    Thread.Sleep(3000);
                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    quote.DogYourDetails(user);
                    quote.ClickContinueButton();
                    quote.DogYourDetailsPet(user);
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.ClickContinueButton2();
                    //Thread.Sleep(5000);
                    quote.KeepingYouInformed();
                    quote.Terms();
                    quote.ClickContinueButton();
                    //Thread.Sleep(2000);
                    quote.PaymentYearly();
                    quote.ClickContinueButton();
                    quote.DDPayment();
                    quote.ClickContinueButton();
                    quote.DDPaymentEnter();
                    passedTest(testName, extentTestName);
                }
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void CatQuoteCreditCardMonthly()
        {
            string testName = "CatQuoteCreditCardMonthly";
            string extentTestName = "Cat Credit Card Monthly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectCatQuote();
                quote.CatYourDetails();
                quote.ClickContinueButton();
                //quote.CatYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.ExtrasButtons();
                quote.ClickContinueButton();
                //Thread.Sleep(2000);
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(2000);
                quote.PaymentMonthly();
                quote.ClickContinueButton();
                quote.CCPaymentClick();
                quote.ClickContinueButton();
                Thread.Sleep(2000);
                Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                quote.CCPaymentEnter();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }


        [Test]
        public void CatQuoteCreditCardYearly()
        {
            string testName = "CatQuoteCreditCardYearly";
            string extentTestName = "Cat Credit Card Yearly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectCatQuote();
                quote.CatYourDetails();
                quote.ClickContinueButton();
                quote.CatYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.ExtrasButtons();
                quote.ClickContinueButton();
                //Thread.Sleep(2000);
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                quote.PaymentYearly();
                quote.ClickContinueButton();
                quote.CCPaymentClick();
                quote.ClickContinueButton();
                Thread.Sleep(2000);
                Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                quote.CCPaymentEnter();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }
        
        [Test]
        public void CatQuoteDirectDebitMonthly()
        {
            string testName = "CatQuoteDirectDebitMonthly";
            string extentTestName = "Cat Direct Debit Monthly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectCatQuote();
                quote.CatYourDetails();
                quote.ClickContinueButton();
                quote.CatYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.ExtrasButtons();
                quote.ClickContinueButton();
                //Thread.Sleep(2000);
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                //Thread.Sleep(2000);
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                quote.PaymentMonthly();
                quote.ClickContinueButton();
                quote.DDPayment();
                quote.ClickContinueButton();
                quote.DDPaymentEnter();
                //Thread.Sleep(5000);
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void CatQuoteDirectDebitYearly()
        {
            string testName = "CatQuoteDirectDebitYearly";
            string extentTestName = "Cat Direct Debit Yearly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                //Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                //Thread.Sleep(1000);
                quote.SelectCatQuote();
                quote.CatYourDetails();
                quote.ClickContinueButton();
                quote.CatYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.ExtrasButtons();
                quote.ClickContinueButton();
                //Thread.Sleep(2000);
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(2000);
                quote.PaymentYearly();
                quote.ClickContinueButton();
                quote.DDPayment();
                quote.ClickContinueButton();
                quote.DDPaymentEnter();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void RabbitQuoteDirectDebitMonthly()
        {
            string testName = "RabbitQuoteDirectDebitMonthly";
            string extentTestName = "Rabbit Direct Debit Monthly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectRabbitQuote();
                quote.RabbitYourDetails();
                quote.ClickContinueButton();
                quote.RabbitYourDetailsPet();
              //  quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                quote.PaymentMonthly();
                quote.ClickContinueButton();
                quote.DDPayment();
                quote.ClickContinueButton();
                quote.DDPaymentEnter();
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void RabbitQuoteDirectDebitYearly()
        {
            string testName = "RabbitQuoteDirectDebitYearly";
            string extentTestName = "Rabbit Direct Debit Yearly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectRabbitQuote();
                quote.RabbitYourDetails();
                quote.ClickContinueButton();
                quote.RabbitYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                quote.PaymentYearly();
                quote.ClickContinueButton();
                quote.DDPayment();
                quote.ClickContinueButton();
                quote.DDPaymentEnter();
                quote.ClickContinueButton();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void RabbitQuoteCreditCardMonthly()
        {
            string testName = "RabbitQuoteCreditCardMonthly";
            string extentTestName = "Rabbit Credit Card Monthly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectRabbitQuote();
                quote.RabbitYourDetails();
                quote.ClickContinueButton();
                quote.RabbitYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                quote.PaymentYearly();
                quote.ClickContinueButton();
                quote.CCPaymentClick();
                quote.ClickContinueButton();
                Thread.Sleep(2000);
                Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                quote.CCPaymentEnter();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void RabbitQuoteCreditCardYearly()
        {
            string testName = "RabbitQuoteCreditCardYearly";
            string extentTestName = "Rabbit Credit Card Yearly";

            try
            {
                test = extent.CreateTest(extentTestName);
                var home = new AutomationExample.PageObjects.Shawbrook(this);
                home.GoToPage();
                Thread.Sleep(1000);
                home.CookieAccept();

                var quote = new QuotePage(this);
                quote.GoToPage();

                Thread.Sleep(1000);
                quote.SelectRabbitQuote();
                quote.RabbitYourDetails();
                quote.ClickContinueButton();
                quote.RabbitYourDetailsPet();
                quote.ClickContinueButton();
                quote.CoverOption();
                quote.ClickContinueButton();
                quote.Excess();
                quote.ClickContinueButton();
                quote.Vacctination();
                quote.ClickContinueButton2();
                //Thread.Sleep(5000);
                quote.KeepingYouInformed();
                quote.Terms();
                quote.ClickContinueButton();
                //Thread.Sleep(5000);
                quote.PaymentYearly();
                quote.ClickContinueButton();
                quote.CCPaymentClick();
                quote.ClickContinueButton();
                Thread.Sleep(2000);
                Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                quote.CCPaymentEnter();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }

        [Test]
        public void MultiPet_DogQuote_CreditCardYearly()
        {
            string testName = "MultiPet_DogQuote_CreditCardYearly";
            string extentTestName = "MultiPet - Dog Credit Card Yearly";
            try
            {
                var userServices = new ParseCSV();
                var petServices = new ParseCSV();

                var allusers = userServices.Users();
                var allPets = petServices.Pets();


                foreach (var user in allusers)
                {
                    test = extent.CreateTest(extentTestName);
                    var quote = new QuotePage(this);
                    quote.GoToPage();
                    Thread.Sleep(1000);

                    quote.SelectDogQuote();

                    Thread.Sleep(3000);
                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    quote.DogYourDetails(user);
                    //quote.PopUpClose();
                    quote.ClickContinueButton();
                    quote.DogYourDetailsPet(user);
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.addAnotherPet();
                    foreach (var pet in allPets)
                    {
                        quote.DogYourDetailsPet(pet);
                    }
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons2();
                    quote.ClickContinueButton();
                    //quote.Vacctination();
                    quote.ClickContinueButton2();
                    Thread.Sleep(2000);
                    quote.KeepingYouInformed();
                    quote.Terms();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    quote.PaymentYearly();
                    quote.ClickContinueButton();
                    quote.CCPaymentClick();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                    quote.CCPaymentEnter();
                    passedTest(testName, extentTestName);
                }
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);

            }
        }

        [Test]
        public void MultiPet_DogAndCat_QuoteCreditCardYearly()
        {
            string testName = "MultiPet_DogAndCat_QuoteCreditCardYearly";
            string extentTestName = "MultiPet - Dog and Cat Credit Card Yearly";
            try
            {
                var userServices = new ParseCSV();
                var petServices = new ParseCSV();

                var allusers = userServices.Users();
                var allPets = petServices.Pets();


                foreach (var user in allusers)
                {
                    test = extent.CreateTest(extentTestName);
                    var quote = new QuotePage(this);
                    quote.GoToPage();
                    Thread.Sleep(1000);

                    quote.SelectDogQuote();

                    Thread.Sleep(3000);
                    var signupOverlay = Driver.FindElements(By.Id("yie-close-button-*")).FirstOrDefault();

                    if (signupOverlay != null)
                    {
                        Driver.SwitchTo().Frame(signupOverlay);
                    }

                    quote.DogYourDetails(user);
                    //quote.PopUpClose();
                    quote.ClickContinueButton();
                    quote.DogYourDetailsPet(user);
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.addAnotherPet();
                    foreach (var pet in allPets)
                    {
                        quote.DogYourDetailsPet(pet);
                    }
                    quote.ClickContinueButton();
                    quote.CoverOption();
                    quote.ClickContinueButton();
                    quote.Excess();
                    quote.ClickContinueButton();
                    quote.ExtrasButtons2();
                    quote.ClickContinueButton();
                    quote.Vacctination();
                    quote.ClickContinueButton2();
                    Thread.Sleep(2000);
                    quote.KeepingYouInformed();
                    quote.Terms();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    quote.PaymentYearly();
                    quote.ClickContinueButton();
                    quote.CCPaymentClick();
                    quote.ClickContinueButton();
                    Thread.Sleep(2000);
                    Driver.SwitchTo().Frame(Driver.FindElement(By.Id("CreditCardIFrame")));
                    quote.CCPaymentEnter();
                    passedTest(testName, extentTestName);
                }
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);

            }
        }

        [Test]
        public void KCFreeCoverGuest()
        {
            string testName = "KCFreeCoverGuest";
            string extentTestName = "KC - Free Cover - Guest";

            try
            {
                test = extent.CreateTest(extentTestName);
                var quote = new QuotePage(this);
                Driver.Navigate().GoToUrl("https://eqtr-test.kcinsurance.co.uk");
                quote.KCFreeCover();
                passedTest(testName, extentTestName);
            }
            catch (Exception e)
            {
                failedTest(testName, extentTestName, e);
                throw new Exception("Error: " + e);
            }
        }


        //[Test]
        //public void CheckAllLinks()
        //{

        //    var home = new HomePage(this);
        //    home.GoToPage();
        //    Thread.Sleep(1000);
        //    home.CookieAccept();

        //List<IWebElement> 
        //links = Driver.FindElements(By.TagName("a"));

        //foreach (var item in Driver.FindElements(By.TagName("a")))
        //{
        //    Console.WriteLine(item.GetAttribute("herf"));
        //}
        //}

        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush(); 
        }
    }
}
