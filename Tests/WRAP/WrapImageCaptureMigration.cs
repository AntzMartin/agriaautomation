﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using OpenQA.Selenium;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SeleniumExtras.PageObjects;
using CsvHelper;
using CsvHelper.Configuration.Attributes;

namespace Wrap
{

    public class WrapImageCaptureMigration : SeleniumTest
    {
        //region Constructors - Do not modify
        public WrapImageCaptureMigration(string description, Target browser) : base(description, browser) { }

        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocation = ("C:\\temp\\tests\\screenshots\\Agria\\test\\1.2\\");

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\WrapImageCheckerMigration.html");
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Anthony Image Checker";
            htmlReporter.Configuration().ReportName = "Anthony Image Report";

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }

        string allPagesFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Basic - Campaign - Resources.txt";
        string allPagesResults = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Basic - Campaign - Resources - RESULTS.txt";

        [Test]
        public void ImageCapture()
        {
            List<string> pageList = File.ReadAllLines(allPagesFilePath).ToList();
            foreach (string page in pageList)
            {
                var wrap = new WrapMigration(this);
                Thread.Sleep(2000);

                try
                {
                    {
                        int count = 0;
                        Driver.Navigate().GoToUrl(page);
                        Thread.Sleep(5000);
                        wrap.ChromeFullScreenshot();
                        //Thread.Sleep(2000);
                        count++;
                        var screenshotTaken = page.Replace(page, page + "- Screenshot Taken");

                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(allPagesResults, true)) file.WriteLine(screenshotTaken);
                        Thread.Sleep(2000);
                        //restart();
                    }
                }
                catch (IOException)
                {
                    Console.WriteLine(page + " - Error - Check Manually");
                    var pageError = page.Replace(page, page + " ******** Error - Check Manually ********");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(allPagesResults, true)) file.WriteLine(allPagesResults);
                    // wait.Until(ExpectedConditions.ElementToBeClickable(closeAvailability)).Click();
                    // restart();
                }
            }
        }

        [Test]

        public void passedTest()
        {
            TakeFullPageSnapShots("Test Passed");
            string fullScreenshot1 = (screenshotLocation + "DogQuoteCreditCardMonthly\\Anthony desktop chrome - Test Passed 1.png");
            string fullScreenshot2 = (screenshotLocation + "DogQuoteCreditCardMonthly\\Anthony desktop chrome - Test Passed 2.png");
            test.AssignCategory("Passed");
            test.AddScreenCaptureFromPath(fullScreenshot1);
            test.AddScreenCaptureFromPath(fullScreenshot2);
        }

        public void failedTest()
        {
            TakeFullPageSnapShots("Test Failed");
            string fullScreenshot1 = (screenshotLocation + "DogQuoteCreditCardMonthly\\Anthony desktop chrome - Test Failed 1.png");
            string fullScreenshot2 = (screenshotLocation + "DogQuoteCreditCardMonthly\\Anthony desktop chrome - Test Failed 2.png");

            // test.Fail(e.Message);
            test.Fail("Screenshots below of where the test failed: ");
            test.AssignCategory("Failed");
            test.AddScreenCaptureFromPath(fullScreenshot1);
            test.AddScreenCaptureFromPath(fullScreenshot2);
        }
        [Test]
        public void getLinks()
        {
            string testName = "Image Checker - Wrap broken Image";
            //   string currentHotel = Console.ReadLine();



            try
            {
                //string pageFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\missingWRAPpages.txt";
                //string pageResultsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\missingWRAPpages - results.txt";


                var wrap = new WrapMigration(this);
                test = extent.CreateTest(testName);
                int count = 0;


                wrap.GoToPage();
                Thread.Sleep(2000);
                wrap.AdminLogin();
                Driver.Navigate().GoToUrl("https://beta.wrap.org.uk/admin/content");
                Thread.Sleep(2000);
                wrap.BasicPage();
                Thread.Sleep(2000);

                //Get First page links
                wrap.GetLinksBasicPages();

                //Retrive Next Button
                var nextButton = wrap.NextPage();

                //If Next Button Exists
                while (nextButton != null)
                {

                    wrap.NextPage().Click();

                    //Gets Links
                    wrap.GetLinksBasicPages();

                    //Check If Next Button Exists
                    nextButton = wrap.NextPage();
                };


                //;
                //wrap.GetLinks();
                //Thread.Sleep(2000);
                //wrap.NextPage();
                //wrap.GetLinks();
                //Thread.Sleep(20000);
                //***************************** WRAP TAKE OUT!!!!!!!!!!! ***********************************

                //List<string> hotelListFile = File.ReadAllLines(hotelsFilePath).ToList();
                //foreach (string hotel in hotelListFile)
                //{
                //    count++;
                //    Thread.Sleep(2000);

                //    try
                //    {
                //        {
                //            Driver.Navigate().GoToUrl(hotel);
                //            Thread.Sleep(5000);
                //            greene.ChromeFullScreenshot();
                //            //Thread.Sleep(2000);
                //            var screenshotTaken = hotel.Replace(hotel, count + "- Screenshot Taken - Review Manually");

                //            using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(screenshotTaken);
                //            Thread.Sleep(2000);
                //            //restart();
                //        }
                //    }
                //    catch (IOException)
                //    {
                //        Console.WriteLine(hotel + " - Error - Check Manually");
                //        var hotelError = hotel.Replace(hotel, hotel + " ******** Error - Check Manually ********");
                //        using (System.IO.StreamWriter file = new System.IO.StreamWriter(resultsFilePath, true)) file.WriteLine(hotelError);
                //       // wait.Until(ExpectedConditions.ElementToBeClickable(closeAvailability)).Click();
                //       // restart();
                //    }
                //}
                //***************************** WRAP TAKE OUT!!!!!!!!!!! ***********************************
                //greene.CompleteHotelSearch();

                //TakeFullPageSnapShots("Test Passed");

                //var hotelResults = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingResults.txt";
                //string[] results = System.IO.File.ReadAllLines(hotelResults);

                //foreach (string line in results)
                //{
                //    if (line.Contains("No availability"))
                //    {
                //        test.Fail("\n" + line);
                //    }
                //    else
                //    {
                //        test.Pass("\n" + line);
                //    }

                //string fullScreenshot1 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Passed 1.png");
                //string fullScreenshot2 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Passed 1.png");
                //test.AssignCategory("Passed");
                //test.AddScreenCaptureFromPath(fullScreenshot1);
                //test.AddScreenCaptureFromPath(fullScreenshot2);
                //Thread.Sleep(10000);
                // }
            }
            catch (Exception e)
            {
                TakeFullPageSnapShots("Test Failed");
                //string fullScreenshot1 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Failed 1.png");
                //string fullScreenshot2 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Failed 2.png");

                //test.Fail(e.Message);
                //test.Fail("Screenshots below of where the test failed: ");
                //test.AssignCategory("Failed");
                //test.AddScreenCaptureFromPath(fullScreenshot1);
                //test.AddScreenCaptureFromPath(fullScreenshot2);
                throw new Exception("Error: ", e);
            }
        }

        [Test]
        public void CapturePages()
        {
            //wrap.GetLinks();
            //Thread.Sleep(2000);
            //wrap.NextPage();
            //wrap.GetLinks();
            //Thread.Sleep(20000);
            // *****************************WRAP TAKE OUT!!!!!!!!!!!***********************************
            string BasicLinks = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\BasicPagesAfter.txt";
            string BasicPageScreenshots = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\BasicPagesScreenshotsAfter.txt";

            string AssetLinks = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AssetPages.txt";
            string AssetScreenshots = @"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\AssetPagesScreenshotsAfter.txt";


            bool acceptedCookies = false;
            var wrap = new WrapMigration(this);
            wrap.GoToPage();

            if (acceptedCookies == false)
            {
                Thread.Sleep(2000);
                wrap.AcceptCookies();
                acceptedCookies = true;
            }

            wrap.AdminLogin();

            List<string> linkList = File.ReadAllLines(AssetLinks).ToList();
            foreach (string page in linkList)
            {
                try
                {
                    
                     
                        Driver.Navigate().GoToUrl(page);
                        Thread.Sleep(3000);
                        

                        var filename = page.Replace("https://beta.wrap.org.uk", "").Replace("/", "-");
                       
                        TakeFullPageSnapShots(filename);

                        //TakeSnapShot("Beta" + count);
                        //TakeFullPageSnapShots("Test Passed");
                        //string fullScreenshot1 = (screenshotLocation + "Beta");
                        //string fullScreenshot2 = (screenshotLocation + "Beta2");
                        //Thread.Sleep(2000);

                        if (page == Driver.Url)
                        {
                            var screenshotTaken = page.Replace(page, page + " ** Screenshot Taken ** ");

                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(AssetScreenshots, true))

                            {
                                file.WriteLine(screenshotTaken);
                                
                            }


                            Thread.Sleep(2000);

                        }
                        else
                        {
                            var Issue = page.Replace(page, page + " ####### Url didn't load ####### ");

                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(AssetScreenshots, true)) file.WriteLine(Issue);
                            Thread.Sleep(2000);
                        }
                    
                }
                catch (IOException)
                {
                    Console.WriteLine(page + " - Error - Check Manually");
                    var Error = page.Replace(page, page + " ~~~~~ Error - Check Manually ~~~~~");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(AssetScreenshots, true)) file.WriteLine(Error);
                    // wait.Until(ExpectedConditions.ElementToBeClickable(closeAvailability)).Click();
                    // restart();
                }
            }
        }

        [FindsBy(How = How.XPath, Using = "//input[@id='edit-name']")]
        protected IWebElement MediaNameInput;



        public class BasicNodes
        {
            [Index(0)]
            public string Name { get; set; }

            [Index(1)]
            public string Status { get; set; }

            [Index(2)]
            public string PublishedDate { get; set; }
        }
        public class BaiscNodeImages
        {
            public string FileName { get; set; }

            public string NodeName { get; set; }

        }



        [Test]
        public void AddHero()
        {
            string testName = "Add hero";

            var wrap = new WrapMigration(this);

            TextReader BasicNodes = new StreamReader("C:/Users/anthony.martin/source/repos/AutomationTest/AutomationTest/Reports/BasicPages.csv");

            TextReader BasicPageImage = new StreamReader("C: /Users/anthony.martin/source/repos/AutomationTest/AutomationTest/Reports/BasicPagesImages.txt");
            
            var nodeList = new List<BasicNodes>();

            var imageList = new List<BaiscNodeImages>();

            //Set up CSV Reader
            using (CsvReader csvReader = new CsvReader(BasicNodes))
            {

                csvReader.Configuration.HasHeaderRecord = false;

                nodeList = csvReader.GetRecords<BasicNodes>().ToList();

            }

            //Set up CSV Reader
            using (CsvReader csvReader = new CsvReader(BasicPageImage))
            {
                csvReader.Configuration.HasHeaderRecord = false;

                imageList = csvReader.GetRecords<BaiscNodeImages>().ToList();

            }                        

            //Loop over all basic Pages
            foreach (var BasicPage in nodeList)
            {

                try
                {
                    //Get Header Image
                    var image = imageList.FirstOrDefault(x => x.NodeName == BasicPage.Name);

                    //If image not Null
                    if (image != null)
                    {

                        test = extent.CreateTest(testName);

                        wrap.GoToPage();

                        Thread.Sleep(2000);

                        wrap.AdminLogin();

                        Driver.Navigate().GoToUrl(BasicPage.Name);

                        wrap.EditBasic();

                        Thread.Sleep(2000);

                        wrap.selectMedia();

                        Thread.Sleep(2000);

                        Driver.SwitchTo().Frame(Driver.FindElement(By.Id("entity_browser_iframe_media_entity_browser")));

                        Thread.Sleep(2000);

                        Driver.FindElement(By.XPath("//a[contains(text(),'Existing Files')]")).Click();

                        Thread.Sleep(2000);

                        Driver.FindElement(By.XPath("//input[@id='edit-name']")).SendKeys(image.FileName);

                        Thread.Sleep(2000);

                        var allElements = Driver.FindElements(By.XPath(".//div[@class='views-row']"));

                        allElements.FirstOrDefault().Click();

                        Thread.Sleep(2000);

                        wrap.Submit();

                        Thread.Sleep(10000);

                        Thread.Sleep(2000);

                    }

                }
                catch (Exception e)
                {

                    throw new Exception("Error: ", e);

                }

            }

        }

    }

}