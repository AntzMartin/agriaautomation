﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using OpenQA.Selenium;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Collections;

using System.Drawing; //Required for eyes.Open API
using Applitools.Selenium; //Applitools Selenium SDK 👈🏼
using OpenQA.Selenium.Chrome; // Selenium's Chrome browser SDK
using Target = Test.Automation.Framework.ConfigurationHelpers.Target;

namespace Shawbrook
{
    public class ShawbrookFunctionality : SeleniumTest
    {
        //region Constructors - Do not modify
        public ShawbrookFunctionality(string description, Target browser) : base(description, browser) { }
        
        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocation = (@"C:\temp\tests\screenshots\Agria\SBLive\1.2\");

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\Shawbrook.html");
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Anthony Image Checker";
            htmlReporter.Configuration().ReportName = "Anthony Image Report";

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }


        [Test]
        public void BusinessPage()
        {
            var eyes = new Eyes();
            eyes.ForceFullPageScreenshot = true;
            eyes.ApiKey = "y1IkRLCN1NneSWSqH6KOEpD6Z5qpSzh43eo1aFC106IGk110";
            
            string testName = "BusinessPage";
            test = extent.CreateTest(testName);

            var SBHome = new ShawbrookHomePage(this);
            SBHome.GoToPage();
            Thread.Sleep(3000);
            SBHome.closeCookiePolicy();

            try
            {
                var innerDriver = Driver;
                eyes.Open(innerDriver, "Demo C# app", "Shawbrook", new Size(1900, 950));

                SBHome.goToBusinessPage();
                eyes.CheckWindow();
                eyes.Close();
                SBHome.SelectContactUs();
                eyes.CheckWindow();
                eyes.Close();
                SBHome.SelectIntermediaries();
                eyes.CheckWindow();
                eyes.Close();
                //TakeFullPageSnapShots(testName);
                //string fullScreenshot1 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 1.png");
                //string fullScreenshot2 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 2.png");
                //string fullScreenshot3 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 3.png");
                //string fullScreenshot4 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 4.png");
                //string fullScreenshot5 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 5.png");
                //string fullScreenshot6 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 6.png");
                //string fullScreenshot7 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 7.png");
                test.AssignCategory("Passed");
                //test.AddScreenCaptureFromPath(fullScreenshot1);
                //test.AddScreenCaptureFromPath(fullScreenshot2);
                //test.AddScreenCaptureFromPath(fullScreenshot3);
                //test.AddScreenCaptureFromPath(fullScreenshot4);
                //test.AddScreenCaptureFromPath(fullScreenshot5);
                //test.AddScreenCaptureFromPath(fullScreenshot6);
                //test.AddScreenCaptureFromPath(fullScreenshot7);

                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                TakeFullPageSnapShots("Test Failed");
                string fullScreenshot1 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "BusinessPage\\Anthony desktop chrome - " + testName + " 1.png");
                test.Fail(e.Message);
                test.AssignCategory("Failed");
                test.Fail("Screenshots below: ");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                throw new Exception("Error: ", e);
            }
            finally
            {
                eyes.AbortIfNotClosed();
            }
            }

        [Test]
        public void ContactUs()
        {
            string testName = "Contact Us Grid";
            test = extent.CreateTest(testName);
            var SBHome = new ShawbrookHomePage(this);
            SBHome.GoToPage();
            Thread.Sleep(3000);
            //home.CookieAccept();
            SBHome.closeCookiePolicy();

            try
            {
                SBHome.goToBusinessPage();
                SBHome.SelectContactUs();

                Thread.Sleep(1000);
                TakeFullPageSnapShots(testName);
                string fullScreenshot1 = (screenshotLocation + "ContactUs\\Anthony desktop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "ContactUs\\Anthony desktop chrome - " + testName + " 2.png");
                test.AssignCategory("Passed");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
            }
            catch (Exception e)
            {
                TakeFullPageSnapShots("Test Failed");
                string fullScreenshot1 = (screenshotLocation + "ContactUs\\Anthony desktop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "ContactUs\\Anthony desktop chrome - " + testName + " 2.png");
                test.Fail(e.Message);
                test.AssignCategory("Failed");
                test.Fail("Screenshots below: ");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                throw new Exception("Error: ", e);
            }
        }

        [Test]
        public void OpenContact()
        {
            string testName = "OpenContact";
            test = extent.CreateTest(testName);
            var SBHome = new ShawbrookHomePage(this);
            
            try
            {
                Driver.Navigate().GoToUrl("https://accounts.google.com/signin/v2/identifier?continue=" +
                    "https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
                
                SBHome.EnterEmail();
                SBHome.ClickButton();
                SBHome.Password();
                SBHome.ClickButton();
                Thread.Sleep(1000);

                SBHome.NewTab();
                SBHome.GoToPage();
                //    Driver.Navigate().GoToUrl("https://www.shawbrook.co.uk/business/help-support/contact-us/");
                //    Driver.SwitchTo().Window(Driver.WindowHandles[1]);
                //    Driver.Navigate().GoToUrl("https://www.shawbrook.co.uk/business/help-support/contact-us/");
                Thread.Sleep(2000);
                SBHome.GoToPage();
                Thread.Sleep(3000);
                SBHome.closeCookiePolicy();
                
                SBHome.goToBusinessPage();
                SBHome.SelectContactUs();
                SBHome.SelectContactTechnology();
                SBHome.SelectFM();
                SBHome.WriteEmail();


                Thread.Sleep(1000);

                TakeFullPageSnapShots(testName);
                string fullScreenshot1 = (screenshotLocation + "OpenContact\\Anthony desktop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "OpenContact\\Anthony desktop chrome - " + testName + " 2.png");
                string fullScreenshot3 = (screenshotLocation + "OpenContact\\Anthony desktop chrome - " + testName + " 3.png");
                test.AssignCategory("Passed");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                test.AddScreenCaptureFromPath(fullScreenshot3);
            }
            catch (Exception e)
            {
                test.AssignCategory("Failed");
                TakeFullPageSnapShots(testName);
                string fullScreenshot1 = (screenshotLocation + "OpenContact\\Anthony desktop chrome - " + testName + " 1.png");
                string fullScreenshot2 = (screenshotLocation + "OpenContact\\Anthony desktop chrome - " + testName + " 2.png");
                string fullScreenshot3 = (screenshotLocation + "OpenContact\\Anthony desktop chrome - " + testName + " 3.png");
                test.Fail(e.Message);
                test.AssignCategory("Failed");
                test.Fail("Screenshots below: ");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                test.AddScreenCaptureFromPath(fullScreenshot3);
                throw new Exception("Error: ", e);
            }
        }

        //[Test]
        //public void SelectElement()
        //{
        //    string testName = "SelectElement";
        //    test = extent.CreateTest(testName);
        //    var SBHome = new ShawbrookHomePage(this);
        //    SBHome.GoToPage();
        //    Thread.Sleep(3000);
        //    //home.CookieAccept();
        //    SBHome.closeCookiePolicy();

        //    try
        //    {
        //        SBHome.goToBusinessPage();
        //        SBHome.SelectContactUs();
        //        Thread.Sleep(1000);
        //        SBHome.SelectNotPresentElement();
        //        Thread.Sleep(1000);
        //    }
        //    catch (Exception e)
        //    {
        //        TakeFullPageSnapShots("Test Failed");
        //        string fullScreenshot1 = (screenshotLocation + testName + "\\Anthony desktop chrome - Test Failed 1.png");
        //        string fullScreenshot2 = (screenshotLocation + testName + "\\Anthony desktop chrome - Test Failed 2.png");
        //        test.Fail(e.Message);
        //        test.AssignCategory("Failed");
        //        test.Fail("Screenshots below: ");
        //        test.AddScreenCaptureFromPath(fullScreenshot1);
        //        test.AddScreenCaptureFromPath(fullScreenshot2);
        //        throw new Exception("Error: ", e);
        //    }
        //}


        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush();
        }
    }
}
