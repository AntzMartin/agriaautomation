﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;
using System;
using FrameworkExample.TestDataModels;
using System.Threading;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using OpenQA.Selenium;
using AventStack.ExtentReports.Reporter.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Automation
{

    public class GreeneKingBookingTest : SeleniumTest
    {
        //region Constructors - Do not modify
        public GreeneKingBookingTest(string description, Target browser) : base(description, browser) { }

        public static IWebDriver driver;
        private static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;
        public static ExtentTest test;
        public static string screenshotLocation = ("C:\\temp\\tests\\screenshots\\Agria\\test\\1.2\\");
        public static string offersLocation = (@"C:\temp\tests\screenshots\GK\liveGreeneKing\1.2\CrawlOffers");

        [OneTimeSetUp]
        public void SetupReporting()
        {
            htmlReporter = new ExtentHtmlReporter(@"C:\Users\anthony.martin\source\repos\AutomationTest\AutomationTest\Reports\GreeneKingImageChecker.html");
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Anthony Image Checker";
            htmlReporter.Configuration().ReportName = "Anthony Image Report";

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }
        [Test]
        public void CrawlHotels()
        {
            string testName = "Hotels";
            var greene = new GreeneKingBooking(this);
            test = extent.CreateTest(testName);
            int count = 0;

            greene.GoToPage();
            greene.Cookie();

            var hotelURLs = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingUrlsHotels.txt";
            var urlResultsHotel = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Hotel - CrawlResults.txt";


            string[] hotelUrlList = System.IO.File.ReadAllLines(hotelURLs);

            foreach (string url in hotelUrlList)
            {
           
                try
                {
                    var pageName = url.Replace("https://www.greenekinginns.co.uk/", "").Replace("/", "-"); ;

                    Driver.Navigate().GoToUrl(url);
                    TakeFullPageSnapShots(testName + " - " + pageName);

                    //string fullScreenshot1 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 1.png");
                    //string fullScreenshot2 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 2.png");
                    //string fullScreenshot3 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 3.png");
                    //string fullScreenshot4 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 4.png");
                    //string fullScreenshot5 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 5.png");
                    //string fullScreenshot6 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 6.png");
                    //string fullScreenshot7 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 7.png");
                    //string fullScreenshot8 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 8.png");
                    //string fullScreenshot9 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 9.png");
                    //string fullScreenshot10 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 10.png");
                    //test.AssignCategory("Passed");


                    //test.AddScreenCaptureFromPath(fullScreenshot1);
                    //test.AddScreenCaptureFromPath(fullScreenshot2);
                    //test.AddScreenCaptureFromPath(fullScreenshot3);
                    //test.AddScreenCaptureFromPath(fullScreenshot4);
                    //test.AddScreenCaptureFromPath(fullScreenshot5);
                    //test.AddScreenCaptureFromPath(fullScreenshot6);
                    //test.AddScreenCaptureFromPath(fullScreenshot7);
                    //test.AddScreenCaptureFromPath(fullScreenshot8);
                    //test.AddScreenCaptureFromPath(fullScreenshot9);
                    //test.AddScreenCaptureFromPath(fullScreenshot10);

                    var cleanCapture = url.Replace(url, url + " - Succesful");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsHotel, true)) file.WriteLine(cleanCapture);
                }
                catch (Exception e)
                {
                    TakeFullPageSnapShots("Test Failed");
                    string fullScreenshot1 = (screenshotLocation + "Offers\\Anthony desktop chrome - Test Passed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "Offers\\Anthony desktop chrome - Test Passed 2.png");
                    test.Fail(e.Message);
                    test.Fail("Screenshots below of where the test failed: ");
                    test.AssignCategory("Failed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    throw new Exception("Error: ", e);

                    var issueCapture = url.Replace(url, url + " - *** Issue ***");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsHotel, true)) file.WriteLine(issueCapture);
                }
            }
        }


        [Test]
        public void CrawlPubs()
        {
            string testName = "Pubs";
            var greene = new GreeneKingBooking(this);
            test = extent.CreateTest(testName);
            int count = 0;

            greene.GoToPage();
            greene.Cookie();


            var pubsURLs = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingUrlsPubs.txt";
            var urlResultsPubs = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Pubs - CrawlResults.txt";

            string[] pubsUrlList = System.IO.File.ReadAllLines(pubsURLs);


            foreach (string url in pubsUrlList)
            {
                try
                {
                    Driver.Navigate().GoToUrl(url);
                    TakeFullPageSnapShots(testName);
                    string fullScreenshot1 = (screenshotLocation + "Pubs\\Anthony desktop chrome - Test Passed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "Pubs\\Anthony desktop chrome - Test Passed 2.png");
                    test.AssignCategory("Passed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    Thread.Sleep(5000);

                    var cleanCapture = url.Replace(url, url + " - Succesful");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsPubs, true)) file.WriteLine(cleanCapture);
                }
                catch (Exception e)
                {
                    TakeFullPageSnapShots(testName);
                    string fullScreenshot1 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Failed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Failed 2.png");

                    test.Fail(e.Message);
                    test.Fail("Screenshots below of where the test failed: ");
                    test.AssignCategory("Failed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    throw new Exception("Error: ", e);

                    var issueCapture = url.Replace(url, url + " - *** Issue ***");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsPubs, true)) file.WriteLine(issueCapture);
                }
            }
        }

        [Test]
        public void CrawlOffers()
        {
            string testName = "Offers";

            var greene = new GreeneKingBooking(this);
            test = extent.CreateTest(testName);
            

            greene.GoToPage();
            greene.Cookie();


            var offersURLs = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingUrlsOffers.txt";
            var urlResultsOffers = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\Offers - CrawlResults.txt";

            string[] offersUrlList = System.IO.File.ReadAllLines(offersURLs);

            foreach (string url in offersUrlList)
            {
                int count = 0;
                try
                {
                    var pageName = url.Replace("https://www.greenekinginns.co.uk/", "").Replace("/", "-"); ;

                    Driver.Navigate().GoToUrl(url);
                    TakeFullPageSnapShots(testName + " - " + pageName);
                    
                    string fullScreenshot1 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 1.png");
                    string fullScreenshot2 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 2.png");
                    string fullScreenshot3 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 3.png");
                    string fullScreenshot4 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 4.png");
                    string fullScreenshot5 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 5.png");
                    string fullScreenshot6 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 6.png");
                    string fullScreenshot7 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 7.png");
                    string fullScreenshot8 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 8.png");
                    string fullScreenshot9 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 9.png");
                    string fullScreenshot10 = (offersLocation + "\\Anthony desktop chrome - " + testName + " - " + pageName + " 10.png");
                    test.AssignCategory("Passed");

                    
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    test.AddScreenCaptureFromPath(fullScreenshot3);
                    test.AddScreenCaptureFromPath(fullScreenshot4);
                    test.AddScreenCaptureFromPath(fullScreenshot5);
                    test.AddScreenCaptureFromPath(fullScreenshot6);
                    test.AddScreenCaptureFromPath(fullScreenshot7);
                    test.AddScreenCaptureFromPath(fullScreenshot8);
                    test.AddScreenCaptureFromPath(fullScreenshot9);
                    test.AddScreenCaptureFromPath(fullScreenshot10);
                       
                    var cleanCapture = url.Replace(url, url + " - Succesful");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsOffers, true)) file.WriteLine(cleanCapture);
                }
                catch (Exception e)
                {
                    TakeFullPageSnapShots("Test Failed");
                    string fullScreenshot1 = (screenshotLocation + "Offers\\Anthony desktop chrome - Test Passed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "Offers\\Anthony desktop chrome - Test Passed 2.png");
                    test.Fail(e.Message);
                    test.Fail("Screenshots below of where the test failed: ");
                    test.AssignCategory("Failed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    throw new Exception("Error: ", e);

                    var issueCapture = url.Replace(url, url + " - *** Issue ***");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsOffers, true)) file.WriteLine(issueCapture);
                }
            }
        }

        [Test]
        public void CrawlPDFs()
        {
            string testName = "PDFs";
                var greene = new GreeneKingBooking(this);
                test = extent.CreateTest(testName);
                int count = 0;

                greene.GoToPage();
                greene.Cookie();
                
                var pdfURLs = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingUrlsPDFs.txt";
                var urlResultsPDFs = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\PDFs - CrawlResults.txt";

                string[] pdfsUrlList = System.IO.File.ReadAllLines(pdfURLs);

            foreach (string url in pdfsUrlList)
            {
                try
                {
                    Driver.Navigate().GoToUrl(url);
                    string fullScreenshot1 = (screenshotLocation + "PDFs\\Anthony desktop chrome - Test Passed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "PDFs\\Anthony desktop chrome - Test Passed 2.png");
                    test.AssignCategory("Passed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    Thread.Sleep(5000);

                    var cleanCapture = url.Replace(url, url + " - Succesful");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsPDFs, true)) file.WriteLine(cleanCapture);

                }
                catch (Exception e)
                {
                    TakeFullPageSnapShots("Test Failed");
                    string fullScreenshot1 = (screenshotLocation + "PDFs\\Anthony desktop chrome - Test Failed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "PDFs\\Anthony desktop chrome - Test Failed 2.png");

                    test.Fail(e.Message);
                    test.Fail("Screenshots below of where the test failed: ");
                    test.AssignCategory("Failed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    throw new Exception("Error: ", e);

                    var issueCapture = url.Replace(url, url + " - *** Issue ***");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(urlResultsPDFs, true)) file.WriteLine(issueCapture);
                }
            }
        }


        [Test]
        public void BookingEngineImageCapture()
        {
            string testName = "Image Checker - Hotel Booking";
            //   string currentHotel = Console.ReadLine();

            try
            {
                //string hotelsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\ListOfHotels - June2019.txt";
                //string resultsFilePath = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\HotelsMaintenancePage - June2019 - results.txt";

                var quote = new QuotePage(this);
                var greene = new GreeneKingBooking(this);
                test = extent.CreateTest(testName);
                int count = 0;
               

                greene.GoToPage();
                greene.Cookie();
                //greene.CompleteHotelSearch();
                greene.CheckAvailability();
                TakeFullPageSnapShots("Test Passed");

                var hotelResults = "C:\\Users\\anthony.martin\\source\\repos\\AutomationTest\\AutomationTest\\Reports\\GreeneKingResults.txt";
                string[] results = System.IO.File.ReadAllLines(hotelResults);

                foreach (string line in results)
                {
                    if (line.Contains("No availability"))
                    {
                        test.Fail("\n" + line);
                    }
                    else
                    {
                        test.Pass("\n" + line);
                    }

                    string fullScreenshot1 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Passed 1.png");
                    string fullScreenshot2 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Passed 2.png");
                    test.AssignCategory("Passed");
                    test.AddScreenCaptureFromPath(fullScreenshot1);
                    test.AddScreenCaptureFromPath(fullScreenshot2);
                    Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                TakeFullPageSnapShots("Test Failed");
                string fullScreenshot1 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Failed 1.png");
                string fullScreenshot2 = (screenshotLocation + "Image Checker - Hotel Booking\\Anthony desktop chrome - Test Failed 2.png");

                test.Fail(e.Message);
                test.Fail("Screenshots below of where the test failed: ");
                test.AssignCategory("Failed");
                test.AddScreenCaptureFromPath(fullScreenshot1);
                test.AddScreenCaptureFromPath(fullScreenshot2);
                throw new Exception("Error: ", e);
            }
        }

        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush();
        }

    }
}