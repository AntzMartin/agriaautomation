﻿using Test.Automation.Framework;
using NUnit.Framework;
using AutomationExample.PageObjects;
using Test.Automation.Framework.ConfigurationHelpers;
using Test.Automation.Framework.GeneralHelpers;

namespace AutomationExample
{
    public class JourneyTests : SeleniumTest
    {
        #region Constructors - Do not modify
        public JourneyTests(string description, Target browser) : base(description, browser) { }
        #endregion

        #region Tests

        [Test]
        public void Validation_AboutPageJourney()
        {
            var home = new HomePage(this);
            home.GoToPage();
            TakeFullPageSnapShots("Home page load");
            home.ValidateTitle();
            home.OpenMenu();
            TakeSnapShot("Home page menu open");
			home.ValidateMainMav();
            //home.ValidateMainNavFromGeneralSettings();
            var about = home.GoToAboutPage();
            TakeFullPageSnapShots("About page load");
            about.ValidateTitle();
        }
        #endregion
    }
}
