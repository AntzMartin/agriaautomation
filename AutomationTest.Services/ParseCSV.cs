﻿using AutomationTest.Core.Models;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AutomationTest.Services
{
    public class ParseCSV
    {
        public IList<QuoteAndBuyUserModel> Users()
        {
            var userModel = new List<QuoteAndBuyUserModel>();

            TextReader userReader = new StreamReader("C:/Users/anthony.martin/source/repos/AutomationTest/AutomationTest/bin/Debug/TestData/Users.csv");

            //Set up CSV Reader
            using (CsvReader csvReader = new CsvReader(userReader))
            {
                csvReader.Configuration.HasHeaderRecord = true;

                userModel = csvReader.GetRecords<QuoteAndBuyUserModel>().ToList();
            }

            return userModel;
        }

        public IList<QuoteAndBuyPetModel> Pets()
        {
            var petModel = new List<QuoteAndBuyPetModel>();

            TextReader petReader = new StreamReader("C:/Users/anthony.martin/source/repos/AutomationTest/AutomationTest/bin/Debug/TestData/Pet.csv");

            //Set up CSV Reader
            using (CsvReader csvReader = new CsvReader(petReader))
            {
                csvReader.Configuration.HasHeaderRecord = true;

                petModel = csvReader.GetRecords<QuoteAndBuyPetModel>().ToList();
            }

            return petModel;

        }
    }
}
