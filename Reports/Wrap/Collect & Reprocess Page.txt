Paper & Card Flow 2020
Household food waste collections guide
Household food waste collections guide section 1: Context and background
Household food waste collections guide Section 2: Householder perceptions of food waste recycling
Household food waste collections guide section 3: How much food waste can be collected for recycling?
Household food waste collections guide section 4: Food waste caddies and caddy liners
Household food waste collections guide section 5: Food waste collection vehicles
Household food waste collections guide section 6: Food waste communications
Household food waste collections guide section 7: Treatment and bulking of food waste
Household food waste collections guide section 8: Food waste collection from flats
Household food waste collections guide section 9: Costs and productivity
Household food waste collections guide section 10: Implementing successful food recycling
Household food waste collections guide section 11: Increasing food waste capture
Household food waste collections guide section 12: Health and safety considerations
Household food waste collections guide: Appendices
Market Snapshot: June 2018
Market Snapshot: October 2018
Gate Fees Report 2018: comparing the costs of waste treatment options
WRAP Gate Fees 2018 Report Map
Increasing recycling in urban areas
Market Snapshot: March 2018
Putting the business case to the test
Greater consistency in household recycling: summary of local authority business cases
The Food Waste Recycling Action Plan (FWRAP) Annual Report 2016/17
Collection of food and drink cartons at the kerbside
Market Snapshot June 2017
Market Snapshot March 2017
Market Snapshot: October 2017
Recycling Guidelines
Container colour consultation
Gate Fees Report 2017: comparing the costs of waste treatment options
HDPE Milk Bottle Research and Development
Biffa Polymers Mixed Plastics Recycling Facility
Commercial Scale Mixed Plastics Recycling Trials
Feasibility of Composting Wood and Cardboard Waste with Garden Waste
Guidelines for Quality Compost use in Growing Media
Environmental and Economic Benefits of Re-use
Market Snapshot: December 2016
Market Snapshots
Market Snapshot: January 2013
Market Snapshot: June 2013
Market Snapshot: September 2013
Market Snapshot: January 2014
Market Snapshot: May 2014
Market Snapshot: November 2013
Market Snapshot: July 2014
Market Snapshot: September 2014
Market Snapshot: November 2014
Market Snapshot: May 2015
Market Snapshot: September 2015
Market Snapshot: December 2015
Market Snapshot: June 2016
Market Snapshot: September 2016
Materials Pricing Report
Organics Market Situation Reports
Glass Market Situation Reports
Paper Market Situation Report
China Market Situation Report On Paper and Plastics
Plastics Market Situation Reports
Market Situation Reports
Gate Fees Report 2011
Gate Fees Report 2012
Gate Fees Report 2013
Gate Fees Report 2015
Gate Fees Report 2016
Gate Fee Reports
Development and Evaluation of a Method for Testing the Residual Biogas Potential of Digestates
Impact of Anaerobic Digestion on Some Common Crop Pests and Diseases
Anaerobic Digestion Economic Modelling Report
A Survey of the UK Anaerobic Digestion Industry in 2013 (ASORI)
Biofertiliser Management
The Effect of the Composting Process on Particular Plant, Animal and Human Pathogens
Working with Retailers to Increase Use of Sustainable Growing Media
Compost & Anaerobic Digestate Quality for Welsh Agriculture
Development of Standard Laboratory Based Test to Measure Compost Stability
BSI PAS 100: Compost Specification
BSI PAS 100 FAQs
In-Market Trial Black CPET Trays
Recycling of Aluminium Plastic Laminated Tubes and Pouches
Recycling Post-Consumer Film Trials
End Markets for Recycled Household Plastic Film
WRAP Plastics Compositional Analysis at MRFs
Optimisation of Pallet Wrapping Film Use
Plastic Packaging Market Flows and Scenarios
Study into the Re-use Potential of Household Bulky Items
Glassflow 2012 Report
Composition and Re-use Potential of Household Bulky
Assessing the Suitability of Post-Consumer Glass in Container Applications
Introducing Pots Tubs and Trays (PTTs) and Tetrapak Cartons to Existing Kerbside Collections Schemes
Materials Facility (MRF) Reporting Portal
3Rs Tracking Survey 2015: Recycling Attitudes and Behaviour
Analysis of retailers' front of store plastic film collection
Plastics Recycling Business Opportunities Report
Food Grade HDPE Recycling Process: Commercial Feasibility Study
Reports on Glass Collection from Commercial Premises
Household Waste Collection Commitment: Report & Guidance
Household Food & Drink Waste � A Product Focus
Successful PTT Collection in Cambridgeshire Through MRF Partnership
Understanding consumer decision-making for re-use and repair
Factors influencing recycling performance
Analysis of retailers' front of store plastic film collection
Wood Flow 2020
Textiles Market Situation Report
3Rs Tracking Survey 2015: Recycling Attitudes and Behaviour
Barriers to Recycling at Home
Recycling Guidelines
WRAP Gate Fees report 2015 - download summary report
Bulky waste technical report
Composition of plastic waste collected via kerbside
Recycle Week 2018 - the year we said 'We do'
Household food waste collections guide
Gate Fees Report 2018: comparing the costs of waste treatment options
WRAP Gate Fees 2018 Report Map
Increasing recycling in urban areas
Putting the business case to the test
Collection of food and drink cartons at the kerbside
Container colour consultation
Feasibility of Composting Wood and Cardboard Waste with Garden Waste
Home Composting Guidance and Information
Guidelines for Quality Compost use in Growing Media
Market Snapshots
Market Situation Reports
Gate Fee Reports
BSI PAS 110: Producing Quality Anaerobic Digestate
BSI PAS 110: Digestate Specification Registration Form
Guidance on Re-use in Collections and Sorting
Guidance on Kerbside Recycling Collection
Environmental Transformation Fund
Driving Innovation in AD: Round 2
Driving Innovation in AD: Round 1
Introduction to Driving Innovation in AD
A Survey of the UK Anaerobic Digestion Industry in 2013 (ASORI)
Doctor AD: Expert Advice for AD Plant Operators
Market Expectations and Requirements for Digestate
BSI PAS 100: Compost Specification
New Markets for Digestate
Thermophilic Aerobic Digestion
Anaerobic digestion - the process
Guidance on Anaerobic Digestion
Open Windrow Composting
In Vessel Composting (IVC)
Compost and Digestate in Agriculture: Good Practice Guide
Digestate Use in Landscaping and Regeneration Projects
Compost Production for use in Growing Media: A Good Practice Guide
Compost Use in Horticulture
Rigid Plastics Recyclability Guidance and Case Studies
Optimisation of Pallet Wrapping Film Use
Plastic Film Packaging
Collection and Sorting of Household Plastic Film Packaging
Collection and Sorting of Household Rigid Plastic Packaging
Rigid Plastic Packaging
Mixed Plastics Packaging
HDPE Plastic Bottles
PET Plastic Bottles
Plastic Bottle Recycling
Types of Plastic Packaging
Guidance on Plastics Collection, Sorting and Reprocessing
WRAP Recycling Managers Training Programme - Booking Terms and Conditions
Re-using and Recycling Bulky Items Workshops
BSI PAS 111: Processing Wood Waste
Tools for Wood Reprocessors to Identify Contaminated Wood
Guidance on Re-using and Recycling Bulky Items
The Quality Protocol for the Production of Processed Cullet from Waste Flat Glass
Collecting Glass for Commercial Premises: Solutions for Common Problems
Assessing the Suitability of Post-Consumer Glass in Container Applications
Guidance on Collecting and Reprocessing Glass
Introducing Pots Tubs and Trays (PTTs) and Tetrapak Cartons to Existing Kerbside Collections Schemes
Kerbside Collection Plastic Bottles Guide
The Materials Recovery Facilities (MRFs) Sorting Guide
Materials Facility (MRF) Reporting Portal
Guidance for Material Recovery Facilities (MRFs)
3Rs Tracking Survey 2015: Recycling Attitudes and Behaviour
Local Authority Information Dissemination Workshops
Increase Metal Recycling Rates at the Kerbside
A Well Planned Communications Campaign for the Introduction of a New Recycling Service: Cheshire West and Chester Council
Communicate a New or Enhanced Recycling Service: Worcestershire and Herefordshire Partnership
Communications to Support the Introduction of a New Waste and Recycling Service: Newcastle-under-Lyme Borough Council
Love Food Hate Waste �Student MasterChef�: Greater Manchester
Introducing a New Recycling Service to All Residents: Coventry City Council
Local Authority Communications Case Study: Oldham Council
Local Authority Communications Case Study: Barrow Borough Council
Local Authority Communications Case Study: Solihull Metropolitan Borough Council
Communications Campaign to Support Southwark�s Food Waste Recycling Trial
Local Authority Waste Prevention Case Study: Herefordshire and Worcestershire Councils
Love Food Hate Waste campaign launch Greater Manchester Waste Partnership
Local authority communications case studies
Re-use Communications Pilots
Global Food Waste Reduction
Benefits of Reducing Global Food Waste
The Business Recycling and Waste Services Commitment
Food waste in the NHS
Guidance on Commercial Waste Collections
Bring Recycling Guide
Innovation in Waste Prevention Fund (England)
Household Waste Recycling Centres (HWRCs) Guide
Guidance on Household Waste Recycling Centres (HWRCs) Collections
Household Waste Collection Commitment: Report & Guidance
The Household Waste Collection Commitment
Household Recycling Collection Systems
Guidance on High Density Housing and Flats Collections
Successful PTT Collection in Cambridgeshire Through MRF Partnership
Alternate weekly collections - guidance for local authorities
Kerbside recycling: indicative costs and performance
Partnerships are Key to Success in Re-use
Quality Management Systems (QMS) for Municipal MRFs
Commercial recycling collections guide
Quality Management Systems (QMS) for Municipal MRFs - introduction
Quality Management Systems (QMS) for Municipal MRFs - quick start flowchart
Quality Management Systems (QMS) for Municipal MRFs - ISO 9001
Quality Management Systems (QMS) for Municipal MRFs - ISO 14001
Quality Management Systems (QMS) for Municipal MRFs - OHSAS 18001
Quality Management Systems (QMS) for Municipal MRFs - final report
Household Waste Prevention Hub
About the Household Waste Prevention Hub
What is waste prevention?
The environmental, social and economic benefits of waste prevention
Different types of waste prevention activities
Delivering waste prevention
Stage 1: Making the case
Initial planning
Estimating cost and carbon savings
Stage 2: Planning waste prevention
How to write a waste prevention plan
Social policy & drivers
Environmental policy drivers
Other drivers
Good practice waste prevention plans
Stage 3: Implementing and communicating waste prevention
Waste prevention behaviours
Key stakeholders and partnership working
Communicating waste prevention
Communicating re-use
Defra 4Es model
Communications Resources
Stage 4: Monitoring and evaluation
Monitoring and evaluation approaches
WRAP monitoring and evaluation guidance
Tools and benefits calculators
Waste Prevention Activities
Food - Overview
Love Food Hate Waste
Food communications research
Food banks
Food waste data
Case studies: Food waste prevention
Garden waste - Overview
Home composting
Home composting costs and impacts
Case studies: Garden waste
Real nappies - Overview
Planning a real nappy campaign
Real nappies - working with partners
Case studies: Real nappies
Unwanted mail
Case studies: Unwanted mail
Re-use - Overview
Items for re-use
Benefits of re-use
Barriers to re-use
Re-use how to guides
How to establish a re-use baseline
How to develop a re-use strategy
How to set up and run a re-use forum
How to produce a re-use action plan
How to communicate re-use and write a communication plan
How to provide for re-use at Household Waste Recycling Centres
How to provide a re-use focussed bulky waste collection service
How to engage in re-use through the use of an EMS
Electricals - Overview
Electrical and Electronic Equipment Sustainability Action Plan (esap)
Repair of electricals
Case studies: Electricals
Textiles - Overview
Clothing
Non-clothing textiles
Love Your Clothes
Sustainable Clothing Action Plan
Case Studies: Textiles
Bulky waste - Overview
Case studies: Bulky waste prevention
Increasing demand for re-use
Encouraging donation
Partnerships for re-use
Swap shops, online exchange, hire, loan
Face-to-face exchange for re-use
Online exchange for re-use
Loan and hire activities
Case Studies: Re-use
Waste Prevention Case Study Library
Recycling glass in the hospitality sector
The Food Waste Recycling Action Plan
BSI PAS 100: Specification for Composted Materials
Good Practice Guide: Using PAS 100 compost in landscape and regeneration projects
The PAS 141 Guide for WEEE Treatment
National Communications Advisory Panel (NCAP)
Recycle Now Refresh
Improving recycling through effective communications
Background
Situational Analysis
Aim and Objectives
Target Audience
Branding and Message
Strategy and Communications Methods
Campaign Activities
Planning your Activities
Monitoring and Evaluation
Gate Fees Summary Report 2016
Household food waste collections guide
PET Categorisation Tool
HDPE Categorisation Tool
Environmental and Economic Benefits of Re-use
BSI PAS 100: Compost Specification
Compost Calculator
Tools for Wood Reprocessors to Identify Contaminated Wood
GlassRite Tool
Materials Facility (MRF) Reporting Portal
BSI PAS 100: Compost Specification Registration Form
Commercial and Household Waste/Recycling Apportionment Tool
Materials Facility Reporting Portal
Market Knowledge Portal
Glass
Recovered glass container prices
Glass PRN prices
Glass packaging recovery and recycling
Organics
Fertiliser prices
Gate fees and compost prices
Plastic
Virgin plastics prices (Europe)
Merchant prices for recovered plastic bottles
Merchant prices for recovered plastic film
Plastic PRN prices
Recovered plastic imports and exports
Plastic packaging recovery and recycling
Plastic packaging flow and end markets
Plastic packaging recovery
Recovered plastic export destinations
Seasonal trends in Chinese recovered plastic imports
Seasonal trends in UK recovered plastic exports
UK exports of recovered plastic
Metal
Ferrous scrap metal prices
Non ferrous scrap metal prices
Recovered metal can prices
Aluminium PRN prices
Steel PRN prices
Recovered metals imports and exports
Aluminium packaging recovery and recycling
Steel packaging recovery and recycling
Virgin Aluminium Prices
Paper
Prices for recovered paper
Paper PRN prices
Recovered paper imports and exports
Paper usage and exports
Paper packaging recovery and recycling
Seasonal trends in Chinese recovered paper imports
Seasonal trends in UK recovered paper exports
Pulp (NBSK) Prices
Wood
Recovered wood gate fees
Waste wood end markets
Wood PRN prices
Wood packaging recovery and recycling
Textiles
Exports of used textiles
Recovered textile export tonnage
Value of UK recovered textile exports
WEEE
Household EEE sales
Household WEEE collected
Household small and large WEEE collected (%)
Household WEEE collected ("Large" categories)
Household small WEEE collected ("Small" categories)
Cross-market data
PRN prices
Materials recovery - industry turnover
Wholesale of waste and scrap - industry turnover
Global oil spot prices
UK gas price index
Electricity spot prices
Retail petrol and diesel prices
Cotton prices
US$ per �
Yuan per US$
� per US$
Data sources
Aluminium packaging recovery & recycling
Carbon prices on the European Climate Exchange
Electricity spot prices
Ferrous scrap metal prices
Fertiliser prices
Glass collected from households for recycling 1997-2007
Glass packaging recovery & recycling
Glass production & recycling
Global oil spot prices
Household WEEE collection
Household WEEE recovery ratio
Materials recovery - industry turnover
Merchant prices for recovered paper
Metals collected from households for recycling 1997-2007
Non ferrous scrap metal prices
Organics collected from households for recycling 1997-2007
PIX Recovered Paper Index (Europe)
PRN prices
PRN � per tonne / one tonne of wood
Paper collected from households for recycling 1997-2007
Paper collection, usage and exports
Paper consumption vs recovered 2005 - 2009
Paper packaging recovery & recycling
Plastic packaging recovery & recycling
Prices for recovered paper
Prices for recovered plastic bottles
Prices for recovered plastic film
Recovered glass container prices
Recovered glass imports & exports
Recovered metal beverage container prices
Recovered metals imports and exports
Recovered paper imports & exports
Recovered plastic imports & exports
Recovered wood gate fees
Recovered wood imports & exports
Retail petrol and diesel prices
Steel packaging recovery & recycling
UK gas price index
Virgin plastic prices (Europe)
Wholesale of Waste and Scrap - industry turnover
� per PRN / one tonne of aluminium
PRN / one tonne of glass
PRN / one tonne of paper
PRN / one tonne of plastic
PRN / one tonne of steel
Greater consistency in household recycling: summary of local authority business cases
Biffa Polymers Mixed Plastics Recycling Facility
Compost Use in Landscape and Regeneration Projects
Soil Remediation Case Study: Materials Management Planning
Digestate Distribution Models
Construction of a Composting Facility: A Cost Benefit Case Study
Landscape and Regeneration: Compost Case Studies
Digestate Use in Landscaping and Regeneration Projects
Growing Media Case Study � Duchy of Cornwall Nursery
Rigid Plastics Recyclability Guidance and Case Studies
Recycling Post-Consumer Film Trials
Using Recycled Content Paper in Magazines - Trials and Case Studies
Outstanding Results from Fully Automated Plastics Sort
Investment Pays Off at a Pioneering Material Recovery Facility (MRF)
Northern Ireland Facility Reaps the Benefits of a New Environmental Management System
State of the Art Recycling Plant in East London Runs Widest Range of Collection Systems Possible
Council-run Recycling Facility Invests in Hi-Tech Kit to Maintain Paper Quality
Pioneering Material Recovery Facility (MRF) Recovers Added Value from its Residuals
The Materials Recovery Facilities (MRFs) Sorting Guide
Increase Metal Recycling Rates at the Kerbside
A Well Planned Communications Campaign for the Introduction of a New Recycling Service: Cheshire West and Chester Council
Communicate a New or Enhanced Recycling Service: Worcestershire and Herefordshire Partnership
Communications to Support the Introduction of a New Waste and Recycling Service: Newcastle-under-Lyme Borough Council
Love Food Hate Waste �Student MasterChef�: Greater Manchester
Introducing a New Recycling Service to All Residents: Coventry City Council
Local Authority Communications Case Study: Oldham Council
Local Authority Communications Case Study: Barrow Borough Council
Local Authority Communications Case Study: Solihull Metropolitan Borough Council
Communications Campaign to Support Southwark�s Food Waste Recycling Trial
Local Authority Waste Prevention Case Study: Herefordshire and Worcestershire Councils
Love Food Hate Waste campaign launch Greater Manchester Waste Partnership
Local authority communications case studies
Re-use Communications Pilots
Food waste in the NHS
Dorset Waste Partnership: Collecting Pots, Tubs and Trays (PTTs) as Part of a New Kerbside Service
