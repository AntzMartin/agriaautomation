﻿using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkExample.TestDataModels
{
    public class ContactFormData
    {
        public string Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string MobileNum { get; set; }
        public string HomeNum { get; set; }
        public string Email { get; set; }
        
        public override string ToString()
        {
            return $"ContactFormData_{Id}";
        }
    }
}
